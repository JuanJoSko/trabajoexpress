@empty(Auth::user()->type && Auth::user()->state && Auth::user()->city  && Auth::user()->phone)
<div id="verticalcenter" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Cuentanos más de ti</h4>
                </div>
                <div class="modal-body">
                    <p class="alert-complete">Antes de ofrecer trabajo, <br> completa tu perfil</p>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <a href="/" class="btn btn-info waves-effect" >Más tarde</button>
                    </a>
                    <a href="/profile/{{ Auth::user()->id }}" class="btn btn-info waves-effect" >Completar</button>
                    </a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endempty