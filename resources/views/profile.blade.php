@extends('layouts.app')

@section('content')

<!-- Title Header Start -->
<section class="inner-header-title" style="background-image:url({{ asset('/img/banners/banner_auth.jpg') }});">
    <div class="container">
        @auth
            @if ($user->id == Auth::user()->id)
                <h1>MI PERFIL</h1>
            @else
                <h1>PERFIL {{ $user->username }}</h1>
            @endif
        @else
            <h1>PERFIL {{ $user->username }}</h1>
        @endauth
       
    </div>
</section>
<div class="clearfix"></div>

<!-- Candidate Profile Start -->
<section class="detail-desc advance-detail-pr gray-bg">
    <div class="container white-shadow">
        <div class="row">
            <div class="detail-pic" id ="picUser">
                <img id="avatarFile" src="{{ asset('/img/users/'.$user->photo) }}"  class="img" alt="" />
                @auth
                    @if ($user->id == Auth::user()->id)
                        <a id="addPhoto" href="javascript:void(0)" class="detail-edit" title="edit">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <form id="photoForm" action="{{ route('profile.updatePhoto') }}" method="POST" enctype="multipart/form-data" >
                            @csrf
                            <input class ="d-none" type="file" name="photo" id="fileInput">
                        </form>
                    @endif
                @endauth
            </div>
        </div>

        <div class="row bottom-mrg">
            <div class="col-md-12 col-sm-12">
                <div class="advance-detail detail-desc-caption">
                    <div class="stars">
                        @for ($i = 0; $i < $user->stars(); $i++)
                            <i class="fa fa-star fill"></i>
                        @endfor
                        @for ($i = 0; $i < (5 - $user->stars()); $i++)
                            <i class="fa fa-star"></i>
                        @endfor
                    </div>
                    <span>
                        <i class="fa fa-map-marker"></i>
                        {{ $user->state.', '.$user->city  }}
                    </span>
                    <h4>{{ $user->name }}</h4>
                    <span>{{ $user->type }}</span>   
                </div>
            </div>
        </div>
        <div class="row bottom-mrg">
            <div class="col-md-12 col-sm-12">
                <div class="advance-detail detail-desc-caption">
                    <ul>
                        <li><strong class="j-view">{{ count($user->jobs) }}</strong>Trabajos Ofrecidos</li>
                        <li><strong class="j-applied">{{count($user->services)}}</strong>Negocios Ofrecidos</li>
                        <li><strong class="j-shared">{{$user->acceptingPostulations()}}</strong>Trabajos Completados</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="fix-section-background">
    <div class="container">
        @include('commons.msg')
        @auth
            @if ($user->id == Auth::user()->id)
                <div class="col-md-8 col-sm-8">
            @else
                <div class="col-md-12 col-sm-12">
            @endif
        @else
            <div class="col-md-12 col-sm-12">
        @endauth
        
            <div class="sidebar-wrapper" id="about">
                <div class="sidebar-box-header bb-1">
                    @auth
                        @if ($user->id == Auth::user()->id)
                            <div class="edit-button">
                                <a  @click="myAbout"  href="javascript:void(0)" title="edit" class="detail-edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                            @if ($user->id == Auth::user()->id)
                            <h4>Acerca de mí</h4>
                            @else
                                <h4>Acerca de {{$user->name}} </h4>
                            @endif
                        @endif
                    @else
                        <h4>Acerca de {{$user->name}} </h4>
                    @endauth
                   
                </div>
                <div  v-bind:class="{ hidden: isActive }">
                    @if ($user->type != 'Empresa/Negocio' && $user->curriculum)
                        <ul class="detail-list">
                            <li>{{ $user->type }}</li>
                            <li>{{ $user->curriculum->genere }}</li>
                            <li>{{ $user->curriculum->age }} años</li>
                            <li>{{ $user->curriculum->description }}</li>
                        </ul>
                    @elseif($user->type == 'Empresa/Negocio' && $user->company)
                        <ul class="detail-list">
                            <li>{{ $user->type }}</li>
                            <li>{{ $user->company->name }}</li>
                            <li>{{ $user->company->turn }}</li>
                            <li>{{ $user->company->description }}</li>
                        </ul>
                    @endif

                </div>
                @auth
                    @if ($user->id == Auth::user()->id)
                        <div class="hide" v-bind:class="{ active: isActive }" >
                            <form action="{{ route('profile.updateAbout') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <input id="type" type="hidden" value="{{ $user->type }}">
                                        <select name="type" id="whoAre" class="form-control">
                                            <option value="">¿Qué erés?</option>
                                            <option value="Adulto Mayor">Adulto Mayor</option>
                                            <option value="Desempleado">Desempleado</option>
                                            <option value="Empresa/Negocio">Empresa/Negocio</option>
                                            <option value="Estudiante">Estudiante</option>
                                            <option value="Madre/Padre Soltero">Madre/Padre Soltero</option>
                                            <option value="Trabajador">Trabajador</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row hide" id="openCompany">
                                    <div class="col-md-6 col-sm-12">
                                        <input id="company" name="company" type="text" value="{{ $user->company ? $user->company->name : '' }}" placeholder="Nombre de la empresa" class="form-control">
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <input id="turn" name="turn" type="text" value="{{ $user->company ? $user->company->turn : '' }}" placeholder="Giro" class="form-control">
                                    </div>
                                </div>
                                <div class="row hide" id="openJob">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="card-body">
                                            <h4 class="card-title">¿Tienes trabajo actual?</h4>
                                            <input value="{{ $user->curriculum ? $user->curriculum->isWork : '' }}" type="hidden" name="" id="isWorkDefault">
                                            <fieldset class="radio">
                                                <label for="isWork1">
                                                    <input type="radio" id="isWork1" name="isWork" value="1"> Si, tengo trabajo
                                                </label>
                                            </fieldset>
                                            <fieldset class="radio">
                                                <label for="isWork0">
                                                    <input type="radio" id="isWork0" name="isWork" value="0" checked=""> No, soy desempleado
                                                </label>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hide" id="openGenere">
                                    <div class="col-md-6 col-sm-12">
                                        <input id="genereDefault" type="hidden" value="{{$user->curriculum ? $user->curriculum->genere : ''}}">
                                        <select id="genere" name="genere"  class="form-control">
                                            <option value="">Genero</option>
                                            <option value="Mujer">Mujer</option>
                                            <option value="Hombre">Hombre</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <input value="{{ $user->curriculum ? $user->curriculum->age : '' }}" name="age" type="number"  placeholder="Edad" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <textarea placeholder="Sobre mí" name="description" id="" rows="10" class="form-control" style="width: 100%;">{{ $user->type == 'Empresa/Negocio' ? ($user->company ? $user->company->description : '') : ($user->curriculum ? $user->curriculum->description : '') }}
                                        </textarea>
                                    </div> 
                                </div>
                                <div class="detail-pannel-footer-btn center">
                                    <button type="submit" href="javascript:void(0)" title="" class="footer-btn grn-btn">Guardar</button>
                                </div>
                            </form>
                        </div>
                    @endif
                @endauth
                
            </div>


            <div class="sidebar-wrapper">
                <div class="sidebar-box-header bb-1">
                    <h4>Calificaciones</h4>
                </div>
                   
                @foreach ($user->qualifications as $qualify)
                    <article class="advance-search-job">
                        <div class="row no-mrg">
                            <div class="col-md-6 col-sm-6">
                                <a href="/profile/{{ $qualify->id }}" title="job Detail">
                                    <div class="advance-search-img-box">
                                    <img style="width: 72px;height: 72px;" src="{{ asset('img/users/'.$qualify->author->photo) }}" class="img-responsive img-circle" alt="">                                    
                                </div>
                                </a>
                                <div class="advance-search-caption">
                                    <a href="/profile/{{ $qualify->author->id }}" title="Job Dtail">
                                        <h4>{{$qualify->author->username}}</h4>
                                    </a>
                                    <span class="cl-success">{{$qualify->author->type}}</span>
                                    <p>{{$qualify->comment}}</p>
                                </div>
                            </div>
                            
                            <div class="col-md-6 col-sm-6">
                                <div class="stars pull-right">
                                    @for ($i = 0; $i < $user->stars(); $i++)
                                        <i class="fa fa-star fill"></i>
                                    @endfor
                                    @for ($i = 0; $i < (5 - $user->stars()); $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                </div>
                            </div>
                           
                        </div>
                        
                    </article>
                @endforeach
            </div>


        </div>
        @auth
            @if ($user->id == Auth::user()->id)
            <!-- Sidebar Start-->
            <div class="col-md-4 col-sm-4">
                <div class="sidebar-widgets">
                    <div class="ur-detail-wrap" id="contact">
                        <div class="ur-detail-wrap-header">
                            <div class="edit-button">
                                <a  @click="myContact" href="javascript:void(0)" title="edit" class="detail-edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                            <h4>Datos de contacto</h4>
                        </div>
                        <div class="ur-detail-wrap-body" >
                            <div v-bind:class="{ hidden: isActive }">
                                <ul class="ove-detail-list">
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <h5>Nombre:</h5>
                                        <span>{{ $user->name.' '.$user->lastName }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <h5>Telefóno:</h5>
                                        <span>{{ $user->phone }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        <h5>Dirección:</h5>
                                        <span>{{ $user->address }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map"></i>
                                        <h5>Estado:</h5>
                                        <span>{{ $user->state }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map"></i>
                                        <h5>Ciudad:</h5>
                                        <span>{{ $user->city }}</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="hide" v-bind:class="{ active: isActive }">
                                <form action="{{ route('profile.updateContact') }}" method="post">
                                    @csrf
                                    <ul class="ove-detail-list">
                                        <li>
                                            <input value="{{ $user->phone }}" name="phone" type="number"  placeholder="Telefóno" class="form-control">
                                        </li>
                                        <li>
                                            <input value="{{ $user->address }}" name="address" value="{{ old('adress') }}" type="text" class="form-control" placeholder="Dirección (Opcional)">
                                        </li>
                                        <li>
                                            <input value="{{ $user->city }}" type="text" name="city"  placeholder="Ciudad" class="form-control">
                                        </li>
                                        <li>
                                            <select  id="state" class="form-control" name="state" required autofocus>
                                                <option value="Aguascalientes">Aguascalientes</option>
                                                <option value="Baja California">Baja California</option>
                                                <option value="Baja California Sur">Baja California Sur</option>
                                                <option value="Campeche">Campeche</option>
                                                <option value="Coahuila">Coahuila</option>
                                                <option value="Colima">Colima</option>
                                                <option value="Chiapas">Chiapas</option>
                                                <option value="Chihuahua">Chihuahua</option>
                                                <option value="CDMX">CDMX</option>
                                                <option value="Durango">Durango</option>
                                                <option value="Guanajuato">Guanajuato</option>
                                                <option value="Guerrero">Guerrero</option>
                                                <option value="Hidalgo">Hidalgo</option>
                                                <option value="Jalisco">Jalisco</option>
                                                <option value="México">México</option>
                                                <option value="Michoacán">Michoacán</option>
                                                <option value="Morelos">Morelos</option>
                                                <option value="Nayarit">Nayarit</option>
                                                <option value="Nuevo León">Nuevo León</option>
                                                <option value="Oaxaca">Oaxaca</option>
                                                <option value="Puebla">Puebla</option>
                                                <option value="Querétaro">Querétaro</option>
                                                <option value="Quintana Roo">Quintana Roo</option>
                                                <option value="San Luis Potosí">San Luis Potosí</option>
                                                <option value="Sinaloa">Sinaloa</option>
                                                <option value="Sonora">Sonora</option>
                                                <option value="Tabasco">Tabasco</option>
                                                <option value="Tamaulipas">Tamaulipas</option>
                                                <option value="Tlaxcala">Tlaxcala</option>
                                                <option value="Veracruz">Veracruz</option>
                                                <option value="Yucatán">Yucatán</option>
                                                <option value="Zacatecas">Zacatecas</option>
                                            </select> 
                                        </li>
                                        <li>
                                            <div class="detail-pannel-footer-btn center">
                                                <button type="submit" href="javascript:void(0)" title="" class="footer-btn grn-btn">Guardar</button>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-widgets">
                    <div class="ur-detail-wrap" id="account">
                        <div class="ur-detail-wrap-header">
                            <div class="edit-button">
                                <a  @click="myAccount" href="javascript:void(0)" title="edit" class="detail-edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                            <h4>Mi Cuenta</h4>
                        </div>
                        <div class="ur-detail-wrap-body">
                            <div v-bind:class="{ hidden: isActive }">
                                <ul class="ove-detail-list">
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <h5>Usuario:</h5>
                                        <span>{{ $user->username }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <h5>Nombre:</h5>
                                        <span>{{ $user->name }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <h5>Apellido:</h5>
                                        <span>{{ $user->lastName }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <h5>Correo:</h5>
                                        <span>{{ $user->email }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-key"></i>
                                        <h5>Contraseña:</h5>
                                        <span>*******</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="hide" v-bind:class="{ active: isActive }">
                                <form action="{{ route('profile.updateAccount') }}" method="post">
                                    @csrf
                                    <ul class="ove-detail-list">
                                        <li>
                                            <input value="{{ $user->username }}" name="username" type="text"  placeholder="Usuario" class="form-control">
                                        </li>
                                        <li>
                                            <input value="{{ $user->name }}" name="name" type="text"  placeholder="Nombre" class="form-control">
                                        </li>
                                        <li>
                                            <input value="{{ $user->lastName }}" name="lastName" type="text"  placeholder="Apellido" class="form-control">
                                        </li>
                                        <li>
                                            <input value="{{ $user->email }}" name="email" type="text"  placeholder="Correo Electrónico" class="form-control">
                                        </li>
                                        <li>
                                            <input value="" name="password" type="password"  placeholder="Ingresa una nueva contraseña" class="form-control">
                                        </li>
                                        <li>
                                            <input value="" name="password_confirmation" type="password"  placeholder="Confirmar nueva contraseña" class="form-control">
                                        </li>
                                        <li>
                                            <div class="detail-pannel-footer-btn center">
                                                <button type="submit" href="javascript:void(0)" title="" class="footer-btn grn-btn">Guardar</button>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sidebar-widgets">
                    <div class="ur-detail-wrap" id="socials">
                        <div class="ur-detail-wrap-header">
                            <div class="edit-button">
                                <a  @click="mySocial" href="javascript:void(0)" title="edit" class="detail-edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                            <h4>Redes Sociales</h4>
                        </div>
                        <div class="ur-detail-wrap-body">
                            <div v-bind:class="{ hidden: isActive }">
                                <ul class="ove-detail-list">
                                    <li>
                                        <i class="fa fa-whatsapp"></i>
                                        <h5>WhatsApp:</h5>
                                        <span>{{ $user->whats }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-facebook"></i>
                                        <h5>Facebook:</h5>
                                        <span>/{{ $user->fb }}</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-instagram"></i>
                                        <h5>Instagram:</h5>
                                        <span>/{{ $user->insta }}</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="hide" v-bind:class="{ active: isActive }">
                                <form action="{{ route('profile.updateSocials') }}" method="post">
                                    @csrf
                                    <ul class="ove-detail-list">
                                        <li>
                                            <input value="{{ $user->whats }}" name="whats" type="number"  placeholder="WhatsApp" class="form-control">
                                        </li>
                                        <li>
                                            <input value="{{ $user->fb }}" name="fb" type="text"  placeholder="Facebook" class="form-control">
                                        </li>
                                        <li>
                                            <input value="{{ $user->insta }}" name="insta" type="text"  placeholder="Instagram" class="form-control">
                                        </li>
                                        <li>
                                            <div class="detail-pannel-footer-btn center">
                                                <button type="submit" href="javascript:void(0)" title="" class="footer-btn grn-btn">Guardar</button>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endauth
        
    <!-- End Sidebar -->
</section>
<!-- Modal -->
<div class="modal fade" id="myModalMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Ubicación</h5>
            <p>(Arrastra o Busca tu dirección)</p>
            <button type="button" class="close closeModal" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
                <div id="myMap"></div>
                <div class="col-md-12 col-sm-12 bt mb-1">
                    <label for="">Dirección:</label>
                    <div class="input-group">
                        <input  class="form-control" id="address" type="text" />
                        <span class="input-group-btn">
                            <button id="submit" type="button" class="btn btn-primary">Buscar</button>
                        </span>
                    </div>
                </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary closeModal">Guardar</button>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/js/bootstrap.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('#addPhoto').click(function(){
                $('#fileInput').trigger('click');
            });

            $('#fileInput').on('change', function() {
                $('.Loader').addClass('show');
                readUrl(this, $('#avatarFile'));

                var form = $('#photoForm');
                var url = form.attr('action');
                var token = $('#csrf-token-id').attr('content');

                var fd = new FormData();
                var files = $('#fileInput')[0].files[0];
                fd.append('photo',files);

                
                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': token},
                    url: url,
                    data: fd,
                    contentType:false,
                    processData: false,
                    success: function(response)
                    {
                        $('.Loader').removeClass('show');
                    }
                });
                
            });

            function readUrl(inputImage, showImage){
                var oFReader = new FileReader();
                
                oFReader.readAsDataURL(inputImage.files[0]);
                oFReader.onload = function (oFREvent) {
                    showImage.attr('src', oFREvent.target.result);
                };
            }

            $('#whoAre').on('change', function(){
                var optionSelected = $("option:selected", this);
                if(optionSelected.text() == 'Empresa/Negocio'){
                    $('#openCompany').removeClass('hide');
                    $('#openGenere').addClass('hide');
                }else{
                    $('#openCompany').addClass('hide');
                    $('#openGenere').removeClass('hide');
                }
                if(optionSelected.text() != 'Desempleado' && optionSelected.text() != 'Trabajador' && optionSelected.text() != 'Empresa/Negocio' ){
                    $('#openJob').removeClass('hide');
                }else{
                    $('#isWork0').prop('checked', true);
                    $('#openJob').addClass('hide');
                }
            });

            $("#whoAre").val($("#type").val());
            $('#genere').val($('#genereDefault').val());

            if($('#isWorkDefault').val() == '0'){
                $('#isWork0').attr('checked', 'checked');
            }else{
                $('#isWork1').attr('checked', 'checked');
            }

            if($("#type").val() == 'Empresa/Negocio'){
                $('#openCompany').removeClass('hide');
                $('#openGenere').addClass('hide');
            }else{
                $('#openCompany').addClass('hide');
                $('#openGenere').removeClass('hide');
            }

            if($("#type").val() != 'Trabajador' && $("#type").val() != 'Empresa/Negocio' ){
                $('#openJob').removeClass('hide');
            }else{
                $('#openJob').addClass('hide');
            }

        });

    </script>


    <script>
        new Vue({
            el: '#about',
        
            data: {
                isActive: false
            },
        
            methods: {
                myAbout: function(){
                    this.isActive = !this.isActive;
                }
            }
            });
        
        new Vue({
            el: '#contact',
        
            data: {
                isActive: false
            },
        
            methods: {
                myContact: function(){
                    this.isActive = !this.isActive;
                // some code to filter users
                }
            }
        });
        new Vue({
            el: '#socials',
        
            data: {
                isActive: false
            },
        
            methods: {
                mySocial: function(){
                    this.isActive = !this.isActive;
                // some code to filter users
                }
            }
        });
        new Vue({
            el: '#account',
        
            data: {
                isActive: false
            },
        
            methods: {
                myAccount: function(){
                    this.isActive = !this.isActive;
                // some code to filter users
                }
            }
        });
    </script>
   
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWUW7HZKqFhhWMdcvQh_x7Qw-62myS9Ko">
    </script>
    <script>
        $(document).ready(function() {
            var map, infoWindow;
            function initialize() {

            map = new google.maps.Map(document.getElementById('myMap'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            infoWindow = new google.maps.InfoWindow;
            var geocoder = new google.maps.Geocoder();

            var pos = {
                lat: 0,
                lng: 0
            };


            marker = new google.maps.Marker({
                map: map,
                position: pos,
                draggable: true,
            });

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                marker.setPosition(pos);
                infoWindow.open(map);
                map.setCenter(pos);


                geocoder.geocode({ 'latLng': pos }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });



            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
            } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
            }

            document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
            });

            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === 'OK') {
                        $('#address').val(results[0].formatted_address);
                        $('#addressForm').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        resultsMap.setCenter(results[0].geometry.location);
                        infoWindow.setContent(results[0].formatted_address);
                        marker.setPosition(results[0].geometry.location);
                        infoWindow.open(map, marker);

                    } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

        

            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    });

    </script>

    <script>
        $('#selectCategory').change(function(){
            var optionSelected = $("option:selected", this);
            if(optionSelected.text() == 'Otra'){
                $('#openOther').addClass('show');
            }else{
                $('#openOther').removeClass('show');
            }
        });
    </script>
    
    <script>
        $('.openModal').click(function(){
            $('#myModalMap').addClass('d-block');

        });

        $('.closeModal').click(function(){
            $('#myModalMap').removeClass('d-block');

        });

        new Vue({
        el: '#picUser',
        data:{
            url: '/img/jobs/default.png',
        },
            methods: {
                trigger () {
                    this.$refs.fileInput.click()
                },
                onFileChange(e) {
                    const file = e.target.files[0];
                    this.url = URL.createObjectURL(file);
                }
            }
        });
    </script>

@endsection