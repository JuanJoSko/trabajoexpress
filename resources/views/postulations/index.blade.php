@extends('layouts.app')


@section('css')
<style amp-custom>
        .rating {
          --star-size: 3;  /* use CSS variables to calculate dependent dimensions later */
          padding: 0;  /* to prevent flicker when mousing over padding */
          border: none;  /* to prevent flicker when mousing over border */
          unicode-bidi: bidi-override; direction: rtl;  /* for CSS-only style change on hover */
          text-align: left;  /* revert the RTL direction */
          user-select: none;  /* disable mouse/touch selection */
          font-size: 3em;  /* fallback - IE doesn't support CSS variables */
          font-size: calc(var(--star-size) * 1em);  /* because `var(--star-size)em` would be too good to be true */
          cursor: pointer;
          /* disable touch feedback on cursor: pointer - http://stackoverflow.com/q/25704650/1269037 */
          -webkit-tap-highlight-color: rgba(0,0,0,0);
          -webkit-tap-highlight-color: transparent;
          margin-bottom: 1em;
        }
        /* the stars */
        .rating > label {
          display: inline-block;
          position: relative;
          width: 1.1em;  /* magic number to overlap the radio buttons on top of the stars */
          width: calc(var(--star-size) / 3 * 1.1em);
        }
        .rating > *:hover,
        .rating > *:hover ~ label,
        .rating:not(:hover) > input:checked ~ label {
          color: transparent;  /* reveal the contour/white star from the HTML markup */
          cursor: inherit;  /* avoid a cursor transition from arrow/pointer to text selection */
        }
        .rating > *:hover:before,
        .rating > *:hover ~ label:before,
        .rating:not(:hover) > input:checked ~ label:before {
          content: "★";
          position: absolute;
          left: 0;
          color: gold;
        }
        .rating > input {
          position: relative;
          transform: scale(3);  /* make the radio buttons big; they don't inherit font-size */
          transform: scale(var(--star-size));
          /* the magic numbers below correlate with the font-size */
          top: -0.5em;  /* margin-top doesn't work */
          top: calc(var(--star-size) / 6 * -1em);
          margin-left: -2.5em;  /* overlap the radio buttons exactly under the stars */
          margin-left: calc(var(--star-size) / 6 * -5em);
          z-index: 2;  /* bring the button above the stars so it captures touches/clicks */
          opacity: 0;  /* comment to see where the radio buttons are */
          font-size: initial; /* reset to default */
        }
        form.amp-form-submit-error [submit-error] {
          color: red;
        }
      </style>
    
@endsection

@section('content')

<!-- Title Header Start -->
<section class="inner-header-title" style="background-image:url({{ asset('/img/banners/banner_job.jpeg') }});">
    <div class="container">
        <h1>MIS POSTULACIONES</h1>
    </div>
</section>
<div class="clearfix"></div>


<!-- Candidate Profile Start -->
<section class="detail-desc advance-detail-pr gray-bg">
        <div class="container white-shadow">
            <div class="row bottom-mrg">
                <div class="col-md-12 col-sm-12">
                    <div class="advance-detail detail-desc-caption">
                        <ul>
                            <li><strong class="j-view">{{ Auth::user()->cancelPostulations() }}</strong>Canceladas</li>
                            <li><strong class="j-applied">{{Auth::user()->pendingPostulations()}}</strong>Pendientes</li>
                            <li><strong class="j-shared">{{Auth::user()->acceptingPostulations()}}</strong>Aceptadas</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="full-detail-description full-detail gray-bg">
        <div class="container">
            <div class="col-md-12 col-sm-12">
                @include('commons.msg')
                <div class="">
                  <div class="deatil-tab-employ tool-tab">
                        <ul class="nav simple nav-tabs" id="simple-design-tab">
                            <li class="active"><a href="#allPostuls">Todas</a></li>
                            <li><a href="#address">Pendientes</a></li>
                            <li><a href="#matches-job">Aceptadas</a></li>
                            <li><a href="#friends">Canceladas</a></li>
                        </ul>
                        
                        <!-- Start All Sec -->
                        <div class="tab-content">
                            <!-- Start Friend List -->
                            <div id="allPostuls" class="tab-pane  active">
                                <div class="row">
                                    @foreach ($postulations as $postulation) 
                                        <div class="col-md-4 col-sm-4">
                                            <div class="manage-cndt">
                                                <input type="hidden" class="photoUser" value="{{$postulation->user->photo}}">
                                                <input type="hidden" class="postulationID" value="{{$postulation->pivot->id}}">
                                                <input type="hidden" class="nameUser" value="{{$postulation->user->username}}">
                                                <input type="hidden" class="userID" value="{{$postulation->user->id}}">
                                                @if ($postulation->pivot->status == 'Aceptado' && $postulation->pivot->starsEmployee() == 0 )
                                                    <span class="starsBtn part-time quali-time cursor"><i class="fa fa-star"></i> Calificar</span>
                                                @endif
                                                <span style="    left: -4px;" class="tg-themetag tg-featuretag {{ ($postulation->pivot->status == 'Cancelada'  ? 'tg-cancel' : ( $postulation->pivot->status == 'Pendiente'  ? 'tg-warning' : 'tg-success') ) }}">{{$postulation->pivot->status}}</span>
                                                <div class="cndt-caption">
                                                    <div class="cndt-pic">
                                                            @empty($postulation->photo)
                                                            @if ($postulation->category->name != 'Express')
                                                                <i class="cat-{{ $postulation->category->icon }} cat-photo-detail"></i>
                                                            @else
                                                                <i class="icon-express" style="    font-size: 3em;
                                                                position: relative;
                                                                top: 18px;
                                                                right: 0;
                                                                color: white;
                                                                cursor: pointer;" ></i>
                                
                                                            @endif
                                                        @else
                                                            <img src="{{ asset('/img/jobs/'.$postulation->photo) }}" class="img-responsive" alt="">
                                                        @endempty
                                                    </div>
                                                    <div class="rateing">
                                                        @if(count($postulation->pivot->stars) > 0 )
                                                                @for ($i = 0; $i < $postulation->pivot->stars[0]->points; $i++)
                                                                <i class="fa fa-star filled"></i>
                                                                @endfor
                                                                @for ($i = 0; $i < (5 -  $postulation->pivot->stars[0]->points); $i++)
                                                                    <i class="fa fa-star"></i>
                                                                @endfor
                                                        @endif
                                                    </div>

                                                    <span>{{$postulation->user->state.', '.$postulation->user->city}}</span>
                                                    <h4>{{$postulation->title}}</h4>
                                                    <p>{{str_limit( $postulation->description, 32)}}</p>
                                                    @if ($postulation->pivot->status == 'Aceptado')
                                                        <ul class="employee-social">
                                                            @if($postulation->user->phone != null)
                                                                <li><a href="tel:{{ $postulation->user->phone }}" title=""><i class="fa fa-phone"></i></a></li>
                                                            @endif
                                                            @if($postulation->user->email != null)
                                                                <li><a href="mailto:{{ $postulation->user->email }}" title=""><i class="fa fa-envelope"></i></a></li>
                                                            @endif
                                                            @if($postulation->user->fb != null)
                                                                <li><a href="https://www.facebook.com/{{ $postulation->user->fb }}" title=""><i class="fa fa-facebook"></i></a></li>
                                                            @endif
                                                            @if($postulation->user->whats != null)
                                                                <li><a href="https://web.whatsapp.com/send?phone=+52{{ $postulation->user->whats }}" title=""><i class="fa fa-whatsapp"></i></a></li>
                                                            @endif
                                                            @if($postulation->user->insta != null)
                                                                <li><a href="https://www.instagram.com/{{ $postulation->user->insta }}" title=""><i class="fa fa-instagram"></i></a></li>
                                                            @endif
                                                        </ul>
                                                    @endif
                                                </div>
                                                <a  href="/job/{{$postulation->slug}}" title="" class="cndt-profile-btn">Ver Trabajo</a>
                                               
                                            </div>
                                        </div>
                                    @endforeach
                            </div>
                            
                        </div>
                        <!-- Start All Sec -->
                    </div>  
                </div>
            </div>
        </div>
    </section>
    <!-- Candidate Profile End -->

    <div id="starsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titleModal">Califica a JuanJO</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form id="idForm" method="POST" action="{{ route('qualification.store') }}">
                    <input id="user_idModal" type="hidden" name="user_id" value="">
                    <input id="postulation_idModal" type="hidden" name="postulation_id" value="">
                    <input type="hidden" name="type" value="Employee">
                    <input type="hidden" name="author_id" value="{{ Auth::user()->id }}">

                    <div class="modal-body">
                        <div class="user-stars">
                            <img id="imgModal" src="http://trabajoexpress.loc/img/jobs/1562256822300_angel_hor_3.jpg" class="img-responsive" alt="">
                        </div>

                        <div class="form-stars">
                            <p class="clasificacion">
                                <input id="radio1" type="radio" name="points" value="5">
                                <label class="cursor label-star" for="radio1">★</label>
                                <input id="radio2" type="radio" name="points" value="4">
                                <label class="cursor label-star" for="radio2">★</label>
                                <input id="radio3" type="radio" name="points" value="3">
                                <label class="cursor label-star" for="radio3">★</label>
                                <input id="radio4" type="radio" name="points" value="2">
                                <label class="cursor label-star" for="radio4">★</label>
                                <input id="radio5" type="radio" name="points" value="1">
                                <label class="cursor label-star" for="radio5">★</label>
                            </p>
                            <textarea  placeholder="Deja un comentario (Opcional)" name="comment" class="form-control" rows="10"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" data-dismiss="modal">Envíar</button>
                    </div>
                    </form>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    
@endsection

@section('scripts')
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>   

    <script src="{{ asset('/js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/js/bootstrap.min.js') }}"></script>
    <script>
        var currentStarBtn;
        $('.starsBtn').click(function(){
            currentStarBtn = $(this);
            var urlImg = $(this).closest('.manage-cndt').find('.photoUser').val();
            $('#imgModal').attr('src', '/img/users/'+urlImg);
            var userName = $(this).closest('.manage-cndt').find('.nameUser').val();
            $('#titleModal').text('Califica a '+userName);
            var userID = $(this).closest('.manage-cndt').find('.userID').val();
            $('#user_idModal').val(userID);
            var postulationID = $(this).closest('.manage-cndt').find('.postulationID').val();
            $('#postulation_idModal').val(postulationID);
            $('#starsModal').show();
            
        });

        // this is the id of the form
        $("#idForm").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');
            var token = $('#csrf-token-id').attr('content');

            $.ajax({
                type: "POST",
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                data: form.serialize(), // serializes the form's elements.
                success: function(response)
                {
                    if(response.status == 'save'){
                        $('#starsModal').hide();
                        currentStarBtn.closest('.starsBtn').remove();
                        currentStarBtn.remove();
                    }else{
                        alert('Upps! error inesperado, disculpa las molestías por favor intenta de nuevo');
                    }
                }
                });


        });


    </script>

@endsection