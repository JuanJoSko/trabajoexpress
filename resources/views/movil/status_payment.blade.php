
@extends('layouts.app_movil')

@section('css')
{{-- <link href="{{ asset('css/conekta.css') }}" rel="stylesheet"> --}}
@endsection

@section('content')
<!-- Header Title Start -->
<section class="inner-header-title"  style="background-image:url({{ asset('/img/banners/banner_payment.jpg') }});">
<div class="container">
<h1>Estatus del Pago</h1>
</div>
</section>
<div class="clearfix"></div>
<!-- Header Title End -->

<!-- General Detail Start -->
<div class="detail-desc section">
    <div class="container white-shadow" style="padding-left: 0px; padding-right: 0px;">
        @include('commons.msg')
        @include('commons.errors')
            <div class="sttabs tabs-style-circle" style="margin: auto;box-shadow: 2px 4px 13px #9c9898;height:auto;">
                <nav>
                    <ul style="border-bottom: 2px solid #10c18e;">
                        <li ><a class="sticon fa fa-exclamation-triangle"><span>{{$status}}</span></a></li>
                    </ul>
                </nav>
                <div class="content-wrap text-center">
                    <section id="section-circle-1">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            @switch($status)
                                @case('Pagado')
                                    <h3>Pago Procesado</h3>
                                    <p>Ahora ya puedes regresar a la aplicación.</p>
                                @break
                                @case('Error')
                                    <h3>Error en Pago</h3>
                                    <p>Error inesperado al procesar tu pago, por favor intenta de nuevo o más tarde.</p>
                                @break
                            @endswitch
                        </div>
                    </section>
                </div>
                <!-- /content -->
            </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

<script type="text/javascript" >
    Conekta.setPublicKey('key_ZRzs1X6jBx8roh9YTFCZCiw');
  
    var conektaSuccessResponseHandler = function(token) {
      var $form = $("#card-form");
      //Inserta el token_id en la forma para que se envíe al servidor
       $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
      $form.get(0).submit(); //Hace submit
    };
    var conektaErrorResponseHandler = function(response) {
      var $form = $("#card-form");
      $form.find(".card-errors").text(response.message_to_purchaser);
      $form.find("button").prop("disabled", false);
    };
  
    //jQuery para que genere el token después de dar click en submit
    $(function () {
      $("#card-form").submit(function(event) {
        var $form = $(this);
        // Previene hacer submit más de una vez
        $form.find("button").prop("disabled", true);
        Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
        return false;
      });
    });
  </script>

<script>
    new Vue({
        el: '#section-circle-3',

        data: {
            isActive: false
        },

        methods: {
            myCard: function(){
                this.isActive = !this.isActive;
            // some code to filter users
            }
        }
    });
</script>

<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('js/payment.js') }}"></script>

<script type="text/javascript">
    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
    })();
</script>



@endsection