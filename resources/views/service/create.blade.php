@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
@endsection

@section('content')
<!-- Header Title Start -->
<section class="inner-header-title"  style="background-image:url({{ asset('/img/banners/banner_service.jpg') }});">
    <div class="container">
        <h1>Ofrecer Negocio</h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Header Title End -->

<!-- General Detail Start -->
<div class="detail-desc section">
    <div class="container white-shadow">
    
        <form method="POST" class="add-feild" action="{{ route('service.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="detail-pic" id ="picUser">
                        {{-- src="{{ asset('/img/can-1.png') }} --}}
                    <img v-if="url" :src="url"  class="img" alt="" />
                    <a @click="trigger" href="javascript:void(0)" class="detail-edit" title="edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <input class ="d-none" type="file" name="photo" @change="onFileChange"  ref="fileInput">
                </div>
            </div>

            @include('commons.msg')
            @include('commons.errors')
            
            <div class="row bottom-mrg" id="formJob">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input  value="{{ old('title') }}" maxlength="32" required name="title" type="text" class="form-control" placeholder="Nombre de tu Empresa/Negocio">
                    </div>
                </div>
                
                {{-- <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <select required id="selectCategory" name="category_id" class="form-control input-lg">
                            <option value="1">Categoría</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input id="categories" class="form-control" required placeholder="Giro de tu negocio">
                        <input type="hidden" name="category_id" id="categorSelected" >
                        {{-- <input data-autocomplete="{{ route('getCategories') }}" /> --}}
                    </div>
                </div>
                {{-- <div class="col-md-6 col-sm-6 hide" id="openOther">
                    <label >Especifica otra categoría que no este en la lista</label>
                    <div class="input-group">
                        <input  name="other" type="text" class="form-control" placeholder="Específica otra" value="{{ old('other') }}" requiered >
                    </div>
                </div> --}}
                <div class="col-md-12 col-sm-12">
                    <label >Dirección del servicio</label>
                    <div class="input-group">
                        <input  required id="addressForm" readonly  name="address" value="{{ old('adress') }}" type="text" class="form-control openModal" placeholder="Dirección del Servicio.">
                        <input  value="{{ old('latitude') }}" id="latitude" type="hidden" name="latitude">
                        <input  value="{{ old('longitude') }}" id="longitude" type="hidden" name="longitude">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <label >Contacto del servicio</label>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input  value="{{ old('phone') }}" oninput="maxLengthCheck(this, 10)"  name="phone" type="number" class="form-control" placeholder="Telefóno" required>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input value="{{ old('email') }}" name="email" type="email" class="form-control" placeholder="Correo Electrónico" >
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <textarea required name="description"  class="form-control" placeholder="Descripción">{{ old('description') }}"</textarea>
                </div>
            
                <div class="col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-success btn-primary small-btn">Guardar</button>	
                </div>	
                    
            </div>
        </form>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="myModalMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Ubicación</h5>
            <p>(Arrastra o Busca la dirección del trabajo)</p>
            <button type="button" class="close closeModal" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
                <div id="myMap"></div>
                <div class="col-md-12 col-sm-12 bt mb-1">
                    <label for="">Dirección:</label>
                    <div class="input-group">
                        <input  class="form-control" id="address" type="text" />
                        <span class="input-group-btn">
                            <button id="submit" type="button" class="btn btn-primary">Buscar</button>
                        </span>
                    </div>
                </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary closeModal">Guardar</button>
        </div>
        </div>
    </div>
    </div>
</div>
@include('modals.alert')

@endsection

@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
       $.ajax({
                url: "{{ route('getCategories') }}",
                type: "GET",
                dataType: "json",
                success: function( resp ) {
                    var data =  $.map(resp,function(val) {
                        return {
                            value: val.name ,
                            id: val.id,
                        };
                    });
                    $("#categories").autocomplete({
                        source: data,
                        minLength: 0,
                        select: function( event, ui ) {
                           $('#categorSelected').val(ui.item.id);
                           console.log($('#categorSelected').val());
                        }
                    });
                }
            });

    </script>

    <script>
        function maxLengthCheck(object, size) {
        if (object.value.length >size)
            object.value = object.value.slice(0, size)
        }
        $('#selectCategory').change(function(){
            var optionSelected = $("option:selected", this);
            if(optionSelected.text() == 'Otra'){
                $('#openOther').addClass('show');
            }else{
                $('#openOther').removeClass('show');
            }
        });
    </script>

    <script>
        $('.openModal').click(function(){
            $('#myModalMap').addClass('d-block');

        });

        $('.closeModal').click(function(){
            $('#myModalMap').removeClass('d-block');

        });


        new Vue({
        el: '#picUser',
        data:{
            url: '/img/services/service.png',
        },
            methods: {
                trigger () {
                    this.$refs.fileInput.click()
                },
                onFileChange(e) {
                    const file = e.target.files[0];
                    this.url = URL.createObjectURL(file);
                }
            }
        });
    </script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCklaCU8LI1jGPMNZweSImXr_wOn-0aMr4">
    </script>
    <script>
        $(document).ready(function() {
            var map, infoWindow;
            function initialize() {

            map = new google.maps.Map(document.getElementById('myMap'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            infoWindow = new google.maps.InfoWindow;
            var geocoder = new google.maps.Geocoder();

            var pos = {
                lat: 0,
                lng: 0
            };


            marker = new google.maps.Marker({
                map: map,
                position: pos,
                draggable: true,
            });

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                marker.setPosition(pos);
                infoWindow.open(map);
                map.setCenter(pos);


                geocoder.geocode({ 'latLng': pos }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });



            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
            } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
            }

            document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
            });

            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === 'OK') {
                        $('#address').val(results[0].formatted_address);
                        $('#addressForm').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        resultsMap.setCenter(results[0].geometry.location);
                        infoWindow.setContent(results[0].formatted_address);
                        marker.setPosition(results[0].geometry.location);
                        infoWindow.open(map, marker);

                    } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

        

            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    });

    </script>
    
@endsection