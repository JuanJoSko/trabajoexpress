@extends('layouts.app')


@section('content')

<section class="inner-header-title" style="background-position-y: 52%;  background-image:url({{ asset('/img/banners/banner_service.jpg') }});">
    <div class="container">
        <h1>{{$service->title}}</h1>
    </div>
</section>
<div class="clearfix"></div>
<section class="detail-desc">
        <div class="container">
            
            <div class="ur-detail-wrap top-lay">
                
                <div class="ur-detail-box">
                    <div class="ur-thumb">
                        <img src="{{ asset('/img/services/'.$service->photo)}}"  class="img-responsive" alt="">
                        </div>
                    <div class="ur-caption">
                        <h4 class="ur-title">{{$service->title}}</h4>
                        <p class="ur-location">
                            <div>
                                <img class="flag mx-img" src="{{ asset('/img/mx.png') }}" alt="">
                                {{$service->user->state}}, {{$service->user->city}}
                            </div>
                        <span class="ur-designation">{{$service->user->username}}</span>

                      
                        <div class="rateing">
                            @for ($i = 0; $i < $service->user->stars(); $i++)
                            <i class="fa fa-star filled"></i>
                            @endfor
                            @for ($i = 0; $i < (5 - $service->user->stars()); $i++)
                                <i class="fa fa-star"></i>
                            @endfor
                        </div>
                    </div>
                    
                </div>
                
                {{--  <div class="ur-detail-btn">
                    <div class="verified-action">Verificado</div>
                </div>  --}}
                <div class="fix-contact">
                    <ul class="employee-social">
                        <li>
                            <a href="tel:{{$service->phone}}" title="">
                                <i class="fa fa-phone"></i>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:{{$service->email}}" title="">
                                <i class="fa fa-envelope"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                
            </div>
            
        </div>
    </section>


<!-- Job Detail Start -->
<section class="fix-section-background">
    <div class="container">
        @include('commons.msg')
        <div class="col-md-8 col-sm-8">
            <div class="container-detail-box">
            
                <div class="apply-job-header">
                    <h4>{{$service->title}}</h4>
                    <a href="/profile/{{$service->user->id}}" class="cl-success"><span><i class="fa fa-building"></i>{{$service->user->username}}</span></a>
                    <span><i class="fa fa-map-marker"></i>{{$service->user->state.', '.$service->user->city}}</span>
                </div>
                
                <div class="apply-job-detail">
                    <p>{{$service->description}}</p>
                </div>
                
                @if (isset($service->requirements))
                    <div class="apply-job-detail">
                        <h5>Requerimientos:</h5>
                        <ul class="job-requirements">
                            <li><span>{{$service->requirements}}</span> </li>
                        </ul>
                    </div>
                @endif
            </div>
        </div>

        <!-- Sidebar Start-->
        <div class="col-md-4 col-sm-4">
            <div class="sidebar-widgets">
                <div class="ur-detail-wrap">
                    <div class="ur-detail-wrap-header">
                        <h4>Detalles del negocio</h4>
                    </div>
                    <div class="ur-detail-wrap-body">
                        <ul class="ove-detail-list">
                            <li>
                                <i class="fa fa-envelope"></i>
                                <h5>Correo</h5>
                                <span>{{$service->email}} hrs.</span>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <h5>Telefóno</h5>
                                <span>{{$service->phone}}</span>
                            </li>
                            @empty(!$service->address)
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <h5>Dirección</h5>
                                    <span>{{$service->address}}</span>
                                </li>
                            @endempty
                            <li>
                                <i class="fa fa-eye"></i>
                                <h5>Visitas</h5>
                                <span>{{$service->views}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Job Detail -->
            <div class="sidebar-container">
                <div class="sidebar-box">
                    {{-- <h4 class="flc-rate">20K - 30K</h4> --}}
                    <div class="sidebar-inner-box">
                        <div class="sidebar-box-thumb">
                            <img src="{{ asset('/img/users/'.$service->user->photo)}}" class="img-responsive" alt="" />
                        </div>
                            <div class="stars">
                                @for ($i = 0; $i < $service->user->stars(); $i++)
                                <i class="fa fa-star fill"></i>
                                @endfor
                                @for ($i = 0; $i < (5 - $service->user->stars()); $i++)
                                    <i class="fa fa-star"></i>
                                @endfor
                            </div>
                        </div>
                        
                        
                        <div class="sidebar-box-detail">
                            <h4>{{$service->user->username}}</h4>
                            {{-- <span class="desination">App Designer</span> --}}
                        </div>
                    </div>
                    <div class="sidebar-box-extra" s>
                       
                        <ul class="status-detail">
                            <li class="br-1"><strong>{{$service->user->state}}</strong>Ubicación</li>
                            <li class="br-1"><strong>{{$service->views}}</strong>Visitas</li>
                            <li><strong>{{$service->user->jobs->count()}}</strong>Trabajos</li>
                        </ul>
                    </div>
                </div>
                <a href="/profile/{{ $service->user->id }}" class="btn btn-sidebar bt-1 bg-success">Ver Perfil</a>
            </div>
            
            <!-- Share This Job -->
           
            
        </div>
        <!-- End Sidebar -->


    </div>
</section>
<!-- Job Detail End -->
    
@endsection