@extends('layouts.app')


@section('content')
    <section class="inner-header-title" style="background-image:url({{ asset('/img/banners/banner_auth.jpg') }});">
        <div class="container">
            <h1>Mis Negocios</h1>
        </div>
    </section>

    <section class="manage-employee gray">
        <div class="container">
                <!-- search filter -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        @include('commons.msg')
                        <div class="search-filter">
                        
                            {{--  <div class="col-md-4 col-sm-5">
                                <div class="filter-form">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar...">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default">Buscar</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="col-md-8 col-sm-7">
                                <div class="short-by pull-right">
                                    ordenar por
                                    <div class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">por fecha <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">por fecha</a></li>
                                        <li><a href="#">por vistas</a></li>
                                        <li><a href="#">por postulados</a></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>  --}}
                            
                        </div>
                    </div>
                </div>
            <div class="">
                <div class="deatil-tab-employ tool-tab">
                      <ul class="nav simple nav-tabs" id="simple-design-tab">
                          <li class="active"><a href="#allPostuls">Todos</a></li>
                          <li><a href="#address">Pendientes</a></li>
                          <li><a href="#matches-job">Aceptados</a></li>
                          <li><a href="#friends">Cancelados</a></li>
                      </ul>
                      
                      <!-- Start All Sec -->
                      <div class="tab-content">
                          <!-- Start Friend List -->
                          <div id="allPostuls" class="tab-pane  active">
                              <div class="row">
                                  @foreach ($services as $service) 
                                      <div class="col-md-4 col-sm-4 serviceContent" id="{{$service->id}}">
                                          <div class="manage-cndt">
                                            <div class="pull-right">
                                                <div class="btn-group action-btn fix-btn-action">
                                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#"><i class="fa fa-edit"></i> Editar</a></li>
                                                        <li><a class="deleteBtn" href="javascript:;"><i class="fa fa-trash"></i> Eliminar</a></li>
                                                        <li><a href="service/{{$service->slug}}"><i class="fa fa-eye"></i> Ver</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <span style="left: -5px;" class="tg-themetag tg-featuretag {{ ($service->status == 'Cancelada'  ? 'tg-cancel' : ( $service->status == 'Pendiente'  ? 'tg-warning' : 'tg-success') ) }}">{{$service->status}}</span>
                                            <div class="cndt-caption">
                                                  <div class="cndt-pic">
                                                          @empty($service->photo)
                                                          @if ($service->category->name != 'Express')
                                                              <i class="cat-{{ $service->category->icon }} cat-photo-detail"></i>
                                                          @else
                                                              <i class="icon-express" style="    font-size: 3em;
                                                              position: relative;
                                                              top: 18px;
                                                              right: 0;
                                                              color: white;
                                                              cursor: pointer;" ></i>
                              
                                                          @endif
                                                      @else
                                                          <img src="{{ asset('/img/services/'.$service->photo) }}" class="img-responsive" alt="">
                                                      @endempty
                                                  </div>
                                                  <span>{{$service->user->state.', '.$service->user->city}}</span>
                                                  <h4>{{$service->title}}</h4>
                                                  <p>{{str_limit( $service->description, 32)}}</p>

                                              </div>
                                              <a  href="service/{{$service->slug}}" title="" class="cndt-profile-btn">Ver Negocio</a>
                                             
                                          </div>
                                      </div>
                                  @endforeach
                          </div>
                          
                      </div>
                      <!-- Start All Sec -->
                  </div>  
              </div>
          </div>
            <div class="paginate-center">
                {!! $services->render() !!}
            </div>
        </div>
    </section>

@endsection


@section('scripts')
    <script>
        $('.deleteBtn').click(function(){
            var service_id = $(this).closest('.serviceContent').attr('id');
            var service = $(this).closest('.serviceContent');


            var url = "/service/delete";
            var token = $('#csrf-token-id').attr('content');
            
            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                data: { 'service_id': service_id},
                success: function(response){
                    service.remove();
                    alert(response);
                }
            });
        });
    </script>
@endsection