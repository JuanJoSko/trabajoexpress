@extends('layouts.app')


@section('content')
    <section class="inner-header-title" style="background-image:url({{ asset('/img/banners/banner_home.jpg') }});">
        <div class="container">
            <h1>Mis Trabajos</h1>
        </div>
    </section>

    <section class="manage-employee gray">
        <div class="container">
            @include('commons.msg')
                <!-- search filter -->
                {{-- <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="search-filter">
                        
                            <div class="col-md-4 col-sm-5">
                                 <div class="filter-form">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar...">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default">Buscar</button>
                                        </span>
                                    </div>
                                </div> 
                            </div>
                                
                            <div class="col-md-8 col-sm-7">
                                <div class="short-by pull-right">
                                    Mostrar:
                                    <div class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">todos <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="panelBtn" data-url ="my-jobs"><a href="javascript:;">Todos</a></li>
                                        <li class="panelBtn" data-url ="pending-jobs"><a href="javascript:;">Pendientes</a></li>
                                        <li class="panelBtn" data-url ="pay-jobs"><a href="javascript:;">Pagados</a></li>
                                        <li class="panelBtn" data-url ="cancel-jobs"><a href="javascript:;">Canceladas</a></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div> --}}
            <div class="">
                <div class="deatil-tab-employ tool-tab">
                      <!-- Start All Sec -->
                      <ul class="nav simple nav-tabs" id="simple-design-tab">
                            <li class="panelBtn active cursor" data-url ="my-jobs"><a href="javascript:;">Todos</a></li>
                            <li class="panelBtn" data-url ="pay-jobs"><a href="javascript:;">Pagados</a></li>
                            <li class="panelBtn" data-url ="accept-jobs"><a href="javascript:;">Aprobados</a></li>
                            <li class="panelBtn" data-url ="pending-jobs"><a href="javascript:;">Pendientes</a></li>
                            <li class="panelBtn" data-url ="working-jobs"><a href="javascript:;">Trabajando</a></li>
                            <li class="panelBtn" data-url ="cancel-jobs"><a href="javascript:;">Cancelados</a></li>
                        </ul>
                      <div class="tab-content">
                           <div id="alertMsg" class="hide">
                                @component('components.alertMsg', [
                                    'title' => 'Sin trabajos que mostrar',
                                    'img' => 'search.jpg',
                                    'urlBtn' => '/my-jobs',
                                    'titleBtn' => 'Regresar'
                                    ])
                                @endcomponent
                           </div>
                           

                          <!-- Start Friend List -->
                          <div class="tab-pane  active">
                              <div class="row jobs">
                                @include('job.jobs_myCatalog')
                              
                              </div>
                          </div>
                          
                      </div>
                      <!-- Start All Sec -->
                  </div>  
              </div>
          </div>
            <div class="paginate-center">
                {!! $jobs->render() !!}
            </div>
        </div>
    </section>

    <div id="starsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titleModal">Califica a JuanJO</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form id="idForm" method="POST" action="{{ route('qualification.store') }}">
                    <input id="user_idModal" type="hidden" name="user_id" value="">
                    <input type="hidden" name="author_id" value="{{ Auth::user()->id }}">
                    <input id="postulation_idModal" type="hidden" name="postulation_id" value="">
                    <input type="hidden" name="type" value="Employer">

                    <div class="modal-body">
                        <div class="user-stars">
                            <img id="imgModal" src="http://trabajoexpress.loc/img/jobs/1562256822300_angel_hor_3.jpg" class="img-responsive" alt="">
                        </div>

                        <div class="form-stars">
                            <p class="clasificacion">
                                <input id="radio1" type="radio" name="points" value="5">
                                <label class="cursor label-star" for="radio1">★</label>
                                <input id="radio2" type="radio" name="points" value="4">
                                <label class="cursor label-star" for="radio2">★</label>
                                <input id="radio3" type="radio" name="points" value="3">
                                <label class="cursor label-star" for="radio3">★</label>
                                <input id="radio4" type="radio" name="points" value="2">
                                <label class="cursor label-star" for="radio4">★</label>
                                <input id="radio5" type="radio" name="points" value="1">
                                <label class="cursor label-star" for="radio5">★</label>
                            </p>
                            <textarea  placeholder="Deja un comentario (Opcional)" name="comment" class="form-control" rows="10"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" data-dismiss="modal">Envíar</button>
                    </div>
                    </form>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

@endsection


@section('scripts')
    <script>
        $( document ).ready(function() {

            var route = "/my-jobs";
            var page;
            var filter;
            $(document).on('click', '.pagination a', function(e){
                e.preventDefault();
                page = $(this).attr('href').split('page=')[1];
            
                $.ajax({
                    url: route,
                    data: {
                        page: page,
                        filter: filter
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function(data){
                        $('.jobs').html(data);
                    }
                });
            });

            // $('.panelBtn').click(function(){
            //     $('.panelBtn').removeClass('active');
            //     $(this).addClass('active');
            //     route = '/'+$(this).attr('data-url');
            //     page = 1;
            //     $.ajax({
            //         url: route,
            //         data: {page: page},
            //         type: 'GET',
            //         dataType: 'json',
            //         success: function(data){
            //             $('.jobs').html(data);
            //         }
            //     });

            // });

            $('.panelBtn').click(function(){
                $('.panelBtn').removeClass('active');
                $(this).addClass('active');
                filter = $(this).attr('data-url');
                $.ajax({
                    url: route,
                    data: {
                        page: 1,
                        filter: filter
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function(data){
                        $('.jobs').html(data);
                        if(data == ''){
                            $('#alertMsg').removeClass('hide');
                        }else{
                            $('#alertMsg').addClass('hide');
                        }
                    }
                });

            });
        });
    </script>

    <script>
        $('.deleteBtn').click(function(){
            var job_id = $(this).closest('.jobContent').attr('id');
            var job = $(this).closest('.jobContent');


            var url = "/job/delete";
            var token = $('#csrf-token-id').attr('content');
            
            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                data: { 'job_id': job_id},
                success: function(response){
                    job.remove();
                    alert(response);
                }
            });
        });
    </script>

    <script>
            var currentStarBtn;
            $('.starsBtn').click(function(){
                currentStarBtn = $(this);
                var urlImg = $(this).closest('.manage-cndt').find('.photoUser').val();
                $('#imgModal').attr('src', '/img/users/'+urlImg);
                var userName = $(this).closest('.manage-cndt').find('.nameUser').val();
                $('#titleModal').text('Califica a '+userName);
                var userID = $(this).closest('.manage-cndt').find('.userID').val();
                $('#user_idModal').val(userID);
                var postulationID = $(this).closest('.manage-cndt').find('.postulationID').val();
                $('#postulation_idModal').val(postulationID);
                $('#starsModal').show();
                
            });
    
            // this is the id of the form
            $("#idForm").submit(function(e) {
    
                e.preventDefault(); // avoid to execute the actual submit of the form.
    
                var form = $(this);
                var url = form.attr('action');
                var token = $('#csrf-token-id').attr('content');
    
                $.ajax({
                    type: "POST",
                    url: url,
                    headers: {'X-CSRF-TOKEN': token},
                    data: form.serialize(), // serializes the form's elements.
                    success: function(response)
                    {
                        if(response.status == 'save'){
                            $('#starsModal').hide();
                            currentStarBtn.closest('.starsBtn').remove();
                            currentStarBtn.remove();
                        }else{
                            alert('Upps! error inesperado, disculpa las molestías por favor intenta de nuevo');
                        }
                    }
                    });
    
    
            });
    
    
        </script>
    
@endsection