@extends('layouts.app')

@section('scripts')

    <script>
        $('.confirmBtn').click(function(){
            var token = $('#csrf-token-id').attr('content');
            var article =  $(this).closest('article');
            var postulation_id = $(this).closest('article').attr('id');
            var confirmation = $(this).attr('data-confirmation');

            var url = '/confirmation';
            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                async: true,
                data: { 
                    'postulation_id': postulation_id, 
                    'confirmation': confirmation
                },
                success: function(response){
                    article.addClass('animated fadeOutRight');
                    function deleteArticle(){
                        if(confirmation == 'Aceptado'){
                            $('#congrats-modal').addClass('show');
                            $('.showPerfil').attr('href','/profile/'+response.user_id);
                        }
                        article.remove();
                    }
                    setTimeout(deleteArticle, 500);
                    
                }
            });
        });
        
    </script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js'></script>
    <script src="{{ asset('js/congratulations.js') }}"></script>
    
@endsection



@section('content')
<div class="modal fade " id="congrats-modal">
    <div class="congrats-cotent">       
        <div id='congrats' style="z-index: 99; background: white">
            <h1 id='title'>¡Felicidades!</h1>
            <p>Has aceptado un postulado</p>
            <a id="showPostulation" href="" class="btn btn-success showPerfil">Ver perfil</a>
        </div>
    </div>
</div>

<section class="inner-header-title" style="background-position-y: 32%; background-image:url({{ asset('/img/banners/banner_home.jpg') }});">
    <div class="container">
        <h1>{{$job->title}}</h1>
    </div>
</section>
<div class="clearfix"></div>
<section class="detail-desc">
        <div class="container">
            
            <div class="ur-detail-wrap top-lay">
                
                <div class="ur-detail-box">
                    <div class="ur-thumb">
                        <img src="{{ asset('/img/jobs/'.$job->photo)}}"  class="img-responsive" alt="">
                        </div>
                    <div class="ur-caption">
                        <h4 class="ur-title">{{$job->title}}</h4>
                        <p class="ur-location">
                            <div>
                                <img class="flag mx-img" src="{{ asset('/img/mx.png') }}" alt="">
                                {{$job->user->state}}, {{$job->user->city}}
                            </div>
                        <span class="ur-designation">{{$job->user->username}}</span>

                      
                        <div class="rateing">
                            @for ($i = 0; $i < $job->user->stars(); $i++)
                            <i class="fa fa-star filled"></i>
                            @endfor
                            @for ($i = 0; $i < (5 - $job->user->stars()); $i++)
                                <i class="fa fa-star"></i>
                            @endfor
                        </div>
                    </div>
                    
                </div>
                
                {{--  <div class="ur-detail-btn">
                    <div class="verified-action">Verificado</div>
                </div>  --}}
                
            </div>
            
        </div>
    </section>


<!-- Job Detail Start -->
<section class="fix-section-background">
    <div class="container">
        @include('commons.msg')
        <div class="col-md-8 col-sm-8">
            <div class="container-detail-box">
            
                <div class="apply-job-header">
                    <span class="sidebar-payment"><i class="fa fa-money"></i> ${{$job->payment}} MXN</span>
                    <h4>{{$job->title}}</h4>
                    <a href="/profile/{{ $job->user->id }}" class="cl-success"><span><i class="fa fa-user"></i>{{$job->user->username}}</span></a>
                    <span><i class="fa fa-map-marker"></i>{{$job->user->state.', '.$job->user->city}}</span>
                </div>
                
                <div class="apply-job-detail">
                    <p>{{$job->description}}</p>
                </div>
                
                @if (isset($job->requirements))
                    <div class="apply-job-detail">
                        <h5>Requerimientos:</h5>
                        <ul class="job-requirements">
                            <li><span>{{$job->requirements}}</span> </li>
                        </ul>
                    </div>
                @endif
                @if (empty($job->employee))
                    @auth
                        @if ($job->havePostulation(Auth::user()->id) == 0)
                            @if ($job->user->id != Auth::user()->id)
                                <a style="display: block;margin: auto; width: 128px;" href="/postulation/{{$job->slug}}" class="btn btn-success">Postularme</a>
                            @endif
                        @else
                            @if ($job->user->id != Auth::user()->id)
                                <a style="display: block;margin: auto; width: 200px;" class="btn btn-gray">Postulado</a>
                            @endif
                        @endif
                    
                    @else
                        <a style="display: block;margin: auto; width: 128px;" href="/postulation/{{$job->slug}}" class="btn btn-success">Postularme</a>
                    @endauth
                @else
                    <a style="display: block;margin: auto; width: 128px;" href="javascript:;" class="btn btn-success">Trabajando</a>
                @endif


            </div>
            @auth
                @if ($job->user->id == Auth::user()->id)
                    <div class="sidebar-wrapper">
                        <div class="sidebar-box-header bb-1">
                            <h4>Postulados</h4>
                        </div>
                        
                        @foreach ($job->pendingPostulates as $postulate)
                                <article class="advance-search-job" id="{{$postulate->pivot->id}}">
                                    <div class="row no-mrg">
                                        <div class="col-md-6 col-sm-6">
                                            <a href="/profile/{{ $postulate->id }}" title="job Detail">
                                                <div class="advance-search-img-box">
                                                <img src="{{ asset('img/users/'.$postulate->photo) }}" class="img-responsive img-circle" alt="">                                    
                                            </div>
                                            </a>
                                            <div class="advance-search-caption">
                                                <a href="/profile/{{ $postulate->id }}" title="Job Dtail">
                                                    <h4>{{$postulate->username}}</h4>
                                                </a>
                                                <span class="cl-success">{{$postulate->type}}</span>
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="advance-search-job-locat">
                                                <p><i class="fa fa-map-marker"></i>Querétaro, Querétaro</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <div class="mng-company-action">
                                                <i data-confirmation="Cancelado" class="fa fa-thumbs-down confirmBtn cursor" data-original-title="JuanJO"></i>
                                                <i data-confirmation="Aceptado" class="fa fa-thumbs-up confirmBtn cursor"></i>
                                            </div>
                                            {{--  <a href="javascript:void(0)" data-toggle="modal" data-target="#apply-job" class="btn advance-search" title="apply">Aprobar</a>  --}}
                                        </div>
                                    </div>
                                </article>
                        @endforeach
                    </div>
                @endif
            @endauth
        </div>
        
        <!-- Sidebar Start-->
        <div class="col-md-4 col-sm-4">
            <div class="sidebar-widgets">
                <div class="ur-detail-wrap">
                    <div class="ur-detail-wrap-header">
                        <h4>Detalles del trabajo</h4>
                    </div>
                    <div class="ur-detail-wrap-body">
                        <ul class="ove-detail-list">
                            @if (isset($job->time))
                                <li>
                                    <i class="fa fa-hourglass-half"></i>
                                    <h5>Tiempo (Aprox.)</h5>
                                    <span>{{$job->time}} hrs.</span>
                                </li>
                            @endif
                            <li>
                               
                            </li>
                            <li>
                                <i class="fa fa-money"></i>
                                <h5>Pago</h5>
                                <span>${{$job->payment}} MXN</span>
                            </li>
                            <li>
                                <i class="icon-age"></i>
                                <h5>Edad</h5>
                                <span>{{$job->age}}+ Años</span>
                            </li>
                            <li>
                                <i class="fa fa-eye"></i>
                                <h5>Visitas</h5>
                                <span>{{$job->views}}</span>
                            </li>
                            <li>
                                <i class="fa fa-users"></i>
                                <h5>Postulados</h5>
                                <span>{{count($job->allPostulates)}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Job Detail -->
                <div class="sidebar-container">
                    <a href="/profile/{{ $job->user->id }}" >
                        <div class="sidebar-box">
                        {{-- <h4 class="flc-rate">20K - 30K</h4> --}}
                        <div class="sidebar-inner-box">
                            <div class="sidebar-box-thumb">
                                <img src="{{ asset('/img/users/'.$job->user->photo)}}" class="img-responsive" alt="" />
                            </div>
                                <div class="stars">
                                    @for ($i = 0; $i < $job->user->stars(); $i++)
                                    <i class="fa fa-star fill"></i>
                                    @endfor
                                    @for ($i = 0; $i < (5 - $job->user->stars()); $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                </div>
                            </div>
                            
                            
                            <div class="sidebar-box-detail">
                                <h4>{{$job->user->username}}</h4>
                                {{-- <span class="desination">App Designer</span> --}}
                            </div>
                        </div>
                        <div class="sidebar-box-extra" s>
                        
                            <ul class="status-detail">
                                <li class="br-1"><strong>{{$job->user->state}}</strong>Ubicación</li>
                                <li class="br-1"><strong>{{$job->views}}</strong>Visitas</li>
                                <li><strong>{{$job->user->jobs->count()}}</strong>Trabajos</li>
                            </ul>
                        </div>
                    </a>
                </div>
                <a href="/profile/{{ $job->user->id }}" class="btn btn-sidebar bt-1 bg-success">Ver Perfil</a>
            </div>
            
            <!-- Share This Job -->
           
            
        </div>
        <!-- End Sidebar -->
        
    </div>
</section>
<!-- Job Detail End -->
    
@endsection