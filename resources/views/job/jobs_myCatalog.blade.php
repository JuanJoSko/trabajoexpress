@foreach ($jobs as $job) 
    <div class="col-md-4 col-sm-4">
        <div class="manage-cndt">
            @if ($job->acceptPostulate())
            <input type="hidden" class="photoUser" value="{{$job->acceptPostulate()->photo}}">
            <input type="hidden" class="postulationID" value="{{$job->acceptPostulate()->pivot->id}}">
            <input type="hidden" class="nameUser" value="{{$job->acceptPostulate()->username}}">
            <input type="hidden" class="userID" value="{{$job->acceptPostulate()->id}}">
            @endif

        <div class="pull-right">
            <div class="btn-group action-btn fix-btn-action">
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-v"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="/job/{{$job->slug}}"><i class="fa fa-eye"></i> Ver</a></li>
                    <li><a href="/job/{{$job->slug}}/edit"><i class="fa fa-edit"></i> Editar</a></li>
                    <li><a class="deleteBtn" href="javascript:;"><i class="fa fa-trash"></i> Eliminar</a></li>

                </ul>
            </div>
        </div>
            
            @if ($job->acceptPostulate() && $job->acceptPostulate()->pivot->starsEmployer() == 0 )
                <span class="starsBtn part-time quali-time cursor"><i class="fa fa-star"></i> Calificar</span>
            @endif
            @switch($job->status)
                @case('Cancelado')
                    <span style="left: -5px;" class="tg-themetag tg-featuretag tg-cancel">Cancelado</span>
                @break
                @case('Pendiente')
                    <a href="/payment/job/{{$job->id}}"><span style="left: -5px;" class="tg-themetag tg-featuretag tg-warning">Pagar</span></a> 
                @break
                @case('Rechazado')
                    <a href="/payment/job/{{$job->id}}"><span style="left: -5px;" class="tg-themetag tg-featuretag tg-warning">Rechazado</span></a> 
                @break
                @case('Terminado')
                    <a href="/payment/job/{{$job->id}}"><span style="left: -5px;" class="tg-themetag tg-featuretag tg-finish">Terminado</span></a> 
                @break
                @default
                    <span style="left: -5px;" class="tg-themetag tg-featuretag tg-success">{{$job->status}}</span>
                 @break
            @endswitch
            <div class="job cndt-caption">
                <div class="cndt-pic">
                        @empty($job->photo)
                        @if ($job->category->name != 'Express')
                            <i class="cat-{{ $job->category->icon }} cat-photo-detail"></i>
                        @else
                            <i class="icon-express" style="    font-size: 3em;
                            position: relative;
                            top: 18px;
                            right: 0;
                            color: white;
                            cursor: pointer;" ></i>

                        @endif
                    @else
                        <img src="{{ asset('/img/jobs/'.$job->photo) }}" class="img-responsive" alt="">
                    @endempty
                </div>
            <div class="job-position">
                <span class="job-num fix-top-cero">{{ count($job->allPostulates) }} Postulados</span>
            </div>

                <span>{{$job->user->state.', '.$job->user->city}}</span>
                <h4>{{$job->title}}</h4>
                <p>{{str_limit( $job->description, 32)}}</p>

                @if ($job->acceptPostulate())
                    <ul class="employee-social">
                        @if($job->acceptPostulate()->phone != null)
                            <li><a href="tel:{{ $job->acceptPostulate()->phone }}" title=""><i class="fa fa-phone"></i></a></li>
                        @endif
                        @if($job->acceptPostulate()->email != null)
                            <li><a href="mailto:{{ $job->acceptPostulate()->email }}" title=""><i class="fa fa-envelope"></i></a></li>
                        @endif
                        @if($job->acceptPostulate()->fb != null)
                            <li><a href="https://www.facebook.com/{{ $job->acceptPostulate()->fb }}" title=""><i class="fa fa-facebook"></i></a></li>
                        @endif
                        @if($job->acceptPostulate()->whats != null)
                            <li><a href="https://web.whatsapp.com/send?phone=+52{{ $job->acceptPostulate()->whats }}" title=""><i class="fa fa-whatsapp"></i></a></li>
                        @endif
                        @if($job->acceptPostulate()->insta != null)
                            <li><a href="https://www.instagram.com/{{ $job->acceptPostulate()->insta }}" title=""><i class="fa fa-instagram"></i></a></li>
                        @endif
                    </ul>
                @endif
            </div>
            <a  href="/job/{{$job->slug}}" title="" class="cndt-profile-btn">Ver Trabajo</a>
            
        </div>
    </div>
@endforeach


