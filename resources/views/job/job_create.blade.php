@extends('layouts.app')


@section('content')
<!-- Header Title Start -->
<section class="inner-header-title"  style="background-image:url({{ asset('/img/banners/banner_job.jpeg') }});">
    <div class="container">
        <h1>Ofrece Trabajo</h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Header Title End -->

<!-- General Detail Start -->
<div class="detail-desc section">
    <div class="container white-shadow">
    
        {{-- <div class="row">
            <div class="detail-pic js">
                <div class="box">
                    <input type="file" name="photo" id="upload-pic" class="inputfile" />
                    <label for="upload-pic"><i class="fa fa-upload" aria-hidden="true"></i><span></span></label>
                </div>
            </div>
        </div> --}}
        <form method="POST" class="add-feild" action="{{ route('job.store') }}" enctype="multipart/form-data">

            <div class="row">
                <div class="detail-pic" id ="picUser">
                        {{-- src="{{ asset('/img/can-1.png') }} --}}
                    <img v-if="url" :src="url"  class="img" alt="" />
                    <a @click="trigger" href="javascript:void(0)" class="detail-edit" title="Agregar imagen">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <input class ="d-none" type="file" name="photo" @change="onFileChange"  ref="fileInput">
                </div>
            </div>

            @include('commons.msg')
            @include('commons.errors')

            
            <div class="row bottom-mrg" id="formJob">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input name="title" type="text" class="form-control" placeholder="Nombre del Trabajo" value="{{ old('title') }}" required>
                    </div>
                </div>
                
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input name="payment" type="number" class="form-control" placeholder="Pago" value="{{ old('payment') }}" requiered >
                    </div>
                </div>
                
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <select id="selectCategory" name="category_id" class="form-control input-lg">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input name="time" value="{{ old('time') }}" type="number"  class="form-control" placeholder="Tiempo (Opcional)">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <input name="age" value="{{ old('age') }}" type="number" class="form-control" placeholder="Edad Minima (Opcional)">
                    </div>
                </div>
                <div class="col-md-6">
                    <label style="display: block;
                    text-align: center;">¿Puede realizarlo una persona de la 3era edad?</label>
                        <div class="custom-control check-yes">
                            <label for="isSenior1">
                                <input class="custom-control-input" type="radio" id="isSenior1" name="isSenior" value="1"> Si
                            </label>
                        </div>
                        <div class="custom-control check-no">
                                <label for="isSenior0">
                                <input class="custom-control-input" type="radio" id="isSenior0" name="isSenior" value="0" checked=""> No
                            </label>
                        </div>
                   
                </div>
                {{-- <div class="col-md-6 col-sm-6 hide" id="openOther">
                    <div class="input-group">
                        <input  name="other" type="text" class="form-control" placeholder="Específica otra" value="{{ old('other') }}" requiered >
                    </div>
                </div> --}}
                
                <div class="col-md-12 col-sm-12">
                    <textarea name="description"  value="{{ old('description') }}" class="form-control" placeholder="Descripción" required></textarea>
                </div>
            
                <div class="col-md-12 col-sm-12">
                    <textarea name="requirements" value="{{ old('requirements') }}" class="form-control textarea" placeholder="Requerimientos (Opcional)"></textarea>
                </div>	
                <div class="col-md-12 col-sm-12">
                    <label >Dirección del trabajo</label>
                    <div class="input-group">
                        <input id="addressForm" readonly  name="address" value="{{ old('adress') }}" type="text" class="form-control openModal" placeholder="Dirección del Trabajo" required>
                        {{-- <span class="input-group-btn ">
                            <button  type="button" class="btn btn-primary fix-btn openModal">Ubicación Actual</button>
                        </span> --}}
                        <input id="latitude" type="hidden" name="latitude">
                        <input id="longitude" type="hidden" name="longitude">
                    </div>
                </div>

                <div class="col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-success btn-primary small-btn">Guardar</button>	
                </div>	
                    
            </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Ubicación</h5>
            <p>(Arrastra o Busca la dirección del trabajo)</p>
            <button type="button" class="close closeModal" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
                <div id="myMap"></div>
                <div class="col-md-12 col-sm-12 bt mb-1">
                    <label for="">Dirección:</label>
                    <div class="input-group">
                        <input  class="form-control" id="address" type="text" />
                        <span class="input-group-btn">
                            <button id="submit" type="button" class="btn btn-primary">Buscar</button>
                        </span>
                    </div>
                </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary closeModal">Guardar</button>
        </div>
        </div>
    </div>
    </div>
</div>
@include('modals.alert')

@endsection

@section('scripts')

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCklaCU8LI1jGPMNZweSImXr_wOn-0aMr4">
    </script>
   
    <script>
        $(document).ready(function() {
            var map, infoWindow;
            function initialize() {

            map = new google.maps.Map(document.getElementById('myMap'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            infoWindow = new google.maps.InfoWindow;
            var geocoder = new google.maps.Geocoder();

            var pos = {
                lat: 0,
                lng: 0
            };


            marker = new google.maps.Marker({
                map: map,
                position: pos,
                draggable: true,
            });

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                marker.setPosition(pos);
                infoWindow.open(map);
                map.setCenter(pos);


                geocoder.geocode({ 'latLng': pos }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });



            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
            } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
            }

            document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
            });

            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === 'OK') {
                        $('#address').val(results[0].formatted_address);
                        $('#addressForm').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        resultsMap.setCenter(results[0].geometry.location);
                        infoWindow.setContent(results[0].formatted_address);
                        marker.setPosition(results[0].geometry.location);
                        infoWindow.open(map, marker);

                    } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

        

            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    });

    </script>

    <script>
        $('#selectCategory').change(function(){
            var optionSelected = $("option:selected", this);
            if(optionSelected.text() == 'Otra'){
                $('#openOther').addClass('show');
            }else{
                $('#openOther').removeClass('show');
            }
        });
    </script>
    
    <script>
        $('.openModal').click(function(){
            $('#myModalMap').addClass('d-block');

        });

        $('.closeModal').click(function(){
            $('#myModalMap').removeClass('d-block');

        });

        new Vue({
        el: '#picUser',
        data:{
            url: '/img/jobs/default.png',
        },
            methods: {
                trigger () {
                    this.$refs.fileInput.click()
                },
                onFileChange(e) {
                    const file = e.target.files[0];
                    this.url = URL.createObjectURL(file);
                }
            }
        });
    </script>
    
@endsection