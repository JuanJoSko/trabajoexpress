<div class="container">
    <!--Browse Job In Grid-->
    <div class="row extra-mrg">
        <h2 class="center" >{{$titleTab}}</h2>
        @if (count($jobs) == 0)
            @component('components.alertMsg', [
                'title' => 'Sin resultados que mostrar',
                'img' => 'search.jpg',
                'urlBtn' => '/',
                'titleBtn' => 'Regresar'
                ])
            @endcomponent
        @endif
        @foreach ($jobs as $job)
            <div class="col-md-4 col-sm-6">
                <div class="grid-view brows-job-list  scale-up-center">
                    <div class="brows-job-company-img">
                        @empty($job->photo)
                            @if ($job->category->name != 'Express')
                                <i class="cat-{{ $job->category->icon }} cat-photo"></i>
                            @else
                                <i class="icon-express" style="    font-size: 3em;
                                position: relative;
                                top: 18px;
                                right: 0;
                                color: white;
                                cursor: pointer;" ></i>

                            @endif
                        @else
                            @if ($titleTab != 'Negocios')
                                <img src="{{ asset('/img/jobs/'.$job->photo)}}" class="img-responsive" alt="" />
                            @else
                                <img src="{{ asset('/img/services/'.$job->photo)}}" class="img-responsive" alt="" />
                            @endif

                        @endempty
                    </div>
                    <div class="stars fix-stars">
                        @for ($i = 0; $i < $job->user->stars(); $i++)
                        <i class="fa fa-star fill"></i>
                        @endfor
                        @for ($i = 0; $i < (5 - $job->user->stars()); $i++)
                            <i class="fa fa-star"></i>
                        @endfor
                    </div>
                    <div class="brows-job-position">
                        @if ($titleTab != 'Negocios')
                            <h3><a href="/job/{{$job->slug}}">{{ $job->title}}</a></h3>
                        @else
                            <h3><a href="/service/{{$job->slug}}">{{ $job->title}}</a></h3>
                        @endif
                        <p class="fix-description"><span>{{str_limit( $job->description, 32)}}</span></p>
                    </div>
                    @if ($titleTab != 'Negocios')
                        <div class="job-position">
                            <span class="job-num">{{ count($job->allPostulates) }} Postulados</span>
                        </div>
                    @endif
                  
                    <div class="brows-job-type">
                        @empty(!$job->time)
                            <span class="full-time"><i class="fa fa-clock-o"></i> {{$job->time}} hrs.</span>
                        @endempty
                       @empty(!$job->payment)
                            <span class="full-cash"><i class="fa fa-money"></i> ${{$job->payment}} MXN</span>
                       @endempty
                    </div>
                    <ul class="grid-view-caption">
                        <li>
                            <div class="brows-job-location">
                                <p><i class="fa fa-map-marker"></i>Querétaro, Querétaro</p>
                            </div>
                        </li>
                    </ul>
                    {{-- <span class="tg-themetag tg-featuretag">Premium</span> --}}
                </div>
            </div>
        @endforeach

        
        
    </div>
    
    
    <!--/.Browse Job In Grid-->
    <div class="paginate-center">
        {!! $jobs->render() !!}
    </div>
    
</div>