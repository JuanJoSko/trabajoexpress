<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="csrf-token-id" name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/png" href="{{ asset('/img/favicon.png') }}"/>


    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('plugins/js/jquery.min.js') }}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
    <script type="text/javascript" src="{{ asset('plugins/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/js/loader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/bootsnav.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/wysihtml5-0.3.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/bootstrap-wysihtml5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/datedropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/loader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/gmap3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/jquery.easy-autocomplete.min.js') }}"></script>
    <script src="{{ asset('/js/custom.js') }}"></script>
    <script src="{{ asset('js/jQuery.style.switcher.js') }}"></script>
    
    <script>
        function openRightMenu() {
            document.getElementById("rightMenu").style.display = "block";
        }

        function closeRightMenu() {
            document.getElementById("rightMenu").style.display = "none";
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
    <link href="{{ asset('plugins/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/colors/green-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icomoon.css') }}" rel="stylesheet" type='text/css'>
    <link href="{{ asset('css/categories.css') }}" rel="stylesheet" type='text/css'>
    
    @yield('css')

    

</head>
<body>
    <div class="Loader"></div>

    <div class="wrapper" >

        <nav id="navHeader" class=" nav-trans navbar navbar-default navbar-fixed navbar-light white bootsnav">
            <div class="container">
                <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/') }}">
                <img id="logoID"  src="{{ asset('/img/logo_horizontal-white.png') }}" class="logo logo-scrolled" alt="">
                    </a>
                </div>
            </div>
        </nav>

        <main class="py-4" id="app">
            @yield('content')


        </main>


    </div>
    <footer class="footer">
        <div class="row lg-menu">
            <div class="container">
               
                <div class="col-md-8 co-sm-8 pull-right">
                    <ul class="footer-social">
                        <li>
                            <a href="https://www.facebook.com/Trabajo-Express-402394130580243/">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li><a href="https://www.instagram.com/trabajo_express/">
                            <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row no-padding">
            <div class="container">
                <div class="col-md-4 col-sm-4">
                    <div class="footer-widget">
                        <img class="logo-footer" src="{{ asset('img/logo_white.png') }}" alt="">
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="footer-widget">
                        <h3 class="widgettitle widget-title">Encuentranos en:</h3>
                        <div class="textwidget">
                            <p>Paseo de la República Km 13020, 76230 .
                                <br>Juriquilla, Qro</p>
                            <p><strong>Email:</strong> contacto@trabajoexpress.com</p>
                            <p><strong>Telefóno:</strong> <a href="tel:+4425817797">4425817797</a></p>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="container">
                <p>Copyright © 2019 - Todos los derechos reservados a TrabajoExpress®. Powered by <a href="https://matgagroup.com">Matga Grupo Empresarial</a></p>
            </div>
        </div>
    </footer>
    
    @yield('scripts')


    <script>
        $( document ).ready(function() {
             $(window).scroll(function () {
                if ($(document).scrollTop() == 0) {
                    $('#logoID').attr('src',"{{ asset('/img/logo_horizontal-white.png') }}");
                    $('#navHeader').removeClass('fix-nav-bottom');
                } else {
                    $('#logoID').attr('src',"{{ asset('/img/logo_horizontal.png') }}");
                    $('#navHeader').addClass('fix-nav-bottom');
                    $('#navbarDropdown').css("cssText", "color: white !important");

                }
                
            }); 
          
            
        });
    </script>

</body>
</html>
