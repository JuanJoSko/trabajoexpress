<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('plugins/js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
        <script type="text/javascript" src="{{ asset('/plugins/js/loader.js') }}"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"

    <!-- Custom Js -->
    <script src="
        {{ asset('js/custom.js') }}"></script>

    <!-- Fonts -->
    <style>
        ::-webkit-input-placeholder {
            text-align: center;
        }
        
        :-moz-placeholder { /* Firefox 18- */
            text-align: center;  
        }
        
        ::-moz-placeholder {  /* Firefox 19+ */
            text-align: center;  
        }
        
        :-ms-input-placeholder {  
            text-align: center; 
        }
    </style>
    <!-- Styles -->
    <link rel=" stylesheet"
        href="{{ asset('plugins/css/plugins.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link type="text/css" rel="stylesheet" id="jssDefault" href="{{ asset('css/colors/green-style.css') }}">

</head>
<body class="simple-bg-screen" style="background-image:url(/img/banners/banner_auth.jpg);">
    <div id="app">
            <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full">
                <div class="container" >
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img   src="/img/logo_horizontal.png" class="logo logo-display" alt="">
                    </a></div><div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                            <li class="left-br">
                            <a href="@yield('urlBtn')"  class="signin">@yield('nameBtn')</a>
                            </li>
                            
                        </ul>
                        <div class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                            <li class="dropdown megamenu-fw">
                                <a style="color: black;
                                font-weight: 700;" href="{{ url('/') }}">Inicio</a>
                            </li>
                        </div>
                    </div>
                </div>
            </nav>

        <main class="py-4">

            @yield('content')
        </main>
    </div>
    <footer class="footer-2 text-center"> Copyright © 2019 - All Rights Reserved TrabajoExpress®.</footer>
</body>
</html>