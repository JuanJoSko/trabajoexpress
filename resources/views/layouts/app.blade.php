<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="csrf-token-id" name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/png" href="{{ asset('/img/favicon.png') }}"/>


    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('plugins/js/jquery.min.js') }}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
    <script type="text/javascript" src="{{ asset('plugins/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/js/loader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/bootsnav.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/wysihtml5-0.3.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/bootstrap-wysihtml5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/datedropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/loader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/gmap3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/js/jquery.easy-autocomplete.min.js') }}"></script>
    <script src="{{ asset('/js/custom.js') }}"></script>
    <script src="{{ asset('js/jQuery.style.switcher.js') }}"></script>
    
    <script>
        function openRightMenu() {
            document.getElementById("rightMenu").style.display = "block";
        }

        function closeRightMenu() {
            document.getElementById("rightMenu").style.display = "none";
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
    <link href="{{ asset('plugins/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/colors/green-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icomoon.css') }}" rel="stylesheet" type='text/css'>
    <link href="{{ asset('css/categories.css') }}" rel="stylesheet" type='text/css'>
    
    @auth
        <input type="hidden" id="idByOne" value="{{Auth::user()->id}}">
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                appId: "37f1b09b-c92f-4968-abef-1092128746fc",
                });
            });

            OneSignal.push(function() {
                var idByOne = document.getElementById("idByOne").value;
                /* These examples are all valid */
                // Occurs when the user's subscription changes to a new value.
                OneSignal.on('subscriptionChange', function (isSubscribed) {
                    console.log("The user's subscription state is now:", isSubscribed);
                    OneSignal.sendTag("user_id",idByOne, function(tagsSent)
                    {
                        // Callback called when tags have finished sending
                        console.log("Tags have finished sending!");
                    });
                });

                var isPushSupported = OneSignal.isPushNotificationsSupported();
                if (isPushSupported)
                {
                    // Push notifications are supported
                    OneSignal.isPushNotificationsEnabled().then(function(isEnabled)
                    {
                        if (isEnabled)
                        {
                            console.log("Push notifications are enabled!");

                        } else {
                            OneSignal.showHttpPrompt();
                            console.log("Push notifications are not enabled yet.");
                        }
                    });

                } else {
                    console.log("Push notifications are not supported.");
                }
            });
        </script>
    @endauth

    @yield('css')

    @include('ads.monetyzer')

</head>
<body>
    <div class="Loader"></div>

    <div class="wrapper" >

        @include('static.header')

        <main class="py-4" id="app">
            @yield('content')


        </main>


    </div>
    @include('static.footer')

    
    @yield('scripts')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151877049-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-151877049-1');
    </script>

    <script>

        $( document ).ready(function() {      
            var isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
            if (isMobile) {
                console.log('hi');
            }
        });


        $('#notify').click(function(){
            $('#notifyMenu').css('display', 'block');

            var token = $('#csrf-token-id').attr('content');

            $.ajax({
                url: '/notification/view',
                data: {page: 'page'},
                type: 'GET',
                dataType: 'json',
                success: function(data){
                   console.log(data);
                }
            });

            

        });
        window.addEventListener('mouseup',function(event){
            var pol = document.getElementById('notifyMenu');
            if(event.target != pol && event.target.parentNode != pol){
                pol.style.display = 'none' ;
            }
      }); 

    </script>

    <script>
        $( document ).ready(function() {
             $(window).scroll(function () {
                if ($(document).scrollTop() == 0) {
                    $('#logoID').attr('src',"{{ asset('/img/logo_horizontal-white.png') }}");
                    $('#navHeader').removeClass('fix-nav-bottom');
                } else {
                    $('#logoID').attr('src',"{{ asset('/img/logo_horizontal.png') }}");
                    $('#navHeader').addClass('fix-nav-bottom');
                    $('#navbarDropdown').css("cssText", "color: white !important");

                }
                
            });
            
        });
    </script>

</body>
</html>
