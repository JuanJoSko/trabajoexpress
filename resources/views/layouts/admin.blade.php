<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta id="csrf-token-id" name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }} - Admin</title>

        <link rel="shortcut icon" type="image/png" href="{{ asset('/img/favicon.png') }}"/>

      
        <!-- Styles -->
        <link href="{{ asset('admin/css/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet">
        @yield('css')

        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="{{ asset('admin/assets/scripts/main.js') }}"></script>
        <script src="https://kit.fontawesome.com/f7d12e9ac4.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        {{-- <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script> --}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        @yield('scripts')

        

    </head>
    <body>
        <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
            @include('admin.static.header')
            <div class="app-main">
                @include('admin.static.sidebar')
    
                <div class="app-main__outer">
                    <div class="app-main__inner">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <div id="modalLarge" class="modal fade bd-example-modal-lg"  role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    @yield('modal')
                </div>
            </div>
        </div>
        @yield('modals')
        @yield('addressModal')
        @yield('scripts')
        @include('admin.static.loader')

        @include('admin.static.footer')

    </body>
</html>
