@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/anim.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="clearfix"></div>

<div class="banner home-5" style="background-image:url({{ asset('img/banners/banner_home.jpg')}});">
    <div class="container">
        <div class="banner-caption">
            <div class="col-md-12 col-sm-12 banner-text">
                <h1> Encuentra más de 100,000 trabajos express</h1>
                <form class="form-horizontal" action="/job-search" method="get">
                    <div class="col-md-9 no-padd">
                        <div class="input-group search">
                            <input name="search" type="text" class="form-control" placeholder="Buscar trabajo">
                        </div>
                    </div>
                    <div class="col-md-3 no-padd">
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary search-btn"><i class="fa fa-search" aria-hidden="true"></i> Buscar Trabajo</button>
                        </div>
                    </div>
                </form>
                {{-- <div class="video-box"><a href="#" class="btn btn-video"><i class="fa fa-play" aria-hidden="true"></i></a></div> --}}
            </div>
        </div>
    </div>
</div>

<div class="company-brand">
    <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
                    <div class="col-md-4 col-sm-4">
                        <div class="working-process">
                            <span id="job" class="process-img progress-active">
                                <i class="icon-express icon-button"></i>
                            </span>
                            <h4>Express</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="working-process">
                            <span id="service" class="process-img">
                                <i class="icon-oficios icon-button"></i>
                            </span>
                            <h4>Negocios</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div  class="working-process">
                            <span id="old" class="process-img">
                                <i class="icon-old icon-button"></i>
                            </span>
                            <h4>3era Edad</h4>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>



    <!-- ========== Begin: Brows job Category ===============  -->
<section class="brows-job-category jobs">
    @include('job.jobs_catalog')
</section>

<section class="download-app inverse-bg" style="background-image:url({{ asset('img/banners/banner_app.jpg')}});">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1">
                <div class="app-content">
                    <h2>Descarga la aplicación en tu celular</h2>
                    <p>Ahora ya esta disponible nuestra app TRABAJO EXPRESS descargala en tu celular para  poder encontrar un trabajo eventual con un pago inmediato. Así como para poder anunciar una oferta de trabajo y tener una rápida respuesta de las personas interesadas.</p>
                    <a href="#" class="btn call-btn"><i class="fa fa-apple"></i>iPhone Store</a>
                    <a  target="_blank" href="https://play.google.com/store/apps/details?id=com.matgagroup.TrabajoExpress" class="btn call-btn gps"><i class="fa fa-android"></i>Google Play</a>
                </div>
            </div>
        </div>
        <!--/row-->
    </div>
</section>
    
@endsection


@section('scripts')

    <script src="{{ asset('/js/pagination.js') }}"></script>
    
@endsection

