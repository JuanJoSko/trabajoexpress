<div class="text-center alert-msg">
    <img class="img-alert" src="{{ asset('/img/'.$img) }}" alt="" srcset="">
    <p class="text-alert">{{ $title }}</p>
    <a class="back-btn-alert" href="{{ $urlBtn }}"><i class="icon-back "></i> {{ $titleBtn }}</a>
</div>