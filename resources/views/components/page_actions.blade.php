<div class="page-title-actions">
    <div class="d-inline-block dropdown">
        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
            <span class="btn-icon-wrapper pr-2 opacity-7">
                <i class="fa fa-bars fa-w-20"></i>
            </span>
            Acción
        </button>
        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
            <ul class="nav flex-column">
                @foreach ($buttons as $button)
                    <li class="nav-item">
                        <a class="nav-link "  {{$button['action']}}  >
                            <i class="nav-link-icon fas fa-{{$button['icon']}}"></i>
                            <span>
                                {{$button['name']}}
                            </span>
                        </a>
                    </li> 
                @endforeach
            </ul>
        </div>
    </div>
</div>
