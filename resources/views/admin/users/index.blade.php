@extends('layouts.admin')

@section('css')
    <link href="{{ asset('admin/css/dropify.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
@endsection

@section('scripts')
    <script>
        $('#formUser').submit(function(e) {
            $('#loader').removeClass('d-none');
            
            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');
            var token = $('#csrf-token-id').attr('content');

            //var fd = new FormData(form.serialize());
            //var files = $('#fileInput')[0].files[0];
            //console.log(files);
            //fd.append('photo',files);

            
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': token},
                url: url,
                data: new FormData(this),
                contentType:false,
                processData: false,
                success: function(response)
                {
                    if(response.status == true){
                        $('.close').trigger('click');
                        $('#loader').addClass('d-none');
                        $('#modalLarge').modal('hide');
                        $('#formUser').trigger("reset");
                        $('#tbodyUser').append(
                            '<tr id="'+response.user.id+'" role="row" class="even">'+
                            '<td class="sorting_1">'+response.user.id+'</td>'+
                            '<td>'+response.user.name+'</td>'+
                            '<td>'+response.user.email+'</td>'+
                            '<td>Usuario</td>'+
                            '<td>'+
                                '<i class="fas fa-id-card action-icon" aria-hidden="true"></i>'+
                                '<i class="fas fa-credit-card action-icon" aria-hidden="true"></i>'+
                                '<i class="fas fa-tag action-icon" aria-hidden="true"></i>'+
                                '<i class="fas fa-edit action-icon " aria-hidden="true"></i>'+
                                '<i class="fas fa-trash action-icon deleteBtn" aria-hidden="true"></i>'+
                            '</td>'+
                        '</tr>');
                        Swal.fire({
                            type: 'success',
                            title: 'Usuario Registrado',
                            text: response.msg,
                        })
                    }else{
                        $('#loader').addClass('d-none');
                        Swal.fire({
                            type: 'error',
                            title: 'Uups...',
                            text: response.msg,
                        })
                    }
                }
            });
            
        });
    </script>


    <script src="{{ asset('admin/js/dropify.min.js') }}"></script>
    <script>
            $('.dropify').dropify({
                messages: {
                    'default': 'Arrastra y suelta aquí o haz clic',
                    'replace': 'Arrastra y suelta o haz clic para reemplazar',
                    'remove':  'Eliminar',
                    'error':   'Vaya, algo mal pasó.'
                }
            });
            
    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script>

        "use strict";

        var SweetAlert = function() {};
    
        //examples 
        SweetAlert.prototype.init = function() {

            //Parameter
           
        }

        $('.deleteBtn').click(function(){
            Swal.fire({
                title: '¿Está seguro que desea eliminar este usuario?',
                text: "Si borra a este usuario se eliminará su historial de información.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Borrarlo!'
              }).then((result) => {
                if (result.value) {
                  Swal.fire(
                    'Usuario Borrado!',
                    'El Usuario ha sido elimindado de la base de datos de usuarios',
                    'success'
                  )
                }
              })
        });

    
        $('#tableUser').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );

        $('.buttons-print').find('span').text('Imprimir');
        $('.buttons-copy').find('span').text('Compiar');
    

    </script>
@endsection
@section('content')
    <div class="app-page-title">
        <div class="page-title-heading">
            @component('components.page_title',[
                'title' => 'Usuarios',
                'description' => 'Lista de Usuarios registrados en la Base de datos.',
                'icon' => 'fa-users'
            ])
                
            @endcomponent
            @component('components.page_actions',[
                'buttons' => [
                    [
                        'name'=>'Agregar',
                        'action'=>'data-target=#modalLarge data-toggle=modal',
                        'icon'=> 'plus-circle'
                    ],
                    [
                        'name'=>'Excel',
                        'action'=>'export',
                        'icon'=> 'file-export'
                    ],
                    [
                        'name'=>'PDF',
                        'action'=>'print',
                        'icon'=> 'print'
                    ],
                    [
                        'name'=>'Imprimir',
                        'action'=>'print',
                        'icon'=> 'print'
                    ]
                ]
            ])
                
            @endcomponent 
        </div>
    </div>
    <div class="main-card mb-3 card">
    <div class="card-body">
        <table  style="width: 100%;" id="tableUser" class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Correo</th>
                <th>Tipo</th>
                <th>Acción</th>
            </tr>
            </thead>
            <tbody id="tbodyUser">
                @foreach ($users as $user)
                        <tr id="{{$user->id}}">
                        <td>{{$user->id}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->lastName}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->type}}</td>
                        <td>
                            <i class="fas fa-trash action-icon deleteBtn" title="Eliminar"></i>
                            <i class="fas fa-edit action-icon " title="Editar"></i>
                            <i class="fas fa-eye action-icon deleteBtn" title="Ver"></i>
                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
    </div>
</div>



@endsection

@section('modal')

    @include('admin.users.create')
    
@endsection