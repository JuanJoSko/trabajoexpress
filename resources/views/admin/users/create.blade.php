<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Agregar nuevo usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div id="carouselExampleIndicators" class="carousel slide main-card mb-3 card" data-interval="false"  data-ride="carousel">
            <ol class="carousel-indicators">
                <li  data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
                <form id="formUser" action="#" method="post" enctype="multipart/form-data" >
                    @csrf
                    <div class="carousel-item active">
                        <div class="card-body">
                            <div class="form-modal">
                                <div class="white-box">
                                    <input name="photo" type="file" id="input-file-now" class="dropify"  /> 
                                </div>
        
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user-alt" ></i></span>
                                    </div>
                                    <input name="name" placeholder="Nombre" type="text" class="form-control" >
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user-alt" ></i></span>
                                    </div>
                                    <input name="lastName" placeholder="Apellidos" type="text" class="form-control" >
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user-alt" ></i></span>
                                    </div>
                                    <input name="username" placeholder="Usuario" type="text" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-envelope" ></i></span>
                                    </div>
                                    <input name="email" placeholder="Correo Electrónico" type="email" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key" ></i></span>
                                    </div>
                                    <input name="password" placeholder="Contraseña" type="password" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key" ></i></span>
                                    </div>
                                    <input name="password_confirmation" placeholder="Confirmar Contraseña" type="password" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item  ">
                        <div class="card-body">
                            <div class="form-modal">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <select name="type" id="whoAre" class="form-control">
                                        <option value="">¿Qué erés?</option>
                                        <option value="Adulto Mayor">Adulto Mayor</option>
                                        <option value="Desempleado">Desempleado</option>
                                        <option value="Empresa/Negocio">Empresa/Negocio</option>
                                        <option value="Estudiante">Estudiante</option>
                                        <option value="Madre/Padre Soltero">Madre/Padre Soltero</option>
                                        <option value="Trabajador">Trabajador</option>
                                    </select>
                                </div>
                                <div id="openCompany">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">#</span>
                                        <input id="company" name="company" type="text" placeholder="Nombre de la empresa" class="form-control">
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">#</span>
                                        <input id="turn" name="turn" type="text" placeholder="Giro" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input name="timeEntry" placeholder="Horario de Entrada" type="text" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input name="timeExit" placeholder="Horario de Salida" type="text" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input name="timeBreak" placeholder="Horario de Comida" type="text" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input name="timeTolerance" placeholder="Tiempo de Tolerancia" type="text" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input name="salary" placeholder="Salario Base" type="text" class="form-control">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    <input name="bonous" placeholder="Bonos" type="text" class="form-control">
                                </div>
                                {{-- <div class="input-group">
                                    <button type="button" class="btn-wide  btn-icon-vertical btn-icon-bottom btn-shadow btn btn-outline-success">
                                        Escanear huella digital
                                        <i class="fas fa-fingerprint btn-icon-wrapper"></i>
                                    </button>
                                    <button style="margin-left:20px" type="button" class="btn-wide  btn-icon-vertical btn-icon-bottom btn-shadow btn btn-outline-success">
                                        Permisos
                                        <i class="fas fa-key btn-icon-wrapper"></i>
                                    </button>
                                </div> --}}
                                <div style="padding: 0;" class="form-footer">
                                    <div class="row center">
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn-wide mb-2 mr-2 btn-icon-vertical btn-icon-bottom btn-shadow btn btn-outline-success">
                                            Cancelar
                                            <i class="fas fa-window-close btn-icon-wrapper"></i>
                                        </button>
                                        <button type="submit" class="btn-wide mb-2 mr-2 btn-icon-vertical btn-icon-bottom btn btn-success">
                                            Guardar
                                            <i class="fas fa-save btn-icon-wrapper"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </form>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="far fa-arrow-alt-circle-left form-arrows" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="far fa-arrow-alt-circle-right form-arrows" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    
    