<div id="loader" class="blockUI blockOverlay d-none" style="border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; position: fixed;">
    <div class="blockUI undefined blockPage" style="position: fixed;">
        <div class="body-block-example-1 d-none" style="cursor: default;">
            <div class="loader bg-transparent no-shadow p-0">
                <div class="ball-grid-pulse">
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                    <div class="bg-green"></div>
                </div>
            </div>
        </div>
    </div>
</div>