<div class="app-sidebar sidebar-shadow bg-royal sidebar-text-light">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                    data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar ps ps--active-y">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu metismenu">
                <li class="app-sidebar__heading">Inicio</li>
                <li class="mm-active">
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="metismenu-icon fas fa-tachometer-alt"></i>
                        Dashboard
                    </a>
                </li>
                <li class="divider"></li>
               <li >
                    <a href="{{ route('admin.users.index') }}">
                        <i class="metismenu-icon fas fa-users">
                        </i>Usuarios
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('admin.jobs.index') }}">
                        <i class="metismenu-icon pe-7s-light fas fa-box"></i>
                        Trabajos
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('admin.services.index') }}">
                        <i class="metismenu-icon pe-7s-light fas fa-box"></i>
                        Negocios\Empresas
                    </a>
                </li>
                 {{-- 
                <li class="divider"></li>
                <li>
                    <a href="{{ route('suppliers.index') }}">
                        <i class="metismenu-icon fas fa-user-friends">
                        </i>Provedores
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-light fas fa-box"></i>
                        Artículos
                        <i class="metismenu-state-icon pe-7s-angle-down fas fa-caret-right"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="{{ route('articles.index') }}">
                                <i class="metismenu-icon fas fa-list">
                                </i>Lista
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('products.index') }}">
                                <i class="metismenu-icon fas fa-box">
                                </i>Producto/Servicio
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('kits.index') }}">
                                <i class="metismenu-icon fas fa-boxes">
                                </i>Kit producto/servicio
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('courses.index') }}">
                                <i class="metismenu-icon fas fa-chalkboard-teacher">
                                </i>Cursos
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('memberships.index') }}">
                                <i class="metismenu-icon fas fa-user-tie">
                                </i>Membresías
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('events.index') }}">
                                <i class="metismenu-icon fas fa-calendar-day">
                                </i>Eventos
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('events.index') }}">
                        <i class="metismenu-icon fas fa-cash-register">
                        </i>Punto de Venta
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('events.index') }}">
                        <i class="metismenu-icon fas fa-comment-dollar">
                        </i>Cotizaciones
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-light fas fa-box"></i>
                        Requisición
                        <i class="metismenu-state-icon pe-7s-angle-down fas fa-caret-right"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="{{ route('articles.index') }}">
                                <i class="metismenu-icon fas fa-list">
                                </i>Lista
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('products.index') }}">
                                <i class="metismenu-icon fas fa-box">
                                </i>Pendientes
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('kits.index') }}">
                                <i class="metismenu-icon fas fa-boxes">
                                </i>Aprobadas
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('courses.index') }}">
                                <i class="metismenu-icon fas fa-chalkboard-teacher">
                                </i>Canceladas
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('events.index') }}">
                        <i class="metismenu-icon fas fa-comment-dollar">
                        </i>Roles
                    </a>
                </li> --}}
                
            
                {{--  
                <li class="app-sidebar__heading">Forms</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-light"></i>
                        Elements
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="forms-controls.html">
                                <i class="metismenu-icon">
                                </i>Controls
                            </a>
                        </li>
                        <li>
                            <a href="forms-layouts.html">
                                <i class="metismenu-icon">
                                </i>Layouts
                            </a>
                        </li>
                        <li>
                            <a href="forms-validation.html">
                                <i class="metismenu-icon">
                                </i>Validation
                            </a>
                        </li>
                        <li>
                            <a href="forms-wizard.html">
                                <i class="metismenu-icon">
                                </i>Wizard
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-joy"></i>
                        Widgets
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul class="mm-collapse">
                        <li>
                            <a href="forms-datepicker.html">
                                <i class="metismenu-icon">
                                </i>Datepicker
                            </a>
                        </li>
                        <li>
                            <a href="forms-range-slider.html">
                                <i class="metismenu-icon">
                                </i>Range Slider
                            </a>
                        </li>
                        <li>
                            <a href="forms-input-selects.html">
                                <i class="metismenu-icon">
                                </i>Input Selects
                            </a>
                        </li>
                        <li>
                            <a href="forms-toggle-switch.html">
                                <i class="metismenu-icon">
                                </i>Toggle Switch
                            </a>
                        </li>
                        <li>
                            <a href="forms-wysiwyg-editor.html">
                                <i class="metismenu-icon">
                                </i>WYSIWYG Editor
                            </a>
                        </li>
                        <li>
                            <a href="forms-input-mask.html">
                                <i class="metismenu-icon">
                                </i>Input Mask
                            </a>
                        </li>
                        <li>
                            <a href="forms-clipboard.html">
                                <i class="metismenu-icon">
                                </i>Clipboard
                            </a>
                        </li>
                        <li>
                            <a href="forms-textarea-autosize.html">
                                <i class="metismenu-icon">
                                </i>Textarea Autosize
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading">Charts</li>
                <li>
                    <a href="charts-chartjs.html">
                        <i class="metismenu-icon pe-7s-graph2">
                        </i>ChartJS
                    </a>
                </li>
                <li>
                    <a href="charts-apexcharts.html">
                        <i class="metismenu-icon pe-7s-graph">
                        </i>Apex Charts
                    </a>
                </li>
                <li>
                    <a href="charts-sparklines.html">
                        <i class="metismenu-icon pe-7s-graph1">
                        </i>Chart Sparklines
                    </a>
                </li>  --}}
            </ul>
        </div>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 909px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 817px;"></div>
        </div>
    </div>
</div>