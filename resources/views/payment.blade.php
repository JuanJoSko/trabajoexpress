@extends('layouts.app')

@section('css')
    {{-- <link href="{{ asset('css/conekta.css') }}" rel="stylesheet"> --}}
@endsection

@section('content')
<!-- Header Title Start -->
<section class="inner-header-title"  style="background-image:url({{ asset('/img/banners/banner_payment.jpg') }});">
    <div class="container">
    <h1>Pago de Comisión</h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Header Title End -->

<!-- General Detail Start -->
<div class="detail-desc section">
    <div class="container white-shadow" style="padding-left: 0px; padding-right: 0px;">
        @include('commons.msg')
        @include('commons.errors')

        {{-- @include('conekta.card') --}}
            @if ($job->status != 'Pagado')
                <div class="sttabs tabs-style-circle" style="margin: auto;box-shadow: 2px 4px 13px #9c9898;height:auto;">
                    <nav>
                        <ul style="border-bottom: 2px solid #10c18e;">
                            <li id ="tab-1" class="tab-current"><a href="#section-circle-1" class="sticon fa fa-exclamation-triangle"><span>Aviso</span></a></li>
                            <li id="tab-2"><a href="#section-circle-2" class="sticon fa fa-list-alt"><span>Terminos</span></a></li>
                            <li id="tab-3"><a href="#section-circle-3" class="sticon fa fa-credit-card-alt"><span>Pagar</span></a></li>
                        </ul>
                    </nav>
                    <div class="content-wrap text-center">
                        <section id="section-circle-1" class="content-current">
                            <div class="tab-pane active" role="tabpanel" id="step1">
                                <h3>Aviso</h3>
                                <p>Primer Paso</p>
                                <p>Ya sólo queda un paso más para publicarlo</p>
                                <div class="alert-use">
                                    @if ($type == 'service')
                                        <h3>Se cobrará una comisión de $499 MXN <br> por anunciarse por 2 meses</h3>
                                    @else
                                        <h3>Se cobrará una comisión de ${{ $total }} MXN</h3>
                                    @endif
                                </div>
                                    <button id="2" type="button" class="footer-btn grn-btn button next-step nextBtn">Continuar</button>
                            </div>
                        </section>
                        <section id="section-circle-2">
                            <div class="tab-pane active" role="tabpanel" id="step2">
                                <h3>Terminos y condiciones de pago</h3>
                                <p>Segundo paso</p>
                                @include('text.pay')
                                <div class="custom-control custom-checkbox" style="margin-bottom: 2%;">
                                    <input class="custom-control-input" type="checkbox" name="terms" id="checkTerms" value="accept">
                                    <label class="form-check-label" for="checkTerms">
                                        Acepto los terminos y condiciones.
                                    </label>
                                </div>
                                <ul class="list-inline">
                                    <li><button id="1" type="button" class="footer-btn blu-btn prev-step nextBtn">Anterior</button></li>
                                    <li><button id="3" data-check="terms" type="button" class="footer-btn grn-btn button next-step nextBtn">Siguiente</button></li>
                                </ul>
                            </div>
                        </section>
                        <section id="section-circle-3">
                            <div class="" role="tabpanel" id="step2">
                                <h3>Selecciona el método de pago</h3>
                                <p>Paso 3</p>
                                <div v-bind:class="{ hidden: isActive }" >
                                    {{-- <form id="paypalForm" action="{{ route('paypal.complete') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="job_id" value="{{ $job->id }}">
                                        <input type="hidden" name="type" value="{{ $type }}">

                                        <button  style="display: inline-block;margin: auto; margin-left:10px;" class="footer-btn grn-btn button" type="submit" >
                                            <span class="btn-label">
                                                <i class="fa fa-paypal"></i>
                                            </span>PAYPAL
                                        </button>
                                    </form> --}}
                                    <button style="display: inline-block;margin: auto; margin-left:10px;" class="footer-btn grn-btn button" type="button"  @click="myCard">
                                        <span class="btn-label">
                                            <i class="fa fa-credit-card"></i>
                                        </span>TARJETA
                                    </button>
                                </div>

                                @include('conekta.card')
                            </div>
                        </section>
                        
                    </div>
                    <!-- /content -->
                </div>
            @else
                @component('components.alertMsg', [
                    'title' => $type == 'service' ? 'Este Negocio ya está pagado' : 'Este trabajo ya está pagado',
                    'img' => 'success.jpg',
                    'urlBtn' => '/',
                    'titleBtn' => 'Regresar'
                    ])
                @endcomponent
            @endif
        
    </div>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Acepta los terminos y condiciones</h5>
            <button type="button" class="close closeModal" >
            <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <p style="text-align: center;
            padding: 14px;">Para continuar debes leer y aceptar los terminos y condiciones de pago</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary closeModal">Entendido</button>
        </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/custom.js') }}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

    <script type="text/javascript" >
        $(function () {
            $("#paypalForm").submit(function(event) {
                var $form = $(this);
                $('.Loader').addClass('show');
            });
        });

        Conekta.setPublicKey("key_ZRzs1X6jBx8roh9YTFCZCiw");
      
        var conektaSuccessResponseHandler = function(token) {
          var $form = $("#card-form");
          //Inserta el token_id en la forma para que se envíe al servidor
           $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
          $form.get(0).submit(); //Hace submit
        };
        var conektaErrorResponseHandler = function(response) {
          var $form = $("#card-form");
          $form.find(".card-errors").text(response.message_to_purchaser);
          $form.find("button").prop("disabled", false);
        };
      
        //jQuery para que genere el token después de dar click en submit
        $(function () {
          $("#card-form").submit(function(event) {
            var $form = $(this);
            // Previene hacer submit más de una vez
            $form.find("button").prop("disabled", true);
            Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
            return false;
          });
        });
      </script>

    <script>
        new Vue({
            el: '#section-circle-3',

            data: {
                isActive: false
            },

            methods: {
                myCard: function(){
                    this.isActive = !this.isActive;
                // some code to filter users
                }
            }
        });
    </script>

    <script src="{{ asset('js/payment.js') }}"></script>

@endsection