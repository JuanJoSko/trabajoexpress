@extends('layouts.app')

@section('content')
<!-- Header Title Start -->
<section class="inner-header-title"  style="background-image:url({{ asset('/img/banners/contact-banner.jpg') }});">
    <div class="container">
        <h1>Contáctanos</h1>
    </div>
</section>
<div class="clearfix"></div>
<!-- Header Title End -->

<!-- General Detail Start -->
<div class="detail-desc section">
    <div class="container white-shadow">
    

        @include('commons.msg')

        
        <div class="row bottom-mrg">
            <form id="contactForm" action="{{ route('contact.store') }}" method="POST" class="add-feild" style="margin-top: 4%;">
                @csrf

                @guest
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <input name="name" type="text" class="form-control" placeholder="Nombre" required>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <input name="email" type="email" class="form-control" placeholder="Correo" required>
                        </div>
                    </div> 
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                            <input name="phone" type="number" class="form-control" placeholder="Teléfono (Opcional)">
                        </div>
                    </div>
                @else
                    <div class="col-md-6 col-sm-6">
                        <div class="input-group">
                        <input name="name" value="{{Auth::user()->name.' '.Auth::user()->lastName}}" type="text" class="form-control" placeholder="Nombre" readonly>
                        </div>
                    </div>
                   
                    <input name="email" type="hidden" class="form-control" value="{{ Auth::user()->email }}">
                    <input name="phone" type="hidden" class="form-control" value="{{ Auth::user()->phone }}">
                       
                @endguest

            
                <div class="col-md-6 col-sm-6">
                    <div class="input-group">
                        <select name="subject" class="form-control input-lg" required>
                            <option value="Consulta General">Consulta General</option>
                            <option value="Consulta General">Sugerencias</option>
                            <option value="Aclaración de Pago">Aclaración de Pago</option>
                            <option value="Informar un problema">Informar un problema</option>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-12 col-sm-12">
                    <textarea name="message" class="form-control" placeholder="Mensaje..." required></textarea>
                </div>
            
                <div class="col-md-12 col-sm-12">
                    <button type="submit" class="btn btn-success btn-primary small-btn">Envíar</button>	
                </div>	
                
            </form>
        </div>

        
        
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        $('#contactForm').submit(function(e) {
            
            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');
            var token = $('#csrf-token-id').attr('content');
            
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': token},
                url: url,
                data: new FormData(this),
                contentType:false,
                processData: false,
                success: function(response)
                {
                    Swal.fire({
                        type: response.status,
                        title: response.title,
                        text: response.message,
                    })
                    if(response.status == 'success')
                        $('#contactForm').trigger("reset");

                }
            });
            
        });
    </script>
    
@endsection