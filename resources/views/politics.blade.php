@extends('layouts.app')

@section('content')
    <!-- Header Title Start -->
    <section class="inner-header-title"  style="background-image:url({{ asset('/img/banners/contact-banner.jpg') }});">
        <div class="container">
            <h1>Términos y Condiciones</h1>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- Header Title End -->

    <!-- General Detail Start -->
    <div class="detail-desc section">
        <div class="container white-shadow" style="margin: auto;box-shadow: 2px 4px 13px #9c9898;height:auto;">
            <section style="height:720px;" class="scroll-text">
                @include('text.politics')
            </section>
        </div>
    </div>
@endsection