<div class="bkng-tb-cntnt d-none" v-bind:class="{ active: isActive }">
    <div class="pymnts">
    <form action="{{ route('payment.new') }}" method="POST" id="card-form">
           @csrf
            <input type="hidden" name="job_id" value="{{ $job->id }}">
            <input type="hidden" name="type" value="{{ $type }}">
            
            <div class="pymnt-itm card active center">
                <span style="top: 15px;color: white;font-size: 42px;" id="close-form" class="close-2 cursor">×</span>
                    <div class="card-expl">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4>Todas las Tarjetas de credito y débito:</h4>
                            <img src="{{ asset('img/conekta/cards1.png') }}" alt="">
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <h4>Transacciones realizadas vía:</h4>
                            <img src="{{ asset('img/conekta/conekta.png') }}" alt="">
                        </div>
                    </div>
                <div class="pymnt-cntnt">
                    <span class="card-errors"></span>
                    <div class="sctn-row">
                        <div class="sctn-col l">
                            <label>Nombre del titular</label>
                            <input type="text" class="form-control" placeholder="Como aparece en la tarjeta" autocomplete="off" size="20" data-conekta="card[name]">
                        </div>
                        <div class="sctn-col">
                            <label>Número de tarjeta</label>
                            <input type="number" class="form-control" autocomplete="off" size="20" data-conekta="card[number]"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <label>Fecha de expiración</label>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="number" class="form-control" placeholder="Mes" size="2" data-conekta="card[exp_month]">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <input type="number" class="form-control" placeholder="Año" size="4" data-conekta="card[exp_year]">
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                            <div class="sctn-col cvv"><label>Código de seguridad</label>
                                <input type="number" class="form-control" placeholder="3 dígitos" autocomplete="off"  data-conekta="card[cvc]">
                            </div>
                        </div>
                    </div>
                    <div class="detail-pannel-footer-btn center conekta">
                        <a  @click="myCard" href="javascript:void(0)" title="" class="footer-btn blu-btn">Cancelar</a>
                        <button type="submit" href="javascript:void(0)" title="" class="footer-btn grn-btn button">Pagar</button>
                    </div>
                </div>
            </div>
            <div class="sctn-row">
            </div>
        </form>
    </div>
</div>
