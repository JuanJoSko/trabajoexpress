@extends('layouts.app')

@section('css')
    <style>
        .navbar  {
            background: #1c2733 !important;
        } 
    </style>

@endsection

@section('content')
    <div class="map-option">
        <a style="display: inline-block;margin: auto; width: 128px;" href="/map" class="btn btn-success {{ $type == 'jobs' ? '' : 'map-button-disable'}} ">Trabajos</a>
        <a style="display: inline-block;margin: auto; width: 128px;" href="/map/services" class="btn btn-success {{ $type == 'services' ? '' : 'map-button-disable'}}">Negocios</a>
         <div class="map-filter-form">
            <form action="{{ $type == 'jobs' ? route('map.searchMapJob') : route('map.searchMapService')  }}" method="post">
                @csrf
                <div class="input-group">
                    <input name="search" type="text" class="form-control" placeholder="Buscar...">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="content">
        {!! $map['html'] !!}
    </div>
@endsection

@section('scripts')
    <script type='text/javascript'>
        var centreGot = false;
    </script>
    {!! $map['js'] !!}


    
@endsection