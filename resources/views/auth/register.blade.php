@extends('layouts.front')

@section('nameBtn', 'Entrar')
@section('urlBtn', '/login')

@section('content')
<div class="wrapper">  
			
        <!-- Title Header Start -->
        <section class="signup-screen-sec">
            <div class="container">
                <div class="signup-screen">
                        <a href="/"><img style="max-width: 50%;" src="/img/logo.png" class="fix-border-radius img-responsive" alt=""></a>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Usuario" id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
    
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Nombre" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
    
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            

                            <div class="form-group row">
    
                                <div class="col-md-12">
                                    <input placeholder="Apellidos" id="lastName" type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ old('lastName') }}" required autofocus>
    
                                    @if ($errors->has('lastName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('lastName') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Correo" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
    
                                <div class="col-md-12">
                                    <input placeholder="Contraseña" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
    
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
    
                                <div class="col-md-12">
                                    <input placeholder="Confirmar Contraseña" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>



                            <div class="form-group row">
                                <div class="col-md-12 offset-md-4">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="terms" onchange="this.setCustomValidity(validity.valueMissing ? 'Acepta los términos y condiciones para continuar.' : '');" type="checkbox" required name="terms">
    
                                        <label class="form-check-label" for="terms">
                                            Acepto los <a href="{{ route('terms') }}">Términos y Condiciones</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-4">
                                    <button class="btn btn-login" type="submit" >Registrasrse</button>
                                    <span>¿Ya tienes cuenta? <a href="{{ route('login') }}"> Entrar</a></span>	
                                </div>
                            </div>
                        </form>
                        
                    </form>
                </div>
            </div>
        </section>

</div>
@endsection

@section('scripts')

    <script>

        document.getElementById("terms").setCustomValidity("Acepta los términos y condiciones para continuar.");
    
    </script>
    
@endsection