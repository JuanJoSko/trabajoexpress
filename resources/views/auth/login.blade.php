@extends('layouts.front')
@section('nameBtn', 'Registrarse')
@section('urlBtn', '/register')

@section('content')
    <div class="Loader"></div>
    <div class="wrapper"> 
        <!-- Title Header Start -->
        <section class="login-screen-sec">
            <div class="container">
                <div class="login-screen">
                    <a href="/">
                        <img style="max-width: 50%;" src="/img/logo.png" class="img-responsive fix-border-radius" alt=""></a>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input placeholder="Correo" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input placeholder="Contraseña" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 offset-md-4">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        Recordar cuenta
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <button class="btn btn-login" type="submit">Entrar</button>
                                <span>¿No tienes cuenta? <a href="{{ route('register') }}">Crea una cuenta</a></span>
                                <span>
                                    @if (Route::has('password.request'))
                                        <a style="font-weight:700;" class="btn btn-link" href="{{ route('password.request') }}">
                                            ¿Olvidaste tu contraseña?
                                        </a>
                                    @endif
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        
        
        

    </div>
@endsection

