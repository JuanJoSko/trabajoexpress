@extends('layouts.app')

@section('content')

<section class="inner-header-page">
    <div class="container">
        
        <div class="col-md-8">
            <div class="left-side-container">
                <div class="freelance-image"><a href="company-detail.html"><img src="/img/can-2.png" class="img-responsive img-circle" alt=""></a></div>
                <div class="header-details">
                    <h4>Carlos Sánchez</h4>
                    {{--  <div class="verified-action">Verificado</div>  --}}
                    <p>Estudiante de Ingeniería</p>
                    <ul>
                        <li>
                            <div class="star-rating" data-rating="4.2">
                                <span class="fa fa-star fill"></span>
                                <span class="fa fa-star fill"></span>
                                <span class="fa fa-star fill"></span>
                                <span class="fa fa-star fill"></span>
                                <span class="fa fa-star-half fill"></span>
                            </div>
                        </li>
                        <li><img class="flag" src="http://trabajoexpress.loc/img/mx.png" alt="">Querétaro, Querétaro.</li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-md-4 bl-1 br-gary">
            <div class="right-side-detail">
                <ul>
                    <li><span class="detail-info">Ciudad</span>Querétaro</li>
                    <li><span class="detail-info">Edad:</span>27 Años</li>
                </ul>
                <ul class="social-info">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
        
    </div>
</section>
<div class="clearfix"></div>
<!-- Title Header End -->

<!-- Freelancer Detail Start -->
<section>
    <div class="container">
        
        <div class="col-md-8 col-sm-8">
            <div class="container-detail-box">
            
                <div class="apply-job-header">
                    <h4>Estudiante de Ingeniería</h4>
                    {{-- <a href="company-detail.html" class="cl-success"><span><i class="fa fa-building"></i>App Developer</span></a> --}}
                    <span><i class="fa fa-map-marker"></i>Querétaro, Querétaro</span>
                </div>
                
                <div class="apply-job-detail">
                    <p>Estoy cursando el 4° semestre de Ingeniería, soy un chico responsable, comprometido con ganas de trabajar y ganar dinero extra para continuar mis estudios.</p>
                </div>
                
                <div class="apply-job-detail">
                    <h5>Habilidades</h5>
                    <ul class="skills">
                        <li>Manejo de torno</li>
                        <li>Conducir motocicleta</li>
                        <li>Buena condición</li>
                        <li>Manejo de números</li>
                        <li>Disponibilidad de horario</li>
                        <li>Licencia de manejo</li>
                        <li>Inglés</li>
                    </ul>
                </div>
               <div class="center-btn">
                    <a href="#" class="btn btn-success ">Ofrecer Trabajo</a>
               </div>
                
                
            </div>
            
            <!-- Similar Jobs -->
            <div class="container-detail-box">
            
                <div class="row">
                    <div class="col-md-12">
                        <h4>Calificaciones</h4>
                    </div>
                </div>
                
                <div class="row">
                    
                    <!-- Single Review -->
                    <div class="review-list">
                        <div class="review-thumb">
                            <img src="/img/client-1.jpg" class="img-responsive img-circle" alt="" />
                        </div>
                        <div class="review-detail">
                            <h4>David Luke<span>hace 2 días</span></h4>
                            <span class="re-designation">Repartidor</span>
                            <ul class="start-details-u">
                                <li class="start-details-2">
                                    <div class="star-rating" data-rating="4.2">
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star-half fill"></span>
                                    </div>
                                </li>
                            </ul>
                            <p>Buen muchacho, muy responsable y cuidadoso.</p>
                        </div>
                    </div>
                    
                    <!-- Single Review -->
                    <div class="review-list">
                        <div class="review-thumb">
                            <img src="/img/client-2.jpg" class="img-responsive img-circle" alt="" />
                        </div>
                        <div class="review-detail">
                            <h4>Daniela Martinez<span>hace 10 días</span></h4>
                            <span class="re-designation">Ayudante de bodega</span>
                            <ul class="start-details-u">
                                <li class="start-details-2">
                                    <div class="star-rating" data-rating="5.0">
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                    </div>
                                </li>
                            </ul>
                            <p>Excelente trabajo realizado, lo recomiendo ampliamente.</p>
                        </div>
                    </div>
                    
                    <!-- Single Review -->
                    <div class="review-list">
                        <div class="review-thumb">
                            <img src="/img/client-3.jpg" class="img-responsive img-circle" alt="" />
                        </div>
                        <div class="review-detail">
                            <h4>Charly Montoya<span>hace 4 días</span></h4>
                            <span class="re-designation">Repartidor de volantes</span>
                            <ul class="start-details-u">
                                    <li class="start-details-2">
                                        <div class="star-rating" data-rating="3.9">
                                            <span class="fa fa-star fill"></span>
                                            <span class="fa fa-star fill"></span>
                                            <span class="fa fa-star fill"></span>
                                            <span class="fa fa-star-half fill"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </li>
                                </ul>
                            <p>Buen trabajo.</p>
                        </div>
                    </div>
                    
                    <!-- Single Review -->
                    <div class="review-list">
                        <div class="review-thumb">
                            <img src="/img/client-4.jpg" class="img-responsive img-circle" alt="" />
                        </div>
                        <div class="review-detail">
                            <h4>Ivan Hernández<span>hace 2 días</span></h4>
                            <span class="re-designation">Asesor de Matemáticas</span>
                            <ul class="start-details-u">
                                <li class="start-details-2">
                                    <div class="star-rating" data-rating="5">
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                        <span class="fa fa-star fill"></span>
                                    </div>
                                </li>
                            </ul>
                            <p>Todo muy bien, aprendí y resolvio mis dudas para poder realizar mis tareas de Matemáticas</p>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
        <!-- Sidebar Start-->
        <div class="col-md-4 col-sm-4">
            
            <!-- Make An Offer -->
            <div class="sidebar-container">
                <div class="sidebar-box">
                    <div class="sidebar-inner-box">
                        <div class="sidebar-box-thumb">
                            <img src="/img/can-2.png" class="img-responsive img-circle" alt="" />
                        </div>
                        <div class="sidebar-box-detail">
                            <h4>Carlos Sánchez</h4>
                            <span class="desination">Estudiante de Ingeniería</span>
                        </div>
                    </div>
                    <div class="sidebar-box-extra">
                        <ul class="status-detail">
                            <li class="br-1"><strong>0 </strong>Solicitados</li>
                            <li class="br-1"><strong>52</strong>Visitas</li>
                            <li><strong>44</strong>Completados</li>
                        </ul>
                    </div>
                </div>
                <a href="#" class="btn btn-sidebar bt-1 bg-success">Envíar Mensaje</a>
            </div>
            
            
        </div>
        <!-- End Sidebar -->
        
    </div>
</section>

@endsection