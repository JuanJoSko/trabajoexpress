<nav id="navHeader" class=" nav-trans navbar navbar-default navbar-fixed navbar-light white bootsnav">
    <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="fa fa-bars"></i></button>
        <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/') }}">
        <img id="logoID"  src="{{ asset('/img/logo_horizontal-white.png') }}" class="logo logo-scrolled" alt="">
            </a>
        </div>
        <div  style="padding-top: 2%;" class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                <li><a href="{{ route('job.index') }}"><i class="fa fa-home" aria-hidden="true"></i>Inicio</a></li>
                <li><a href="{{ route('map') }}"><i class="fa fa-map-marker" aria-hidden="true"></i>Mapa</a></li>
                @guest
                    <li><a href="{{ route('contact.index') }}"><i class="fa fa-envelope-o"></i>Contactanos</a></li>
                    {{--  <li><a href="blog.html">Blog</a></li>  --}}
                    <li><a href="{{ route('register') }}"><i class="fa fa-pencil" aria-hidden="true"></i>Registrarse</a></li>
                    <li class="left-br"><a href="{{ route('login') }}" class="signin">Entrar</a></li>
               
                @else
                    <li><a href="{{ route('contact.index') }}"><i class="fa fa-envelope"></i>Contactanos</a></li> 
                    <ul class="nav navbar-nav" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="dropdown megamenu-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus-circle" aria-hidden="true"></i> Ofrecer</a>
                            <ul class="dropdown-menu megamenu-content animated fadeOutUp" role="menu" style="display: none; opacity: 1;">
                                <li><a href="{{ route('job.create') }}"><i class="fa fa-suitcase"></i> Ofrecer Trabajo</a></li>
                                <li><a href="{{ route('service.create') }}"><i class="fa fa-search" aria-hidden="true"></i> Publicar Negocio</a></li>     
                            </ul>
                        </li>
                    </ul>

                    {{--  <li>
                        <a href="blog.html"><i class="fa fa-blog" aria-hidden="true"></i>
                            Blog
                        </a>
                    </li>  --}}
                    @if (Auth::user()->permiss == 'Admin')
                        <li>
                            <a href="{{ route('admin.dashboard') }}"><i class="fa fa-blog" aria-hidden="true"></i>
                                Admin
                            </a>
                        </li>
                    @endif
                 
                    <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="signin" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->username }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/profile/{{ Auth::user()->id }}"><i class="fa fa-user fa-lg" aria-hidden="true"></i> Mi Pefil</a>
                                <a class="dropdown-item" href="{{ route('myJobs') }}"><i class="fa fa-briefcase" aria-hidden="true"></i> Mis Trabajos</a>
                                <a class="dropdown-item" href="{{ route('myServices') }}"><i class="fa fa-home" aria-hidden="true"></i> Mis Negocios</a>
                                <a class="dropdown-item" href="{{ route('postulations.index') }}"><i class="fa fa-search" aria-hidden="true"></i> Mis Postulaciones</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out fa-lg" aria-hidden="true"></i> Cerrar Sesión
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                            
                        </li>
                        
                @endguest

            </ul>
        </div>
    </div>
</nav>

@auth
    <div id="notify" class="notification-box pointer">
        @if(count($notifys) > 0)
            <span class="notification-count">{{ count($notifys) }}</span>
        @endif  
        <div class="notification-bell {{ count($notifys) > 0 ? '' : 'stop-anim-bell'}}">
            <span class="bell-top"></span>
            <span class="bell-middle"></span>
            <span class="bell-bottom"></span>
            <span class="bell-rad {{ count($notifys) > 0 ? '' : 'stop-anim-bell'}}"></span>
        </div>
    </div>

    <div id="notifyMenu" 
    class="dropdown-menu-hover-primary dropdown-menu menu-notification animated fadeInRight">
    <div class=" dropdown-menu-header mb-0">
            <div class="dropdown-menu-header-inner bg-deep-blue">
                <div class="menu-header-image opacity-1" ></div>
                <div class="menu-header-content text-dark">
                    <h5 class="menu-header-title">Notificaciones</h5>
                    <h6 class="menu-header-subtitle">Tienes <b>{{count($notifys)}}</b> nueva notificación sin ver</h6>
                </div>
            </div>
        </div>
        @foreach ($notifys as $notify)
            <div class="vertical-time-simple">
                <a href="/job/{{$notify->slug}}">
                    <h6 tabindex="-1" class="dropdown-header">{{$notify->title}}</h6>
                    <div class="vertical-timeline-item dot-success vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">{{$notify->msg}}
                                </h4>
                                <span class="vertical-timeline-element-date"></span></div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
        @if(count($notifys) > 0)
            <div class="vertical-time-simple">
                <a href="/notification/job/{{$notify->id}}">
                    <div class="text-center">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">Borrar todas las notificaciones
                                </h4>
                                <span class="vertical-timeline-element-date"></span></div>
                        </div>
                    </div>
                </a>
            </div>
        @endif
    </div>
@endauth
