<footer class="footer">
    <div class="row lg-menu">
        <div class="container">
           
            <div class="col-md-8 co-sm-8 pull-right">
                <ul class="footer-social">
                    <li>
                        <a href="https://www.facebook.com/Trabajo-Express-402394130580243/">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li><a href="https://www.instagram.com/trabajo_express/">
                        <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row no-padding">
        <div class="container">
            <div class="col-md-4 col-sm-4">
                <div class="footer-widget">
                    <img class="logo-footer" src="{{ asset('img/logo_white.png') }}" alt="">
                </div>
            </div>
            <div class="col-md-3 col-sm-12">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">Encuentranos en:</h3>
                    <div class="textwidget">
                        <p>WTC Querétaro - Paseo de la República Km 13020, 76230 .
                            <br>Juriquilla, Qro</p>
                        <p><strong>Email:</strong> contacto@trabajoexpress.com</p>
                        <p><strong>Telefóno:</strong> <a href="tel:+4425817797">4425817797</a></p>
                      
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">Navegación</h3>
                    <div class="textwidget">
                        <div class="textwidget">
                            <ul class="footer-navigation">
                                <li><a href="{{ route('map') }}" title="">Mapa</a></li>
                                <li><a href="{{ route('home') }}" title="">Inicio</a></li>
                                <li><a href="{{ route('contact.index') }}" title="">Contactanos</a></li>
                                <li><a href="{{ route('job.create') }}" title="">Ofrecer Trabajo</a></li>
                                <li><a href="{{ route('service.create') }}" title="">Ofrecer Negocio</a></li>
                                <li><a href="" title="">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-4">
                <div class="footer-widget">
                    <h3 class="widgettitle widget-title">Nuestras Políticas:</h3>
                    <div class="textwidget">
                        <ul class="footer-navigation">
                            <li><a href="{{route('politics')}}" title="">Políticas de Privacidad</a></li>
                            <li><a href="" title="">Políticas de Pago</a></li>
                            <li><a href="{{route('terms')}}" title="">Términos y Condiciones de uso</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row copyright">
        <div class="container">
            <p>Copyright © 2019 - Todos los derechos reservados a TrabajoExpress®. Powered by <a href="https://matgagroup.com">Matga Grupo Empresarial</a></p>
        </div>
    </div>
</footer>
