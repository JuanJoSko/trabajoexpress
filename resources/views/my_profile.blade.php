@extends('layouts.app')

@section('content')

<!-- Title Header Start -->
<section class="inner-header-title" style="background-image:url({{ asset('/img/banners/banner-10.jpg') }});">
    <div class="container">
        <h1>MI PERFIL</h1>
    </div>
</section>
<div class="clearfix"></div>


<!-- Candidate Profile Start -->
<section class="detail-desc advance-detail-pr gray-bg">
        <div class="container white-shadow">
        
            <div class="row">
                <div class="detail-pic" id ="picUser">
                        {{-- src="{{ asset('/img/can-1.png') }} --}}
                    <img v-if="url" :src="url"  class="img" alt="" />
                    <a @click="trigger" href="javascript:void(0)" class="detail-edit" title="edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <input class ="d-none" type="file" name="" @change="onFileChange"  ref="fileInput">
                </div>
            </div>
            
            <div class="row bottom-mrg">
                <div class="col-md-12 col-sm-12">
                    <div class="advance-detail detail-desc-caption">
                        <div class="stars">
                            @for ($i = 0; $i < Auth::user()->stars(); $i++)
                            <i class="fa fa-star fill"></i>
                            @endfor
                            @for ($i = 0; $i < (5 - Auth::user()->stars()); $i++)
                                <i class="fa fa-star"></i>
                            @endfor
                        </div>
                        <h4>{{ Auth::user()->name }}</h4>
                        <span class="designation">{{ Auth::user()->curriculum != null ? Auth::user()->curriculum->title: 'Sin Titulo'}}</span>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="full-detail-description full-detail gray-bg">
        <div class="container">
            <div class="col-md-12 col-sm-12">
                @include('commons.msg')
                <div class="full-card">
                  <div class="deatil-tab-employ tool-tab">
                        <ul class="nav simple nav-tabs" id="simple-design-tab">
                            <li class="active"><a href="#about">Acerca de mi</a></li>
                            <li><a href="#address">Datos Personales</a></li>
                        </ul>
                        
                        <!-- Start All Sec -->
                        <div class="tab-content">
                            <div id="about" class="tab-pane
                             in active">
                                <div class="edit-button">
                                    <a  v-bind:class="{ hidden: isActive }"  @click="myAbout"  href="javascript:void(0)" class="detail-edit" title="edit"><i class="fa fa-pencil"></i></a>
                                </div>
                                <div  v-bind:class="{ hidden: isActive }">
                                    <h3 style ="text-align: center;" >{{ Auth::user()->curriculum != null ? Auth::user()->curriculum->title: 'Sin Titulo'}}</h3>
                                    <p>{{Auth::user()->curriculum != null ? Auth::user()->curriculum->description: 'Sin Descripción'}}</p>
                                </div>
                                
                                <div class="d-none edit-pro" v-bind:class="{ active: isActive }" >
                                    <div class="row no-mrg">
                                            <h3 style ="text-align: center;" >Acerca de mi</h3>
                                        <form action="{{ route('myProfile.updateAbout') }}" method="post">
                                            @csrf
                                            <div class="col-md-6 col-sm-12">
                                                <input name = "title" type="text" value="{{ Auth::user()->curriculum != null ? Auth::user()->curriculum->title: 'Sin Titulo'}}" class="form-control" placeholder="Titulo">
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <select class="form-control" name="genere" id="">
                                                    <option value="Hombre">Hombre</option>
                                                    <option value="Mujer">Mujer</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <textarea  style ="width: 100%;" class="form-control" placeholder="Descripción" name="description" id=""  rows="10">{{Auth::user()->curriculum != null ? Auth::user()->curriculum->description: 'Sin Descripción'}}
                                                </textarea>
                                            </div>
                                            
                                            <div class="detail-pannel-footer-btn center">
                                                <a @click="myAbout" href="javascript:void(0)" class="footer-btn blu-btn" title="">Cancelar</a>
                                                <button type="submit" href="javascript:void(0)" class="footer-btn grn-btn" title="">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End About Sec -->
                            
                            <!-- Start Address Sec -->
                            <div id="address" class="tab-pane">
                                <div class="row no-mrg">
                                    <div class="edit-button">
                                        <a  v-bind:class="{ hidden: isActive }"  @click="myAbout"  href="javascript:void(0)"  class="detail-edit" title="edit"><i class="fa fa-pencil"></i></a>
                                    </div>
                                    <div  v-bind:class="{ hidden: isActive }">
                                            <div class="col-md-6 col-sm-12">
                                            <h4 class="center">Datos de Cuenta</h4>
                                            <ul class="job-detail-des">
                                                <li><span>Usuario:</span>{{Auth::user()->username}}</li>
                                                <li><span>Nombre:</span>{{Auth::user()->name}}</li>
                                                <li><span>Apellido:</span>{{Auth::user()->lastName}}</li>
                                                <li><span>Correo:</span>{{Auth::user()->email}}</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <h4 class="center">Datos de Contato</h4>
                                            <ul class="job-detail-des">
                                                <li><span>Dirección:</span>{{Auth::user()->address}}</li>
                                                <li><span>Ciudad:</span>{{Auth::user()->city}}</li>
                                                <li><span>Estado:</span>{{Auth::user()->state}}</li>
                                                <li><span>Teléfono:</span>{{Auth::user()->phone}}</li>
                                                <li><span>Edad:</span>{{Auth::user()->age}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="d-none edit-pro" v-bind:class="{ active: isActive }" >
                                        <div class="edit-pro col-md-6 col-sm-12" >
                                            <h3 class="center">Mi Cuenta</h3>
                                            <div class="col-md-12 col-sm-12">
                                            <input type="text" value="{{Auth::user()->name}}" class="form-control" placeholder="Nombre">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="text" value="{{Auth::user()->lastName}}" class="form-control" placeholder="Apellido">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="email" value="{{Auth::user()->email}}" placeholder="Correo" class="form-control" placeholder="dana.mathew@gmail.com">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="password" value="trabajoexpress" class="form-control" placeholder="Contraseña">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="password" value="trabajoexpress" class="form-control" placeholder="Confirmar Contraseña">
                                            </div>
                                           
                                        </div>
                                        <div class="edit-pro col-md-6 col-sm-12" >
                                            <h3 class="center">Datos de Contato</h3>
                                            <div class="col-md-12 col-sm-12">
                                            <input type="text" value="{{Auth::user()->address}}" class="form-control" placeholder="Dirección">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="text" value="{{Auth::user()->city}}" class="form-control" placeholder="Ciudad">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="text" value="{{Auth::user()->state}}" placeholder="Correo" class="form-control" placeholder="Estado">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="text" value="{{Auth::user()->phone}}" class="form-control" placeholder="Estado">
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <input type="text" value="{{Auth::user()->birdthay}}" class="form-control" placeholder="Ciudad">
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 mt-3">
                                            <div class="detail-pannel-footer-btn center">
                                                <a @click="myAbout" href="javascript:void(0)" class="footer-btn blu-btn" title="">Cancelar</a>
                                                <a href="javascript:void(0)" class="footer-btn grn-btn" title="">Guardar</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- End Address Sec -->
                            
                            <!-- Start Message -->
                            <div id="messages" class="tab-pane fade">
                                <div class="inbox-body inbox-widget">
                                    <div class="mail-card">
                                        <header class="card-header cursor-pointer collapsed" data-toggle="collapse" data-target="#full-message" aria-expanded="false">
                                            <div class="card-title flexbox">
                                              <img class="img-responsive img-circle avatar" src="{{ asset('/img/can-1.png') }}" alt="...">
                                              <div>
                                                <h6>Daniel Duke</h6>
                                                <small>Hoy a las 07:34 AM</small>
                                                <small><a class="text-info collapsed" href="#detail-view" data-toggle="collapse" aria-expanded="false">Ver detalles...</a></small>

                                                <div class="no-collapsing cursor-text collapse" id="detail-view" aria-expanded="false" style="height: 0px;">
                                                  <small class="d-inline-block">From:</small>
                                                  <small>theadmin@thetheme.io</small>
                                                  <br>
                                                  <small class="d-inline-block">To:</small>
                                                  <small>subscriber@yahoo.com</small>
                                                </div>
                                              </div>
                                            </div>
                                        </header>
                                        <div class="collapse" id="full-message" aria-expanded="false" style="height: 0px;">
                                            <div class="card-body">
                                                <P>Me interesa el trabajo, ya he tenido experiencia en este tipo de trabajos.<P>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="mail-card unread">
                                        <header class="card-header cursor-pointer collapsed" data-toggle="collapse" data-target="#meaages-2" aria-expanded="false">
                                            <div class="card-title flexbox">
                                              <img class="img-responsive img-circle avatar" src="{{ asset('/img/can-2.png') }}" alt="...">
                                              <div>
                                                <h6>Daniel Duke</h6>
                                                <small>Hoy a las 07:34 AM</small>
                                                <small><a class="text-info collapsed" href="#detail-view1" data-toggle="collapse" aria-expanded="false">Ver detalles...</a></small>

                                                <div class="no-collapsing cursor-text collapse" id="detail-view1" aria-expanded="false" style="height: 0px;">
                                                  <small class="d-inline-block">From:</small>
                                                  <small>theadmin@thetheme.io</small>
                                                  <br>
                                                  <small class="d-inline-block">To:</small>
                                                  <small>subscriber@yahoo.com</small>
                                                </div>
                                              </div>
                                            </div>
                                        </header>
                                        <div class="collapse" id="meaages-2" aria-expanded="false" style="height: 0px;">
                                            <div class="card-body">
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="mail-card">
                                        <header class="card-header cursor-pointer collapsed" data-toggle="collapse" data-target="#meaages-3" aria-expanded="false">
                                            <div class="card-title flexbox">
                                              <img class="img-responsive img-circle avatar" src="{{ asset('/img/can-1.png') }}" alt="...">
                                              <div>
                                                <h6>Daniel Duke</h6>
                                                <small>Hoy a las 07:34 AM</small>
                                                <small><a class="text-info collapsed" href="#detail-view2" data-toggle="collapse" aria-expanded="false">Ver detalles...</a></small>

                                                <div class="no-collapsing cursor-text collapse" id="detail-view2" aria-expanded="false" style="height: 0px;">
                                                  <small class="d-inline-block">From:</small>
                                                  <small>theadmin@thetheme.io</small>
                                                  <br>
                                                  <small class="d-inline-block">To:</small>
                                                  <small>subscriber@yahoo.com</small>
                                                </div>
                                              </div>
                                            </div>
                                        </header>
                                        <div class="collapse" id="meaages-3" aria-expanded="false" style="height: 0px;">
                                            <div class="card-body">
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="mail-card">
                                        <header class="card-header cursor-pointer collapsed" data-toggle="collapse" data-target="#meaages-4" aria-expanded="false">
                                            <div class="card-title flexbox">
                                              <img class="img-responsive img-circle avatar" src="{{ asset('/img/can-3.png') }}" alt="...">
                                              <div>
                                                <h6>Daniel Duke</h6>
                                                <small>Hoy a las 07:34 AM</small>
                                                <small><a class="text-info collapsed" href="#detail-view3" data-toggle="collapse" aria-expanded="false">Ver detalles...</a></small>

                                                <div class="no-collapsing cursor-text collapse" id="detail-view3" aria-expanded="false" style="height: 0px;">
                                                  <small class="d-inline-block">From:</small>
                                                  <small>theadmin@thetheme.io</small>
                                                  <br>
                                                  <small class="d-inline-block">To:</small>
                                                  <small>subscriber@yahoo.com</small>
                                                </div>
                                              </div>
                                            </div>
                                        </header>
                                        <div class="collapse" id="meaages-4" aria-expanded="false" style="height: 0px;">
                                            <div class="card-body">
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="mail-card unread">
                                        <header class="card-header cursor-pointer collapsed" data-toggle="collapse" data-target="meaages-5" aria-expanded="false">
                                            <div class="card-title flexbox">
                                              <img class="img-responsive img-circle avatar" src="{{ asset('/img/can-4.png') }}" alt="...">
                                              <div>
                                                <h6>Daniel Duke</h6>
                                                <small>Hoy a las 07:34 AM</small>
                                                <small><a class="text-info collapsed" href="#detail-view4" data-toggle="collapse" aria-expanded="false">Ver detalles...</a></small>

                                                <div class="no-collapsing cursor-text collapse" id="detail-view4" aria-expanded="false" style="height: 0px;">
                                                  <small class="d-inline-block">From:</small>
                                                  <small>theadmin@thetheme.io</small>
                                                  <br>
                                                  <small class="d-inline-block">To:</small>
                                                  <small>subscriber@yahoo.com</small>
                                                </div>
                                              </div>
                                            </div>
                                        </header>
                                        <div class="collapse" id="meaages-5" aria-expanded="false" style="height: 0px;">
                                            <div class="card-body">
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="mail-card">
                                        <header class="card-header cursor-pointer">
                                            <div class="card-title flexbox">
                                              <img class="img-responsive img-circle avatar" src="{{ asset('/img/can-4.png') }}" alt="...">
                                              <div>
                                                <h6>Daniel Duke</h6>
                                                <small>Hoy a las 07:34 AM</small>
                                                <small><a class="text-info collapsed" href="#detail-view-6" data-toggle="collapse" aria-expanded="false">Ver detalles...</a></small>

                                                <div class="no-collapsing cursor-text collapse" id="detail-view-6" aria-expanded="false" style="height: 0px;">
                                                  <small class="d-inline-block">From:</small>
                                                  <small>theadmin@thetheme.io</small>
                                                  <br>
                                                  <small class="d-inline-block">To:</small>
                                                  <small>subscriber@yahoo.com</small>
                                                </div>
                                              </div>
                                            </div>
                                        </header>
                                        <div class="" id="meaages-6">
                                            <div class="card-body">
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.</p>
                                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>
                                              <hr>
                                              <h5 class="text-lighter">
                                                <i class="fa fa-paperclip"></i>
                                                <small>Attchments (3)</small>
                                              </h5>
                                                <div class="attachment-block">
                                                    <div class="thumb">
                                                        <img src="{{ asset('/img/gallery1.jpg') }}" alt="img" class="img-responsive">
                                                    </div>
                                                    <div class="attachment-info">
                                                        <h6>Profile Pic  <span>( 1.69 mb )</span></h6>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Descargar</a></li>
                                                            <li><a href="javascript:void(0)">View</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="attachment-block">
                                                    <div class="thumb">
                                                        <img src="{{ asset('/img/gallery2.jpg') }}" alt="img" class="img-responsive">
                                                    </div>
                                                    <div class="attachment-info">
                                                        <h6>Profile Pic  <span>( 1.69 mb )</span></h6>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Descargar</a></li>
                                                            <li><a href="javascript:void(0)">View</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="attachment-block">
                                                    <div class="thumb">
                                                        <img src="{{ asset('/img/gallery3.jpg') }}" alt="img" class="img-responsive">
                                                    </div>
                                                    <div class="attachment-info">
                                                        <h6>Profile Pic  <span>( 1.69 mb )</span></h6>
                                                        <ul>
                                                            <li><a href="{{ asset('/img/gallery3.jpg') }}" download>Descargar</a></li>
                                                            <li><a href="javascript:void(0)">View</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- End Message -->
                        </div>
                        <!-- Start All Sec -->
                    </div>  
                </div>
            </div>
        </div>
    </section>
    <!-- Candidate Profile End -->
    
@endsection

@section('scripts')
<script src="{{ asset('/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('/plugins/js/bootstrap.min.js') }}"></script>

			
<script>
    new Vue({
      el: '#about',

      data: {
          isActive: false
      },

      methods: {
        myAbout: function(){
            this.isActive = !this.isActive;
          // some code to filter users
        }
      }
    });

    new Vue({
      el: '#address',

      data: {
          isActive: false
      },

      methods: {
        myAbout: function(){
            this.isActive = !this.isActive;
          // some code to filter users
        }
      }
    });

    new Vue({
      el: '#picUser',
      data:{
        url: 'img/users/avatar.png',
      },
        methods: {
            trigger () {
                this.$refs.fileInput.click()
            },
            onFileChange(e) {
                const file = e.target.files[0];
                this.url = URL.createObjectURL(file);
            }
        }
    });

    
</script>
    
@endsection