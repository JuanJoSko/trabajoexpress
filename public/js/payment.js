$(document).ready(function () {
    //content-wrap
    var ant;
    $('.nextBtn').on('click', function(){
        if($('#checkTerms').is(":checked") ||  $(this).attr('data-check') != 'terms'){
            $(document).scrollTop(20);
            $('#section-circle-1').removeClass('content-current');
            $('#section-circle-2').removeClass('content-current');
            $('#section-circle-3').removeClass('content-current');
            $('#tab-1').removeClass('tab-current');
            $('#tab-2').removeClass('tab-current');
            $('#tab-3').removeClass('tab-current');
        }
        if($(this).attr('data-check') == 'terms'){
            if($('#checkTerms').is(":checked")){
                $('#section-circle-'+$(this).attr('id')).addClass('content-current');
                $('#tab-'+$(this).attr('id')).addClass('tab-current');
            }else{
                $('#exampleModalCenter').css('display', 'block');
            }
        }else{
            $('#section-circle-'+$(this).attr('id')).addClass('content-current');
            $('#tab-'+$(this).attr('id')).addClass('tab-current');
        }
    });
    function closeModal(){
        $('#exampleModalCenter').css('display', 'none');
        
    }
   
    $('.closeModal').click(function(){
          closeModal(); 
    });
    
    $('#addCard').on('click', function(){
        $('.bkng-tb-cntnt').css('display', 'block');
        $('#myCards').css('display', 'none');
    });

    $('.cardSelect').change(function() {
        if(this.checked) {
            $('#card'+ant).css('display','none');
            $('#select'+ant).prop('checked', false);
            $('#idCard').val($(this).val());
            $('#card'+$(this).val()).css('display','block');
            ant = $(this).val();

        }else{
            $('#idCard').val(-1);
            $('#card'+ant).css('display','none');
        }
    });

    $("#process-form").submit(function(event) {
        var $form = $(this);
        // Previene hacer submit más de una vez
        $form.find("button").prop("disabled", true);
        ProcessPay();
        return false;
      });

    var ProcessPay = function() {
        var $form = $("#process-form");
        $form.get(0).submit(); //Hace submit
    };

    $("#close-form").on('click', function(){
        $('.bkng-tb-cntnt').css('display', 'none');
        $('#myCards').css('display', 'block');
    });
});


