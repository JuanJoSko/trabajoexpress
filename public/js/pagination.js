$( document ).ready(function() {

    var route = "/job";
    var page;
    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        page = $(this).attr('href').split('page=')[1];
       
        $.ajax({
            url: route,
            data: {page: page},
            type: 'GET',
            dataType: 'json',
            success: function(data){
                $('.jobs').html(data);
            }
        });
    });

    $('.process-img').click(function(){
        $('.process-img').removeClass('progress-active');
        $(this).addClass('progress-active');
        route = '/'+$(this).attr('id');
        page = 1;
        $.ajax({
            url: route,
            data: {page: page},
            type: 'GET',
            dataType: 'json',
            success: function(data){
                $('.jobs').html(data);
            }
        });

    });
});
