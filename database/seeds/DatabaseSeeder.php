<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $array = array(
            'Abarrotes',
            'Abogado',
            'Accesorios para auto',
            'Accesorios para Celulares',
            'Accesorios para moto',
            'Aceros y Perfiles',
            'Acuario',
            'Agencia de Viajes',
            'Aguachiles',
            'Alarmas y camaras de seguridad',
            'Albañil',
            'Alfombras',
            'Alimento para Animales',
            'Alimento para mascotas',
            'Alineacion y Balanceo',
            'Alitas',
            'Almuerzos',
            'Alternadores y Marchas',
            'Amortiguadores',
            'Antiguedades',
            'Antojitos',
            'Aparatos Ortopedicos',
            'Artesanias',
            'Articulos de Baño',
            'Articulos para Bebe',
            'Articulos Religiosos',
            'Asesoria Contable',
            'Asesoria Legal',
            'Audio y Video',
            'Auto Lavado',
            'Automotriz',
            'Baguettes',
            'Banda de Musica',
            'Banquetes',
            'Bar',
            'Barbacoa',
            'Barberia',
            'Bazar',
            'Billar',
            'Birria',
            'Bistro',
            'Bisuteria',
            'Bizcocheria',
            'Bodegas',
            'Boliche',
            'Bordadora',
            'Botanas',
            'Botica',
            'Boutique Automotriz',
            'Boutique de Ropa',
            'Bufete Juridico',
            'Buffet',
            'Burritos',
            'Cabrito',
            'Café y Donas',
            'Cafetería',
            'Caldos de Gallina',
            'calentadores solares',
            'Calzado Deportivo',
            'Camaras de Seguridad',
            'Cambio de aceite',
            'Cancha de Futbol',
            'Cancha de Squash',
            'Cancha de Tenis',
            'Cantante',
            'Cantina',
            'Cantina Familiar',
            'Cardiologo',
            'Carnes asadas',
            'Carnes en su jugo',
            'Carnes frías',
            'Carnicería',
            'Carnitas',
            'Carpintero',
            'Cecina',
            'Cenaduria',
            'Cenas',
            'Centro de Copiado',
            'Cerraduras',
            'Cerrajero',
            'Cervecería',
            'Chilaquiles',
            'Chiles secos',
            'Churros',
            'Churros rellenos',
            'Clases de Aleman',
            'Clases de Chino',
            'Clases de Frances',
            'Clases de Ingles',
            'Clases de Italiano',
            'Clases de Manejo',
            'Clases de Musica',
            'Clases de Portugues',
            'Clases de Yoga',
            'Clases de Zumba',
            'Clinica',
            'Closets y Cocinas',
            'Clutch y Frenos',
            'Cochinita pibil',
            'Cocinas Integrales',
            'Comercializacion',
            'Comercializadora',
            'Comida',
            'Comida Alemana',
            'Comida Americana casual',
            'Comida Arabe',
            'Comida Argentina',
            'Comida Brasileña',
            'Comida Cantonesa',
            'Comida china',
            'Comida corrida',
            'Comida Francesa',
            'Comida Gourmet',
            'Comida Italiana',
            'Comida Japonesa',
            'Comida Oxaqueña',
            'Comida para llevar',
            'Comida Rapida',
            'Comida yucateca',
            'Comidas a domicilio',
            'Complementos Alimenticios',
            'Computadoras de auto',
            'Conexiones y Mangueras',
            'Consultorio',
            'Contador',
            'Cortes finos de carne',
            'Cortinas y Persianas',
            'Costurero',
            'Cremería',
            'Crepas',
            'Cristaleria y Articulos de Cocina',
            'Decoracion',
            'Dentista',
            'Deposito de cerveza',
            'Desayunos',
            'Despacho Contable',
            'Detallado automotriz',
            'Dietista',
            'Direcciones Hidraulicas',
            'Disfraces',
            'Distribuidora',
            'DJ',
            'Dulceria',
            'Dulces tipicos',
            'Duplicado de Llaves',
            'Dureria (Chicharron)',
            'Ebanista',
            'Edecanes',
            'Electricista',
            'Electrico Automotriz',
            'Elotes y Esquites',
            'Empanadas',
            'Empeños',
            'Enchiladas',
            'Encurtidos en vinagre',
            'Ensaladas',
            'Equipo de Rehabilitacion',
            'Equipo de Seguridad',
            'Equipo dental',
            'Equipo Medico',
            'Equipo para Laboratorio',
            'Escuela de Baile',
            'Escuela de Idiomas',
            'Escuela de Ingles',
            'Escuela de Karate',
            'Escuela de Kick boxing',
            'Escuela de Manejo',
            'Escuela de Natacion',
            'Escuela de Taekwondo',
            'Escuela de Yoga',
            'Estereos y Alarmas para auto',
            'Estetica',
            'Estetica Canina',
            'Estilista',
            'Estudio Fotografico',
            'Farmacia',
            'Filtros y Aceites',
            'Fletes Economicos',
            'Floreria',
            'Fontanero',
            'Fotocopias',
            'Fotografia y Video',
            'Frutas y verduras',
            'Funeraria',
            'Garrafones de agua',
            'Gelatinas',
            'Gimnasio',
            'Ginecologo',
            'Gorditas',
            'Granos y Semillas',
            'Grupo Musical',
            'Grupo Norteño',
            'Guardias de Seguridad',
            'Hamburguesas',
            'Herrero',
            'Hilos y Estambres',
            'Hojalateria y pintura',
            'Hospital',
            'Hostal',
            'Hot dogs',
            'Hotel',
            'Huaraches',
            'Impermeabilizantes',
            'Importadora',
            'Imprenta',
            'Inmobiliaria',
            'Instrumentos Musicales',
            'Jamón',
            'Jardinero',
            'Joyeria',
            'Joyero',
            'Juegos Inflables',
            'Jugos y licuados',
            'Jugueteria',
            'Laboratorio',
            'Lamparas y Candiles',
            'Lavado automotriz',
            'Lavado de alfombras y muebles',
            'Lavado y detallado automotriz',
            'Lavanderia',
            'Librería',
            'Loncheria',
            'Lonches',
            'Madereria',
            'Mago',
            'Malteadas',
            'Manicura',
            'Mantenimiento aire acondicionado',
            'Manualidades',
            'Maquinaria para Construccion',
            'Marcos y Cuadros',
            'Mariachi',
            'Mariscos',
            'Marroquineria',
            'Masajes',
            'Mascotas',
            'Material Didactico',
            'Material Educativo',
            'Material Electrico y de Iluminacion',
            'Materiales de Construccion',
            'Materias primas',
            'Mecanico',
            'Mecanico General',
            'Medico General',
            'Mensajeria y Paqueteria',
            'Menudo',
            'Merceria',
            'Mesas y Sillas Carpas',
            'Meseros',
            'Mezcalería',
            'Micelanea',
            'Micheladas',
            'Mini Super',
            'Mobiliario de Acero Inoxidable',
            'Mofles',
            'Mole',
            'Molino',
            'Motel',
            'Mudanzas',
            'Muebles',
            'Muebles para Bebe',
            'Nachos',
            'Neveria',
            'Notaria Publiaca',
            'Novedades y Regalos',
            'Nutriologo',
            'Optica',
            'Ortodoncista',
            'Outlet',
            'Oxigeno',
            'Paleteria',
            'Panadería',
            'Pancita',
            'Pañales Desechables',
            'Papeleria',
            'Partes de colision',
            'Partes Usadas',
            'Paseador de perros',
            'Pasta y pizza',
            'Pastas',
            'Pastelería',
            'Pasteles',
            'Pastes',
            'Payaso',
            'Pedicura',
            'Peluquero',
            'Perfumeria',
            'Perifoneo',
            'Pescadería',
            'Pescados y Mariscos',
            'Pigmentos',
            'Pilates',
            'Pintura de casa',
            'Pinturas ',
            'Piñatas',
            'Pisos y Azulejos',
            'Pizzas',
            'Plomero',
            'Podologo',
            'Pollería',
            'Pollo a la leña',
            'Pollo asado',
            'Pollo frito',
            'Pollo rostizado',
            'Pollo y pizza',
            'Posada',
            'Postres',
            'Pozole',
            'Prestamos',
            'Productos de Belleza',
            'Productos Gourmet',
            'Productos naturistas',
            'Productos para Fiestas',
            'Productos para limpieza',
            'Promocionales',
            'Psicologo',
            'Publicidad',
            'Puertas automaticas',
            'Puertas electricas',
            'Purificadora de agua',
            'Quesadillas',
            'Queso',
            'Quimicos',
            'Quiropractico',
            'Radiadores',
            'Raspados',
            'Recicladora',
            'Refacciones para auto',
            'Refacciones para bicicletas',
            'Refacciones para moto',
            'Refrigeracion y Congelacion',
            'Regalos',
            'Rehabilitacion Fisica',
            'Relaciones Publicas',
            'Relojeria',
            'Renta de Autos',
            'Renta de Bodegas',
            'Renta de Casas',
            'Renta de Cuartos',
            'Renta de Disfaces',
            'Renta de Limusinas',
            'Renta de Oficinas',
            'Renta de Trajes y Smoking',
            'Reparacion de calzado',
            'Reparacion de Celulares',
            'Reparacion de Electrodomesticos',
            'Reparacion de herramientas',
            'Reparacion de Hornos',
            'Reparacion de Laptos',
            'Reparacion de Lavadoras',
            'Reparacion de Lavadoras y Refrigeradores',
            'Reparacion de Licuadoras',
            'Reparacion de Refrigeradores',
            'Reparacion de Rines',
            'Reparacion TV y Sonido',
            'Repostería',
            'Restaurant',
            'Restaurant Bar',
            'Restaurante',
            'Restaurante familiar',
            'Rines y Llantas',
            'Rockolas',
            'Ropa Deportiva',
            'Rotulos',
            'Rparacion de Computadoras',
            'Salon de Belleza',
            'Salon de Fiestas',
            'Salon para Uñas',
            'Salud',
            'Sanatorio',
            'Sándwiches',
            'Sastre',
            'Seguridad',
            'Sex Shop',
            'Sincronizadas',
            'Sonido',
            'Sopes',
            'Spa',
            'Spinning',
            'Sushi',
            'Suspensiones',
            'Tacos',
            'Tacos al vapor',
            'Tacos de canasta',
            'Tacos de canasta',
            'Tacos de Guisado',
            'Tacos dorados',
            'Taller de bicicletas',
            'Taller de electronica',
            'Tamales y Atole',
            'Tapiceria e Interiores',
            'Tapicero',
            'Taquizas',
            'Tarjetas de Presentacion',
            'Tcos de Mariscos',
            'Telas',
            'Teneria',
            'Terapia Familiar',
            'Terapia Respiratoria',
            'Tienda de Abarrotes',
            'Tienda de Tornillos',
            'Tintoreria',
            'Tlayudas',
            'Tocineria',
            'Tortas',
            'Tortas Ahogadas',
            'Tortilleria',
            'Tostadas',
            'Transmisiones Automaticas',
            'Transporte de Personal',
            'Transporte Escolar',
            'Transporte Turistico',
            'Trio Musical',
            'Uniformes Escolares',
            'Vegetariana',
            'Venta de Bicicletas',
            'Venta de Herramientas',
            'Venta de Hielo',
            'Venta de Mascotas',
            'Venta de Muebles',
            'Venta de Ropa',
            'Venta de Trajes',
            'Venta de Uniformes',
            'Ventas de llantas',
            'Vestidos Boda, Quinceaños',
            'Veterinario',
            'Vidreria',
            'Vidrios y Canceles',
            'Vidrios y Espejos',
            'Vinos y Licores',
            'Vivero',
            'Vulcanizadora',
            'Yunque',
            'Zapateria'
        );

        $dataSet = [];
        foreach ($array as $key => $cat) {
            $dataSet[] = [
                'name'  => $cat,
                'icon' => 'ctg_ser_'.$key
            ];
        }

        DB::table('category_services')->insert($dataSet);
    }
}
