<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('photo')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->integer('employee_id')->unsigned()->nullable();
            $table->foreign('employee_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')->on('categories');
            $table->string('title');
            $table->text('description');
            $table->text('requirements')->nullable();
            $table->string('payment');
            $table->boolean('isSenior')->nullable();
            $table->integer('age')->unsigned();
            $table->integer('views')->unsigned()->nullable();
            $table->string('slug')->nullable();
            $table->time('time')->nullable();
            $table->enum('status',['Pagado','Pendiente','Pausado','Cancelado','Terminado']);
            $table->timestamps();
        });

        Schema::create('my_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')
                    ->references('id')->on('jobs')
                    ->onDelete('cascade');
            $table->integer('qualification_id')->unsigned();
            $table->foreign('qualification_id')
                    ->references('id')->on('qualifications');
            $table->enum('status',['Completado','Pendiente','Pausado','Cancelado']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
