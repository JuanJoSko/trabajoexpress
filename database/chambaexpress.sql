-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-03-2019 a las 21:19:28
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `chambaexpress`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Abogado', NULL, NULL, NULL),
(2, 'Administrador', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curricula`
--

CREATE TABLE `curricula` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Sin titulo',
  `description` text COLLATE utf8mb4_unicode_ci,
  `genere` enum('Hombre','Mujer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `curricula`
--

INSERT INTO `curricula` (`id`, `user_id`, `title`, `description`, `genere`, `url`, `created_at`, `updated_at`) VALUES
(4, 1, 'Programador', 'Sin Descripción', 'Hombre', NULL, '2019-02-21 03:33:14', '2019-02-23 03:34:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `resume` text COLLATE utf8mb4_unicode_ci,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` time DEFAULT NULL,
  `status` enum('Pagado','Pendiente','Pausado','Cancelado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `jobs`
--

INSERT INTO `jobs` (`id`, `photo`, `user_id`, `category_id`, `title`, `description`, `requirements`, `resume`, `payment`, `time`, `status`, `slug`, `age`, `views`, `created_at`, `updated_at`) VALUES
(1, 'plomero.jpg', 1, 1, 'Plomero', 'Reparar fuga tarja de cocina con una llave rota', 'llevar la herramienta', NULL, '250', NULL, 'Pagado', 'plomero-1', NULL, 36, '2019-03-03 03:47:30', '2019-03-13 02:00:00'),
(2, 'mecanico.jpg', 1, 2, 'Mecanico', 'Se solicita persona para cambiar el clutch de vocho', '', NULL, '200', '02:03:00', 'Pagado', 'mecanico-2', 23, 5, '2019-03-03 03:52:48', '2019-03-12 23:47:10'),
(3, 'tecnico.jpg', 1, 2, 'Técnico en PC', 'Nececito reparar el Display de mi laptop ASUS x200e', '', NULL, '50', '12:03:00', 'Pagado', 'tecnico-laptops-asus-3', NULL, 1, '2019-03-07 01:25:09', '2019-03-12 23:47:16'),
(4, 'perros.jpg', 1, 2, 'Paseaperros', 'Se solicita pasaedor de perros por la mañana', 'buena condición', NULL, '250', '02:00:00', 'Pagado', 'paseaperros-4', 22, 12, '2019-03-07 01:25:33', '2019-03-12 23:49:08'),
(5, 'taqueria.jpeg', 1, 2, 'Mesero', 'Solicito chavo para mesero en taquería pago más propinas', '', NULL, '300', '03:00:00', 'Pagado', 'mesero-5', 20, 0, '2019-03-07 01:40:18', '2019-03-07 01:40:18'),
(6, 'jardinero.jpg', 1, 2, 'Jardinero', 'se solicita jardinero', '', NULL, '200', '12:03:00', 'Pagado', 'jardinero-6', NULL, 0, '2019-03-07 01:40:52', '2019-03-07 01:40:52'),
(7, 'volantes.jpg', 1, 2, 'volantero', 'se solicita repartidor de volantes por candiles', '', NULL, '400', '05:00:00', 'Pagado', 'volantero-7', 16, 0, '2019-03-07 01:41:53', '2019-03-07 01:41:53'),
(8, 'lava.jpg', 1, 2, 'Lava Carros', 'solicito chavo para lavar carros', '', NULL, '400', '08:00:00', 'Pagado', 'lava-carros-8', 18, 1, '2019-03-07 01:42:26', '2019-03-12 00:42:06'),
(9, 'niñera.jpg', 1, 2, 'NIñera', 'Necesito chica para cuidar bebé  de  1 año', 'Experiencia cuidando niños', NULL, '450', '08:00:00', 'Pagado', 'niñera-9', 22, 0, '2019-03-07 01:45:04', '2019-03-07 01:45:04'),
(10, 'pizzeria.jpeg', 1, 2, 'Repartidor Pizzas', 'Solicito repartidor para pizzas', 'licencia de conducir\r\nmotocicleta propia', NULL, '480', '08:03:00', 'Pagado', 'Repartidor-Pizzas-10', NULL, 1, '2019-03-12 00:28:02', '2019-03-12 02:11:24'),
(11, 'vipe.jpeg', 1, 1, 'Encargado de tienda', 'Solicito encargado de tienda por dos días', 'experiencia en ventas y/o administración de negocios', NULL, '600', '16:00:00', 'Pagado', 'Encargado-de-tienda-11', NULL, 4, '2019-03-12 00:31:49', '2019-03-12 00:34:03'),
(12, 'cargador.jpg', 1, 2, 'Cargador', 'Solicito cargador para mercado de abastos, incluye comidas', NULL, NULL, '500', '06:03:00', 'Pagado', 'Cargador-12', NULL, 7, '2019-03-12 23:42:10', '2019-03-13 00:38:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_16_201847_create_curricula_table', 1),
(4, '2019_02_16_202148_create_categories_table', 1),
(5, '2019_02_16_202339_create_qualifications_table', 1),
(6, '2019_02_16_202546_create_jobs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `my_jobs`
--

CREATE TABLE `my_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED NOT NULL,
  `qualification_id` int(10) UNSIGNED NOT NULL,
  `status` enum('Completado','Pendiente','Pausado','Cancelado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `qualifications`
--

CREATE TABLE `qualifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ponits` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Employee','Employer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `photo`, `name`, `lastName`, `email`, `email_verified_at`, `password`, `birthday`, `phone`, `address`, `city`, `state`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'avatar.png', 'Juan José', 'Ledesma Cárdenas', 'juanjosko@gmail.com', NULL, '$2y$10$1KiNakZkWMuAXkLBleTBSeGyNIVp6n0DZsBVCRYPVph6dn8ryqeXa', NULL, '', NULL, 'Querétaro', 'Querétaro', 'm7K92A1PIWgTCrtZJD5xedekpErtn4mFq2RADuJC7cPdThlyzSOTJFJyDkmB', '2019-02-19 00:50:29', '2019-02-19 00:50:29');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `curricula`
--
ALTER TABLE `curricula`
  ADD PRIMARY KEY (`id`),
  ADD KEY `curricula_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_user_id_foreign` (`user_id`),
  ADD KEY `jobs_category_id_foreign` (`category_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `my_jobs`
--
ALTER TABLE `my_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `my_jobs_user_id_foreign` (`user_id`),
  ADD KEY `my_jobs_job_id_foreign` (`job_id`),
  ADD KEY `my_jobs_qualification_id_foreign` (`qualification_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qualifications_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `curricula`
--
ALTER TABLE `curricula`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `my_jobs`
--
ALTER TABLE `my_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `curricula`
--
ALTER TABLE `curricula`
  ADD CONSTRAINT `curricula_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `my_jobs`
--
ALTER TABLE `my_jobs`
  ADD CONSTRAINT `my_jobs_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `my_jobs_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`),
  ADD CONSTRAINT `my_jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `qualifications`
--
ALTER TABLE `qualifications`
  ADD CONSTRAINT `qualifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
