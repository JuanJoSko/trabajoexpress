<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    //

    protected $fillable = [
        'isWork',
        'age', 
        'description',
        'genere',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
