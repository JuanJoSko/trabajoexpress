<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'job_id',
        'user_id',
        'title', 
        'msg',
        'type',
        'slug'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
