<?php

namespace TrabajoExpress\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJob extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public $validator = null;
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'photo' => 'required',
            'title' => 'required|max:80',
            'description' => 'required',
            'payment' => 'required',
            'address' => 'required',
            'category_id' => [
                'required',
                'max:255',
                function ($attribute, $value, $fail) {
                    if ($value === 'Categoria') {
                        $fail('Debes seleccionar una categoría.');
                    }
                },
            ],
        ];
    }

    public function messages()
    {
        return [
            // 'photo.required' => 'El campo foto del Trabajo es obligatorio.',
            'title.required' => 'El campo Nombre del Trabajo es obligatorio.',
            'description.required' => 'El campo Descripción es obligatorio.',
            'payment.required' => 'El campo Pago es obligatorio.',
            'address.required' => 'EL campo Dirección es obligatorio',
            'category_id.required' => 'Debes seleccionar una categoría.'
        ];
    }
}
