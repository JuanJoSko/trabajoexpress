<?php

namespace TrabajoExpress\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public $validator = null;
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:80',
            'description' => 'required',
            'category_id' => [
                'required',
                'max:255',
                function ($attribute, $value, $fail) {
                    if ($value === 'Categoria') {
                        $fail('Debes seleccionar una categoría.');
                    }
                },
            ],
            'address' => 'required',
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => 'required',


        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo "Nombre del Trabajo" es obligatorio.',
            'category_id.required' => 'Debes seleccionar el Giro de tu negocio.',
            'phone.required' => 'El campo "Teléfono" es obligatorio.',
            'email.required' => 'El campo "Correo Electrónico" es obligatorio.',
            'email.email' => 'El campo "Correo Electrónico" debe ser una dirección de correo válida.',
            'address.required' => 'El campo "Dirección" es obligatorio.',
            'description.required' => 'El campo "Descripción" es obligatorio.',
        ];
    }
}
