<?php

namespace TrabajoExpress\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TrabajoExpress\Http\Controllers\Controller;

use TrabajoExpress\User;
use TrabajoExpress\Job;
use TrabajoExpress\Service;


class AdminController extends Controller
{
    public function index(){
        $month = [
            'users' => User::whereMonth('created_at', date('n'))->count(),
            'jobs' =>  Job::whereMonth('created_at', date('n'))->count(),
            'services' =>  Service::whereMonth('created_at', date('n'))->count()
        ];
        $total = [
            'users' => User::all()->count(),
            'jobs' =>  Job::all()->count(),
            'services' =>  Service::all()->count()
        ];     

        return view('admin.index', compact('month','total'));
    }
}
