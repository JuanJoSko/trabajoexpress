<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use TrabajoExpress\Job;
use TrabajoExpress\Service;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

use Auth;
use Redirect;
use Session;
use URL;

class PaypalController extends Controller
{
    public function __construct()
    {
    /** PayPal api context **/
            $paypal_conf = \Config::get('paypal');
            $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
            );
            $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request)
    {
        $source = $request->source;
        if($request->type == 'service'){
            $total = 499;
            $job = Service::find($request->job_id);
            $backUrl = 'myServices';
        }else{
            $job = Job::find($request->job_id);
            $total = ($job->payment * 0.1) - 1;
            if($total < 29)
                $total = 29;
            $backUrl ='myJobs';
            
        }
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
     
        $item_1 = new Item();
        $item_1->setName('Servicio TrabajoExpress.com.mx') /** item name **/
                    ->setCurrency('MXN')
                    ->setQuantity(1)
                    ->setPrice($total); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('MXN')
                ->setTotal($total);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
                    ->setDescription('Comisión por pago de Servicios TrabajoExpress.com.mx');
        $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(URL::route('paypal.status')) /** Specify return URL **/
                    ->setCancelUrl(URL::route('paypal.status'));
        $payment = new Payment();
                $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));
                /** dd($payment->create($this->_api_context));exit; **/
                try {
        $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
        if (\Config::get('app.debug')) {
        \Session::put('error', 'El tiempo de conexión expiro');
                        return Redirect::route($backUrl);
        } else {
        \Session::put('error', 'Ocurrió algún error, disculpe las molestias por favor intente de nuevo');
                        return Redirect::route($backUrl);
        }
        }
        foreach ($payment->getLinks() as $link) {
        if ($link->getRel() == 'approval_url') {
        $redirect_url = $link->getHref();
                        break;
        }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('id', $job->id);
        Session::put('type', $request->type);
        Session::put('source',$source);
        if (isset($redirect_url)) {
        /** redirect to paypal **/
                    return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Ocurrió un error desconocido, por favor intente de nuevo o más tarde');
                return Redirect::route($backUrl)->with('error', 'No se pudo completar el pago, intente de nuevo.');
    }
    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        $id = Session::get('id');
        $type = Session::get('type');
        $source = Session::get('source');

        if($type == 'service'){
            $job = Service::find($id);
            $backUrl ='myServices';

        }else{
            $job = Job::find($id);
            $backUrl ='myJobs';
        }

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
		Session::forget('id');
		Session::forget('type');
		Session::forget('source');
        
		if (empty(Input::get('PayerID')) ||
        empty(Input::get('token'))) {
            if($source == 'movil')
                return redirect('/movil/payment/status/Error')->with('error', 'No se pudo completar el pago, intente de nuevo.');
            else
                return Redirect::route($backUrl)->with('error', 'No se pudo completar el pago, intente de nuevo.');;
        }
        $payment = Payment::get($payment_id, $this->_api_context);
		$execution = new PaymentExecution();
		$execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
		$result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            $job->status = 'Pagado';
            $job->save();
            if($source == 'movil')
                return redirect('/movil/payment/status/Pagado')->with('success', 'Pago realizado, exitosamente!');
            else
                return Redirect::route($backUrl)->with('success', 'Pago realizado, exitosamente!');
        }else
            if($source == 'movil')
                return redirect('/movil/payment/status/Error')->with('error', 'No se pudo completar el pago, intente de nuevo.');
            else
                return Redirect::route($backUrl)->with('error', 'Pago realizado, exitosamente!');
    }
}
