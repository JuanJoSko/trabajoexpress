<?php

namespace TrabajoExpress\Http\Controllers\App;
use TrabajoExpress\Http\Controllers\Controller;
use Illuminate\Http\Request;

use TrabajoExpress\Notification;
use TrabajoExpress\User;


class AppNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $notifications = Notification::where('user_id',$request->user_id)->
                        with('job','job.category')->
                        orderBy('view', 'ASC')->
                        orderBy('created_at', 'DES')->
                        get();
        return response($notifications);
    }
    public function setViews(Request $request)
    {
        $notifications = Notification::where('user_id',$request->user_id)->
                        where('view',false)->get();
        foreach ($notifications as $key => $notifi) {
            $notifi->view = 1;
            $notifi->save();
        }               
        return response('update!');
    }

    public function setViewID(Request $request)
    {
        $notification = Notification::find($request->notification_id);
        $notification->view = 1;
        $notification->save();
        return response('update!');
    }
   
    public function countNoti(Request $request)
    {
        $user = User::find($request->user_id);
        $notifications = $user->newNotifys->count();
        return response($notifications);
    }

    /**
     * Display the specified resource.
     *
     * @param  \TrabajoExpress\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \TrabajoExpress\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \TrabajoExpress\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
