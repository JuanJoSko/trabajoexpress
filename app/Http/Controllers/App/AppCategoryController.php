<?php

namespace TrabajoExpress\Http\Controllers\App;

use Illuminate\Http\Request;
use TrabajoExpress\Category;
use TrabajoExpress\CategoryService;
use TrabajoExpress\Http\Controllers\Controller;


class AppCategoryController extends Controller
{
    public function getAll(){
        $categories = Category::all();
        return response()->json($categories);
    }
    public function getAllService(){
        $categories = CategoryService::all();
        return response()->json($categories);
    }
}
