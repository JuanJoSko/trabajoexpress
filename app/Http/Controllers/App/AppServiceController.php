<?php

namespace TrabajoExpress\Http\Controllers\App;

use Illuminate\Http\Request;
use TrabajoExpress\Service;
use TrabajoExpress\User;
use TrabajoExpress\Http\Controllers\Controller;
use TrabajoExpress\Category;
use Image;
use TrabajoExpress\Http\Requests\ServiceRequest;

class AppServiceController extends Controller
{
     public function index(Request $request){
         
        if(isset($request->search))
            $jobs = Service::search($request->search)
                            ->where('isDelete',0)
                            ->where('isApproved',1)
                            ->get();
        else
            $jobs = Service::where('status','Aprobado')
                            ->orderBy('created_at', 'DES')
                            ->get();
        return response()->json($jobs->load('user', 'category'));

    }
    public function getService(Request $request){
        $service = Service::with('user')->with('category')->get()->find($request->service_id);
        $totalServices = Service::where('user_id', $service->user->id)->count();
        return response()->json([
            'service' => $service,
            'totalServices' => $totalServices,
        ]);
    }

    public function getServiceById(Request $request){
        $user = User::find($request->user_id);
        $services = Service::with('user')
                            ->with('category')
                            ->where('user_id',$request->user_id)
                            ->where('status','!=' ,'Eliminado')
                            ->orderBy('created_at', 'DES')
                            ->get();

        return response()->json([
            'services' => $services,
            'type' => $user->type,
            'publisher' => $user->type == 'Empresa' ? $user->company->name : $user->name

        ]);
    }
    public function getCategoryService(Request $request){
        $services =  Service::with('user','category')->where('category_id',$request->category_id)->get();
        return response()->json($services);
    }


    public function deleteService(Request $request){
        $service = Service::find($request->service_id);
        $service->status = 'Eliminado';
        $service->save();

        return response()->json(
            [
                'status' => 'true',
                'title' => 'Negocio Eliminado',
                'msg' => 'El Negocio '.$service->title.' se ha eliminado exitosamente',
            ]
        );

    }
    public function store(ServiceRequest $request){
        if (isset($request->validator) && $request->validator->fails()) {
            // $error = '';
            // if (isset($request->validator) && $request->validator->fails()) {
            //     foreach ($request->validator->errors()->all() as $value) {
            //         $error .= $value;
            //     }
            // }
            return response()->json([
                'status' => false,
                'title' => '¡Ups ha ocurrido un error!',
                'msg' => $request->validator->errors()->all(),
                'service_id' => 0,
            ]);
        }else{
            $newService = new Service($request->except('photo'));
            $newService->user_id = $request->user_id;
            $newService->save();

            $newSlug =  str_replace(" ", "-", "$request->title");
            $newService->slug =  $newSlug.'-'.$newService->id;
            
            $nameFile = '';
            if($request->hasFile('photo')){
                $file = $request->file('photo');
                $nameFile = time().$file->getClientOriginalName();
                $image = Image::make($file)->resize(120, 120)->
                save(public_path().'/img/services/'.$nameFile);
            }
            $newService->photo = $nameFile;
            $newService->save();

            return response()->json([
                'status' => true,
                'title' => '¡Negocio guardado con exito!',
                'msg' => ['Ya sólo queda un paso más para publicarlo'],
                'service_id' => $newService->id,
            ]);
        }
        

    }

    public function update(ServiceRequest $request){
        $newService = Service::find($request->service_id);

        try{
            if (isset($request->validator) && $request->validator->fails()) {
                return response()->json([
                    'status' => false,
                    'title' => '¡Ups ha ocurrido un error!',
                    'msg' => $request->validator->errors()->all(),
                ]);
            }else{
                $newService->fill($request->except('photo'));
                if($request->hasFile('photo')){
                    $nameFile = '';
                    $file = $request->file('photo');
                    $nameFile = time().$file->getClientOriginalName();
                    $image = Image::make($file)->resize(256, 256)->
                    save(public_path().'/img/services/'.$nameFile);
                    $newService->photo = $nameFile;
                }
                $newService->save();
            

                return response()->json([
                    'status' => true,
                    'title' => '¡Negocio actualizado con exito!',
                    'msg' => ['Al editar tu negocio pasará de nuevo a revisión.'],
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'title' => '¡Uups...!',
                'msg' => $e,
            ]);
        }
    }

}
