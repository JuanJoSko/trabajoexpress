<?php

namespace TrabajoExpress\Http\Controllers\App;

use Illuminate\Http\Request;
use TrabajoExpress\Job;
use TrabajoExpress\Company;
use TrabajoExpress\Curriculum;
use TrabajoExpress\Postulation;
use TrabajoExpress\Http\Controllers\Controller;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use TrabajoExpress\User;
use Validator;
use Image;

class AppUserController extends Controller
{
    private $expoChannel;

    public function __construct(ExpoChannel $expoChannel){
        $this->expoChannel = $expoChannel;

    }

    public function completePerfil(Request $request){
        $user = User::find($request->user_id);
        if($user->type != null){
            return response()->json([
                'status' => true,
                'title' => '¡Ya has completado tu perfil anteriormente!',
                'msg' => ['Ya puedes ofrecer o encontrar trabajos'],
                'userID' => $user->id
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'ocupation' => 'required|string|max:255',
                'phone' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'state' => 'required|string|max:255',
                'city' => 'required|string|max:255',
            ]);
            
            if($validator->fails()){
                return response()->json([
                    'status' => false,
                    'title' => '¡Ups completa todos los datos!',
                    'msg' => $validator->errors()->all(),
                    'userID' => 0
                ]);
            }else{
            
                $user->type = $request->ocupation;
                $user->address = $request->address;
                $user->city = $request->city;
                $user->state = $request->state;
                $user->phone = $request->phone;
                $user->whats = $request->whats;
                $user->fb = $request->fb;
                $user->insta = $request->insta;
                
                if($request->hasFile('photo')){
                    $nameFile = '';
                    $file = $request->file('photo');
                    $nameFile = time().$file->getClientOriginalName();
                    $image = Image::make($file)->resize(120, 120)->
                        save(public_path().'/img/users/'.$nameFile);
                    $user->photo = 	$nameFile;
                }
               

                $user->save();


                if ($request->ocupation == 'Empresa/Negocio') {
                    $newCompany = new Company;
                    $newCompany->name = $request->company;
                    $newCompany->turn = $request->turn;
                    $newCompany->description = $request->description;
                    $user->company()->save($newCompany);
                }else{
                    $newCV = new Curriculum;
                    $request->isWork ? $newCV->isWork = 1 : $newCV->isWork = 0;
                    $newCV->age = $request->age;
                    $newCV->description = $request->description;
                    $newCV->genere = $request->genere;
                    $user->curriculum()->save($newCV);
                }
                return response()->json([
                    'status' => true,
                    'title' => '¡Peril Completado!',
                    'msg' => ['Listo! Ahora ya puedes ofrecer o encontrar trabajos'],
                    'userID' => $user->id
                ]);

            }
        }
    }

    public function setToken(Request $request){
        $user = User::find($request->user_id);
        $interest = $this->expoChannel->interestName($user);
        $user->token = $interest;
        $user->save();
        $this->expoChannel->expo->subscribe($interest, $request->token);

        return response()->json([
            'status' => 'true',
            'token' => $request->token
        ]);
    }

    public function checkUserID(Request $request){
        $user = User::find($request->user_id);
        return response()->json([
            'status' => $user == null ? false : true
        ]);
    }
    public function getUser(Request $request){
        $user = User::find($request->user_id);
        $completes = Postulation::where('user_id', $request->user_id)->
                                where('status','Completado')->count();
        $accepts = Job::where('user_id', $request->user_id)->
        where('employee_id','!=',null)->count();

        $quality = 0;
        foreach ($user->qualifications as $key => $value) {
            $quality +=$value->points;
        }
                                
        return response()->json([
            'status' => true,
            'user' => $user,
            'info'=>  $user->type == 'Empresa/Negocio' ? $user->company : $user->curriculum,
            'completes' => $completes,
            'postulations' => count($user->postulations),
            'totalJobs' => count($user->jobs),
            'qualifications' => count($user->qualifications),
            'quality' => $quality,
            'accepts' => $accepts
            ]);

    }

    public function update(Request $request){
        $user = User::find($request->user_id);

        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
			'name' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'string|email|max:255|unique:users|nullable',
            'password' => 'string|confirmed|nullable',
            'phone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'type' => 'required|string|max:255'
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'title' => ['¡Ups! Completa todos los datos'],
                'msg' => $validator->errors()->all(),
            ]);
        }else{
            $user->username = $request->username;
            $user->name = $request->name;
            $user->lastName = $request->lastName;
            if($request->email != null)
                $user->email = $request->email;
            if($request->password != null)
                $user->password = bcrypt($request->password);
            $user->type = $request->type;
            $user->address = $request->address;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->phone = $request->phone;
            $user->whats = $request->whats;
            $user->fb = $request->fb;
            $user->insta = $request->insta;
            if($request->hasFile('photo')){
                $nameFile = '';
                $file = $request->file('photo');
                $nameFile = time().$file->getClientOriginalName();
                $image = Image::make($file)->resize(120, 120)->
                    save(public_path().'/img/users/'.$nameFile);
                $user->photo = 	$nameFile;
            }

            $user->save();


            if ($request->type == 'Empresa/Negocio') {
                if($user->curriculum){
                    $newCompany = new Company;
                    $newCompany->name = $request->company;
                    $newCompany->turn = $request->turn;
                    $newCompany->description = $request->description;
                    $user->company()->save($newCompany);
                    $user->curriculum->delete();
                }else{
                    $user->company->name = $request->company;
                    $user->company->turn = $request->turn;
                    $user->company->description = $request->description;
                    $user->company->save();
                }
               
            }else{
                if($user->company){
                    $newCV = new Curriculum;
                    $newCV->isWork = (int)$request->isWork;
                    $newCV->age = $request->age;
                    $newCV->description = $request->description;
                    $newCV->genere = $request->genere;
                    $user->curriculum()->save($newCV);
                    $user->company->delete();

                }else{
                    $user->curriculum->isWork = (int)$request->isWork;
                    $user->curriculum->age = $request->age;
                    $user->curriculum->description = $request->description;
                    $user->curriculum->genere = $request->genere;
                    $user->curriculum->save();

                }

            }
            return response()->json([
                'status' => true,
                'title' => '¡Datos guardados!',
                'msg' => ['Tu perfil ha sido actualizado.'],
            ]);
        }
    }

   
}
