<?php

namespace TrabajoExpress\Http\Controllers\App;

use Illuminate\Http\Request;
use TrabajoExpress\Postulation;
use TrabajoExpress\Http\Controllers\Controller;
use TrabajoExpress\Job;
use TrabajoExpress\User;
use TrabajoExpress\Notification;
use TrabajoExpress\Helpers\TemplateOne;
use OneSignal;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;


class AppPostulationController extends Controller
{
    private $expoChannel;
    public function __construct(ExpoChannel $expoChannel)
    {
        $this->expoChannel = $expoChannel;

    }
     public function index(Request $request){
        $user = User::find($request->user_id);
        $postulations = $user->postulations;

        

        return response()->json([
            'postulations' => $postulations->load('category'),
        ]);
    }
   
    public function store(Request $request){
      
        try{
            $exist = Postulation::where('user_id',$request->user_id)->
            where('job_id',$request->job_id)->count();
            $user = User::find($request->user_id);
            $userComplete = true;

            if(empty($user->type)){
                $userComplete = false;
            }
            if($userComplete){
                if($exist == 1){
                    return response()->json([
                        'status' => false,
                        'title' => '¡Ups!',
                        'msg' => 'Ya has envíado una postulación anteriormente',
                        'userComplete' =>$userComplete
                    ]);
                    return response()->json('');
                }else{
                    // $postul = new Postulation($request->all());
                    // $postul->save();
                    $job = Job::find($request->job_id);
                    $user = User::find($request->user_id);
                    $user->postulations()->attach($job->id);
                    $newNotify = Notification::create([
                        'user_id' => $job->user->id,
                        'title' => 'Un nuevo usuario se ha postulado', 
                        'msg'=> 'El usuario '.$user->username.' se ha postulado en: '.$job->title,
                        'type' => 'newPostulation',
                        'slug' => $job->slug,
                        'job_id' => $job->id
                    ]);
                
                    OneSignal::sendNotificationById(
                        TemplateOne::GetName("newPostulate"),
                        $job->user->id,
                        'El usuario '.$user->username.' se ha postulado en: '.$job->title,
                        "http://trabajoexpress.loc/notification/job/".$newNotify->id
                    );
                    $interest = $this->expoChannel->interestName($job->user);

                    $message = new ExpoMessage(
                        'Un nuevo usuario se ha postulado',
                        'El usuario '.$user->username.' se ha postulado en: '.$job->title,
                        '{"status": "ok", "jobID": "'.$job->id.'", "notifiID": "'.$newNotify->id.'"}'
                    );
                    $this->expoChannel->sendPush($interest, $message->toArray());
                }
            
                return response()->json([
                    'status' => true,
                    'title' => '¡Te has postulado!',
                    'msg' => 'Tu postulación  ha sido envíada exitosamente!',
                    'userComplete' => $userComplete
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'title' => '¡Ups!',
                    'msg' => 'Antes de realizar esta acción cuentanos algunas cosas sobre ti',
                    'userComplete' => $userComplete
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'title' => '¡Ups! Ha ocurrido un error',
                'msg' => ['Error inesperado por favor intenta de nuevo o más tarde.'],
            ]);
        }

    }

    public function getPendingPostulate(Request $request){
        $job = Job::find($request->job_id);
        return response()->json([
            'postulates' => $job->pendingPostulates,
            'job' => $job->title
            ]);

    }
    
    public function getAcceptPostulate(Request $request){

        $jobs = Job::where('user_id',$request->user_id)->
                    where('employee_id', '!=', null)->
                    with('employee')->
                    orderBy('created_at', 'DES')->
                    get();
        
        return response()->json([
            'postulates' => $jobs->load('category')
            ]);
    }

    public function confirmationPostulate(Request $request){
        $postulation = Postulation::find($request->postulation_id);
        $postulation->status = $request->confirmation;
        $postulation->save();

        if($request->confirmation == 'Aceptado'){
            $postulation->job->status = 'Trabajando';
            $postulation->job->employee_id = $postulation->user_id;
            $postulation->job->save();
            
            $newNotify = Notification::create([
                'user_id' => $postulation->user_id,
                'title' => '¡Felicidades! postulación aceptada', 
                'msg'=> 'Tu postulación ha sido aceptada en: '.$postulation->job->title,
                'type' => 'acceptPostulation',
                'slug' => $postulation->job->slug,
                'job_id' => $postulation->job_id

            ]);
            OneSignal::sendNotificationById(
                TemplateOne::GetName("acceptPostulate"),
                $postulation->user_id,
                'Tu postulación ha sido aceptada en: '.$postulation->job->title,
                "http://trabajoexpress.loc/notification/job/".$newNotify->id
            );
            $interest = $this->expoChannel->interestName($postulation->user);

            $message = new ExpoMessage(
                '¡Felicidades! postulación aceptada',
                'Tu postulación ha sido aceptada en: '.$postulation->job->title,
                '{"status": "ok", "jobID": "'.$postulation->job_id.'", "notifiID": "'.$newNotify->id.'"}'
            );
            $this->expoChannel->sendPush($interest, $message->toArray());
        }else{
            $newNotify = Notification::create([
                'user_id' => $postulation->user_id,
                'title' => 'Lo sentimos, postulación cancelada', 
                'msg'=> 'Tu postulación ha sido cancelada en: '.$postulation->job->title,
                'type' => 'cancelPostulation',
                'slug' => $postulation->job->slug,
                'job_id' => $postulation->job_id

            ]);
            OneSignal::sendNotificationById(
                TemplateOne::GetName("cancelPostulate"),
                $postulation->user_id,
                'Tu postulación ha sido cancelada en: '.$postulation->job->title,
                "http://trabajoexpress.loc/notification/job/".$newNotify->id
            );
            $interest = $this->expoChannel->interestName($postulation->user);
            
            $message = new ExpoMessage(
                'Lo sentimos, postulación cancelada',
                'Tu postulación ha sido cancelada en: '.$postulation->job->title,
                '{"status": "ok", "jobID": "'.$postulation->job_id.'", "notifiID": "'.$newNotify->id.'"}'
            );
            $this->expoChannel->sendPush($interest, $message->toArray());
        }
        return response()->json([
            'status' => 'save',
            'user_id' =>  $postulation->user_id,
            'user' => $postulation->user,
            'job_id' => $postulation->job_id,
            'job_slug' => $postulation->job->slug
        ]);
    }
}
