<?php

namespace TrabajoExpress\Http\Controllers\App\Auth;

use Illuminate\Http\Request;
use TrabajoExpress\User;
use TrabajoExpress\Notification;
use TrabajoExpress\Http\Controllers\Controller;
use Validator;
use Auth;
use Image;

class AppRegisterController extends Controller
{
    //
    public function register(Request $request){
      	$validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
			'name' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
        
      if($validator->fails()){
		return response()->json([
			'status' => false,
			'title' => '¡Ups! Verifica los datos',
			'msg' => $validator->errors()->all(),
			'userID' => 0
		]);
      }else{
        $user = new User;
        $user->username = $request->username;
		$user->name = $request->name;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
		$user->password = bcrypt($request->password);

		$user->save();

		$newNotify = Notification::create([
			'user_id' => $user->id,
			'title' => 'Bienvenid@ '.$user->username, 
			'msg'=> 'Gracias por registrarte ahora sólo completa tu perfil.',
			'type' => 'alert',
			'slug' => '/mi-perfil',
		]);

		return response()->json([
			'status' => true,
			'title' => '¡Gracias por registrarte!',
			'msg' => ['Listo! Ahora ya puedes ofrecer o encontrar trabajos'],
			'userID' => $user->id
		]);
      }
    }
}


