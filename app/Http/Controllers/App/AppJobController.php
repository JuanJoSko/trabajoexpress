<?php

namespace TrabajoExpress\Http\Controllers\App;

use Illuminate\Http\Request;
use TrabajoExpress\Job;
use TrabajoExpress\User;
use TrabajoExpress\Postulation;
use TrabajoExpress\Http\Controllers\Controller;
use TrabajoExpress\Category;
use Carbon\Carbon;
use Image;
use TrabajoExpress\Http\Requests\StoreJob;

class AppJobController extends Controller
{
    public function index(Request $request){
        if(isset($request->search)){
            $jobs = Job::search($request->search)
            ->where('isDelete',0)
            ->where('isApproved',1)
            ->get();

            return response()->json([
                'status' => true,
                'jobs' => $jobs->load('user', 'category')
            ]);
        }
           
        else{
            $jobs = Job::where('status','!=','Eliminado')
            ->where('status','Aprobado')
            ->orWhere('status', 'Trabajando')
            ->orderBy('created_at', 'DES')
            ->get();

            $jobs3era = Job::where('status','!=','Eliminado')
            ->where('status','Aprobado')
            ->orWhere('status', 'Trabajando')
            ->where('isSenior', '1')
            ->get()
            ->count();

            return response()->json([
                'status' => true,
                'jobs' => $jobs->load('user', 'category'),
                'jobs3eraSize' => $jobs3era
            ]);
        }
           

       

    }
    public function getJob(Request $request){
        $exist = Postulation::where('user_id',$request->user_id)->
                            where('job_id',$request->job_id)->count();
        $job = Job::with('user','employee','category')->get()->find($request->job_id);
        $totalJobs = Job::where('user_id', $job->user->id)->count();
        $isRating = false;
        if($job->acceptPostulate())
            if($request->user_id == $job->user_id)
                $isRating = $job->acceptPostulate()->pivot->starsEmployer() != 0;
            else
                $isRating = $job->acceptPostulate()->pivot->starsEmployee() != 0;
        

        return response()->json([
            'job' => $job,
            'totalJobs' => $totalJobs,
            'totalPostul' => count($job->pendingPostulates),
            'employee' => $job->employee,
            'acceptPostulate' =>$job->acceptPostulate(),
            'isPostulate' => (bool)$exist,
            'isRating' => $isRating
        ]);
    }

    public function getJobsById(Request $request){
        $user = User::find($request->user_id);
        $jobs = Job::with('user','category','pendingPostulates')
                    ->where('user_id',$request->user_id)
                    ->where('status','!=' ,'Eliminado')
                    ->orderBy('created_at', 'DES')
                    ->get();

        return response()->json([
            'job' => $jobs,
            'type' => $user->type,
            'publisher' => $user->type == 'Empresa' ? $user->company->name : $user->name
        ]);
    }
     public function getJobDetails(Request $request){
        $job = Job::with('category','user')->get()->find($request->job_id);
        $totalJobs = Job::where('user_id', $job->user->id)->count();
        return response()->json([
            'photo' => $job->photo,
            'title' => $job->title,
            'description' => $job->description,
            'payment' => $job->payment
        ]);
    }
    
    public function getCategoryJob(Request $request){
       $jobs =  Job::with('user','category')->where('category_id',$request->category_id)->get();
        return response()->json($jobs);
    }


    public function deleteJob(Request $request){
        $job = Job::find($request->job_id);
        $job->status = 'Eliminado';
        $job->isDelete = 0;
        $job->save();

        return response()->json('Trabajo eliminado exitosamente!');

    }
    public function store(StoreJOb $request){
        
       try {

        if (isset($request->validator) && $request->validator->fails()) {
            return response()->json([
                'status' => false,
                'title' => '¡Ups! Ha ocurrido un error',
                'msg' => $request->validator->errors()->all(),
                'job_id' => 0,
            ]);
        }else{
            $newJob = new Job($request->except('photo','isSenior','title'));
            $newJob->isSenior = $request->isSenior;
            $newJob->title = ucfirst($request->title);
            $newJob->user_id = $request->user_id;
            $newJob->save();
            $newSlug =  str_replace(" ", "-", "$request->title");
            $newJob->slug =  $newSlug.'-'.$newJob->id;
            $nameFile = '';
            if($request->hasFile('photo')){
                $file = $request->file('photo');
                $nameFile = time().$file->getClientOriginalName();
                $image = Image::make($file)->resize(256, 256)->
                    save(public_path().'/img/jobs/'.$nameFile);
            }
            $newJob->photo = $nameFile;
            $newJob->save();

            return response()->json([
                'status' => true,
                'title' => '¡Trabajo guardado con exito!',
                'msg' => ['Ya sólo queda un paso más para publicarlo'],
                'job_id' => $newJob->id,
            ]);
        }
       } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'title' => '¡Ups! Ha ocurrido un error',
                'msg' => ['Error inesperado por favor intenta de nuevo.'],
                'job_id' => '',
            ]);
        }
        

    }

    public function update(StoreJOb $request){
        $newJob = Job::find($request->job_id);
        if( count($newJob->pendingPostulates) == 0 && $newJob->employee_id == null){
            try {
    
            if (isset($request->validator) && $request->validator->fails()) {
                return response()->json([
                    'status' => false,
                    'title' => '¡Ups! Ha ocurrido un error!',
                    'msg' => $request->validator->errors()->all(),
                ]);
            }else{
                
                $newJob->isSenior = $request->isSenior;
                $newJob->fill($request->except('photo','isSenior','job_id'));
                
                if($request->payment == $newJob->payment){
                    $newJob->status = 'Pagado';
                }else{
                    $newJob->status = 'Pendiente';
                }
                
                if($request->hasFile('photo')){
                    $nameFile = '';
                    $file = $request->file('photo');
                    $nameFile = time().$file->getClientOriginalName();
                    $image = Image::make($file)->resize(256, 256)->
                        save(public_path().'/img/jobs/'.$nameFile);
                    $newJob->photo = $nameFile;
                }

                $newJob->save();
                
    
                return response()->json([
                    'status' => true,
                    'title' => '¡Trabajo actualizado con exito!',
                    'msg' => ['Al editar un trabajo pasará de nuevo a revisión.'],
                ]);
            }
            } catch (\Exception $e) {
                return response()->json([
                    'status' => false,
                    'title' => '¡Uups...!',
                    'msg' => $e,
                ]);
            }
        }else{
            return response()->json([
                'status' => false,
                'title' => '¡Uups...!',
                'msg' =>   $newJob->employee_id != null ? ['Ya tienes un aceptado en este trabajo, no puedes editarlo.'] :  ['Ya tienes postulados en este trabajo, no puedes editarlo.'],
            ]);
        }
 
     }

}
