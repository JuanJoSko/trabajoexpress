<?php

namespace TrabajoExpress\Http\Controllers\App;

use TrabajoExpress\Http\Controllers\Controller;
use Illuminate\Http\Request;

use TrabajoExpress\Qualification;
use TrabajoExpress\Postulation;
use TrabajoExpress\Job;


class AppQualificationController extends Controller
{
    public function index(Request $request){
        $qualifications = Qualification::where('user_id',$request->user_id)->with('author')->get();
        return response()->json([
            'qualifications' => $qualifications,
        ]);
    }

    public function store(Request $request){
        $postulation = Postulation::find($request->postulation_id);

        if($request->type == 'Employer')
            $isRating = $postulation->starsEmployer() == 0;
        if($request->type == 'Employee')
            $isRating = $postulation->starsEmployee() == 0;
        if($request->type == 'Service'){
            $qualifications =  Qualification::where('user_id', $request->user_id)
            ->where('author_id', $request->author_id)
            ->where('type', $request->type)->get()->count();
            $isRating =$qualifications == 0;
        }

        if($isRating){
            try{
                Qualification::create([
                    'user_id' => $request->user_id,
                    'author_id' => $request->author_id,
                    'postulation_id' => $request->postulation_id,
                    'points' =>  $request->points,
                    'type' => $request->type,
                    'comment' => $request->comment
                ]);
                return  response()->json([
                    'status' => true,
                    'title' => '!Listo!',
                    'msg' => 'Calificación enviada',

                ]);
            }catch(\Exception $e){
                return  response()->json([
                    'status' => false,
                    'title' => '!Uups!',
                    'msg' => 'Error inesperado, por favor intenta de nuevo o más tarde.',
                ]);
            }
        }else{
            return  response()->json([
                'status' => false,
                'title' => '!Uups!',
                'msg' => 'Ya has envíado una calificación anteriormente.',
            ]);
        }

       
    }
}
