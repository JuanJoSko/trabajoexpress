<?php

namespace TrabajoExpress\Http\Controllers\App;

use Illuminate\Http\Request;
use TrabajoExpress\User;
use TrabajoExpress\Http\Controllers\Controller;
use Validator;
use Auth;


class AppLoginController extends Controller
{

    public function Login(Request $request)
    {
		$validator = Validator::make($request->all(),[
			'email'=>'required|email',
			'password'=>'required'
		]);
		if(User::where('email',$request->email)->get()->count() == 0){
			return response()->json([
				'status'=>false,
				'errors' => ["Estas credenciales no coinciden con nuestros registros."],
				]);
		}else{
			if($validator->fails()){
				return response()->json(['status'=>false,'errors'=>$validator->errors()->all()]);
			}else{
				if(Auth::attempt($request->all())){
				return response()->json(['status'=>true,'errors'=>['Usuario logeado'],'user'=>Auth::user()]);
				}else{
				return response()->json([
					'status'=>false,
					'errors' => ["Correo o contraseña incorrectos."],
					'info' => "por favor verifique sus datos"
					]);
				}
			}
		}

    }
   
}
