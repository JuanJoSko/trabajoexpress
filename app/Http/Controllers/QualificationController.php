<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\Qualification;

class QualificationController extends Controller
{
    public function store(Request $request){
        try{
            Qualification::create([
                'user_id' => $request->user_id,
                'author_id' => $request->author_id,
                'postulation_id' => $request->postulation_id,
                'points' =>  $request->points,
                'type' => $request->type,
                'comment' => $request->comment
            ]);
            return  response()->json([
                'status' => 'save'
            ]);
        }catch(\Exception $e){
            return  response()->json([
                'status' => 'error'
            ]);
        }

       
    }
}
