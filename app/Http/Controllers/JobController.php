<?php

namespace TrabajoExpress\Http\Controllers;

use TrabajoExpress\Job;
use TrabajoExpress\Http\Requests\StoreJob;
use Auth;
use TrabajoExpress\Category;
use Carbon\Carbon;
use Image;
use Illuminate\Http\Request;
use TrabajoExpress\Postulation;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobs = Job::where('status','Aprobado')
                    ->orWhere('status', 'Trabajando')
                    ->orderBy('created_at', 'DES')
                    ->paginate(15);
        $titleTab = 'Express';

        if($request->ajax()){
            return response()->json(
                view('job.jobs_catalog',compact('jobs','titleTab'))->render()
            );
        }
        $categories = Category::all();
        return view('home', compact('jobs','categories','titleTab'));
        
    }

    public function search (Request $request){
        $jobs = Job::search($request->search)
        ->where('isDelete',0)
        ->where('isApproved',1)
        ->paginate(15);
        $categories = Category::all();
        $titleTab = 'Express';

        return view('home', compact('jobs','categories','titleTab'));
    }

    public function MyJobs(Request $request){

       
            switch ($request->filter) {
                case 'pay-jobs':
                    $jobs = Job::where('user_id', Auth::user()->id)->where('status','Pagado')->paginate(6);
                break;
                case 'accept-jobs':
                    $jobs = Job::where('user_id', Auth::user()->id)->where('status','Aprobado')->paginate(6);
                break;
                case 'pending-jobs':
                    $jobs = Job::where('user_id', Auth::user()->id)->where('status','Pendiente')->paginate(6);
                break;
                case 'working-jobs':
                    $jobs = Job::where('user_id', Auth::user()->id)->where('status','Trabajando')->paginate(6);
                break;
                case 'cancel-jobs':
                    $jobs = Job::where('user_id', Auth::user()->id)->where('status','Cancelado')->paginate(6);
                break;
                default:
                    $jobs = Job::where('user_id', Auth::user()->id)
                    ->where('status','!=','Eliminado')
                    ->orderBy('created_at', 'DES')
                    ->paginate(6);
                break;
            }
        

        if($request->ajax()){
            return response()->json(
                view('job.jobs_myCatalog',compact('jobs'))->render()
            );
        }



        return view('job.index', compact('jobs'));
    }


    public function pendingJobs(Request $request){
        $jobs = Job::where('user_id', Auth::user()->id)->where('status','Pendiente')->paginate(6);
        if($request->ajax()){
            return response()->json(
                view('job.jobs_myCatalog',compact('jobs'))->render()
            );
        }
        return view('job.index', compact('jobs'));
    }

    public function payJobs(Request $request){
        $jobs = Job::where('user_id', Auth::user()->id)->where('status','Pagado')->paginate(6);
        if($request->ajax()){
            return response()->json(
                view('job.jobs_myCatalog',compact('jobs'))->render()
            );
        }
        return view('job.index', compact('jobs'));
    }

    public function cancelJobs(Request $request){
        $jobs = Job::where('user_id', Auth::user()->id)->where('status','Cancelado')->paginate(6);
        if($request->ajax()){
            return response()->json(
                view('job.jobs_myCatalog',compact('jobs'))->render()
            );
        }
        return view('job.index', compact('jobs'));
    }

    public function old(Request $request)
    {

        $jobs = Job::where('isSenior',true)->paginate(15);
        $titleTab = '3era Edad';

        if($request->ajax()){
            return response()->json(
                view('job.jobs_catalog',compact('jobs','titleTab'))->render()
            );
        }
        $categories = Category::all();

        return view('home', compact('jobs','categories','titleTab'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            $categories = Category::all();
            return view('job.job_create', compact('categories'));
        }else{
            return redirect('/login')->with('Debes iniciar sesión');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJOb $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->withErrors($request->validator);
        }else{
            $newJob = new Job($request->all());
        
            Auth::user()->jobs()->save($newJob);
            $newSlug =  str_replace(" ", "-", "$request->title");
            $newJob->slug =  $newSlug.'-'.$newJob->id;
            if($request->hasFile('photo')){
                $nameFile = '';
                $file = $request->file('photo');
                $nameFile = time().$file->getClientOriginalName();
                $thumbnail = Image::make($file)->resize(120, 120)->
                        save(public_path().'/img/jobs/thumbnails/'.$nameFile);
                $image = Image::make($file)->resize(1024, 720)->
                    save(public_path().'/img/jobs/'.$nameFile);
                $newJob->photo = $nameFile;

            }
            $newJob->status = 'Pendiente';
            $newJob->save();

            return  redirect('/payment/job/'.$newJob->id)->with('status', '¡Información Guardada Exitosamente!');
        }

    }
    

    /**
     * Display the specified resource.
     *
     * @param  \TrabajoExpress\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        $job->views = $job->views + 1;
        $job->save(); 
        return view('job.job_detail', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \TrabajoExpress\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        $categories = Category::all();
        return view('job.edit', compact('job','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \TrabajoExpress\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        //
    }

    public function delete(Request $request)
    {
        $job = Job::find($request->job_id);
        $jobName = $job->title;
        $file_path = public_path().'/img/jobs/'.$job->photo;
        \File::delete($file_path);
        $job->delete();

        return response('El trabajo '.$jobName.' se ha eliminado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \TrabajoExpress\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        //
    }
}
