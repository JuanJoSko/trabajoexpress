<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        return view('home', compact('categories'));
    }
    public function map()
    {
        return view('maps.index');
    }


    public function terms()
    {
        return view('terms');
    }
    public function politics()
    {
        return view('politics');
    }
}
