<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\User;
use TrabajoExpress\Notification;
use TrabajoExpress\Job;
use TrabajoExpress\Postulation;
use Auth;
use TrabajoExpress\Helpers\TemplateOne;
use OneSignal;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class PostulationController extends Controller
{
    private $expoChannel;
    public function __construct(ExpoChannel $expoChannel)
    {
        $this->expoChannel = $expoChannel;
        $this->middleware('auth');
    }

    public function test(){
        
        $postulations = Job::where('user_id',Auth::user()->id)->where('employee_id', '!=', null)->with('employee')->get();
        
        dd($postulations);
    }

    public function index(){
        
        $postulations = Auth::user()->postulations;
        return view('postulations.index', compact('postulations'));
    }
    public function store(Job $job){
        if (empty($job->employee_id)){
            Auth::user()->postulations()->attach($job->id);
            $newNotify = Notification::create([
                'user_id' => $job->user->id,
                'title' => 'Un nuevo usuario se ha postulado', 
                'msg'=> 'El usuario '.Auth::user()->username.' se ha postulado en: '.$job->title,
                'type' => 'newPostulation',
                'slug' => $job->slug,
                'job_id' => $job->id
            ]);
        
            OneSignal::sendNotificationById(
                TemplateOne::GetName("newPostulate"),
                $job->user->id,
                'El usuario '.Auth::user()->username.' se ha postulado en: '.$job->title,
                "http://trabajoexpress.loc/notification/job/".$newNotify->id
            );
            $interest = $this->expoChannel->interestName($job->user);

            $message = new ExpoMessage(
                'Un nuevo usuario se ha postulado',
                'El usuario '.Auth::user()->username.' se ha postulado en: '.$job->title
            );
            $message->setChannelId('postulation-messages');
            $this->expoChannel->sendPush($interest, $message->toArray());

            return redirect()->route('postulations.index')->with('status','Postulación envíada en espera de aprobación');
        }else{
            return redirect()->route('postulations.index')->with('status','Ya hay un postulado seleccionado');
        }
    }

    public function confirmation(Request $request){
       
        $postulation = Postulation::find($request->postulation_id);
        $postulation->status = $request->confirmation;
        $postulation->save();

        if($request->confirmation == 'Aceptado'){
            $newNotify = Notification::create([
                'user_id' => $postulation->user_id,
                'title' => 'Felicidades! postulación aceptada', 
                'msg'=> 'Tu postulación ha sido aceptada en: '.$postulation->job->title,
                'type' => 'acceptPostulation',
                'slug' => $postulation->job->slug,
                'job_id' => $postulation->job_id
            ]);
            OneSignal::sendNotificationById(
                TemplateOne::GetName("acceptPostulate"),
                $postulation->user_id,
                'Tu postulación ha sido aceptada en: '.$postulation->job->title,
                "http://trabajoexpress.loc/notification/job/".$newNotify->id
            );
            $interest = $this->expoChannel->interestName($postulation->user);

            $message = new ExpoMessage(
                'Felicidades! postulación aceptada',
                'Tu postulación ha sido aceptada en: '.$postulation->job->title
            );
            $message->setChannelId('postulation-messages');
            $this->expoChannel->sendPush($interest, $message->toArray());
        }else{
            $newNotify = Notification::create([
                'user_id' => $postulation->user_id,
                'title' => 'Lo sentimos, postulación cancelada!', 
                'msg'=> 'Tu postulación ha sido cancelada en: '.$postulation->job->title,
                'type' => 'cancelPostulation',
                'slug' => $postulation->job->slug,
                'job_id' => $postulation->job_id
            ]);
            OneSignal::sendNotificationById(
                TemplateOne::GetName("cancelPostulate"),
                $postulation->user_id,
                'Tu postulación ha sido cancelada en: '.$postulation->job->title,
                "http://trabajoexpress.loc/notification/job/".$newNotify->id
            );
            $interest = $this->expoChannel->interestName($postulation->user);

            $message = new ExpoMessage(
                'Lo sentimos, postulación cancelada!',
                'Tu postulación ha sido cancelada en: '.$postulation->job->title
            );
            $message->setChannelId('postulation-messages');
            $this->expoChannel->sendPush($interest, $message->toArray());
        }
        return response()->json([
            'status' => 'save',
            'user_id' =>  $postulation->user_id
        ]);
    }
}
