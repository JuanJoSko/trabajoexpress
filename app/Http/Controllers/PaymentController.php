<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\Job;
use TrabajoExpress\Service;
use Conekta\Conekta;
use Auth;

class PaymentController extends Controller
{
    public function reference($value){

        return str_pad($value, 9, '0', STR_PAD_LEFT);

    }

    public function index($type, $id, $source = 'PC'){
        if($type == 'service'){
            $job = Service::find($id);
        }else{
            $job = Job::find($id);
        }
        if($source == 'movil'){
            $total = ($job->payment * 0.1) - 0.1;
            if($total < 29)
                $total = 29;
            return view('movil.payment', compact('job','type','source','total'));
        }
        else{
            $total = ($job->payment * 0.1) - 0.1;
            if($total < 29)
                $total = 29;
            return view('payment', compact('job','type','source', 'total'));
            
        }
    }

    public function statusPaymentMovil($status){

        return view('movil.status_payment', compact('status'));
    }

    public function process(Request $request){

        Conekta::setApiKey("key_cU3C3uxVr6y3cyKBpoZ5sQ");
        Conekta::setApiVersion("2.0.0");

        $customer = \Conekta\Customer::find(Auth::user()->customer_id);
        $paymentSource = $customer->payment_sources;
        

        try{
            $order = \Conekta\Order::create(array(
                'currency' => 'MXN',
                'customer_info' => array(
                'customer_id' => Auth::user()->customer_id
                ),
                "line_items" => array(
                    array(
                      "name" => "comision por servicio de trabajoexpress.com.mx",
                      "unit_price" => 200000,
                      "quantity" => 1
                    )
                  ),
                "metadata" => array("reference" => $this->reference($request->job_id), "more_info" => "trabajoexpress.com.mx"),
                'charges' => array(
                        array(
                        'payment_method' => array(
                            "type" => "card",
                            "payment_source_id" => $paymentSource[$request->idCard]->id
                        )
                    )  
                )
            ));
        } 
        catch (\Conekta\ProcessingError $error){
            return redirect('/payment/job/'.$job->id)->with('error', $error->getMessage());
            } 
        catch (\Conekta\ParameterValidationError $error){
            return redirect('/payment/job/'.$job->id)->with('error', $error->getMessage());
            }
        catch (\Conekta\Handler $error){
            return redirect('/payment/job/'.$job->id)->with('error', $error->getMessage());
        }

        return dd($request);
    }

    public function NewPayment(Request $request) {
        Conekta::setApiKey("key_cU3C3uxVr6y3cyKBpoZ5sQ");
        Conekta::setApiVersion("2.0.0");

        $source = $request->source;
        if($request->type == 'service'){
            $total = 499;
            $job = Service::find($request->job_id);
            $backUrl = 'myServices';
        }else{
            $job = Job::find($request->job_id);
            $total = ($job->payment * 0.1) - 1;
            if($total < 29)
                $total = 29;
            $backUrl ='myJobs';
            
        }
        
        if(Auth::user()->conekta_id == null){
            try {
                $customer = \Conekta\Customer::create(
                array(
                    "name" =>  Auth::user()->name,
                    "email" => Auth::user()->email,
                )//customer
                );
            } catch (\Conekta\ProccessingError $error){
                return $error->getMesage();
            } catch (\Conekta\ParameterValidationError $error){
                return $error->getMessage();
            } catch (\Conekta\Handler $error){
                return $error->getMessage();
            }
            Auth::user()->conekta_id = $customer->id;
            Auth::user()->save();
        }
        
        $customerPay = \Conekta\Customer::find(Auth::user()->conekta_id);
        $source = $customerPay->createPaymentSource(array(
        'token_id' => $request->conektaTokenId,
        'type'     => 'card'
        ));

        try{
            $order = \Conekta\Order::create(
              array(
                "line_items" => array(
                  array(
                    "name" => "comision por servicio de trabajoexpress.com.mx",
                    "unit_price" => $total.'00',
                    "quantity" => 1
                  )
                ),
                "currency" => "MXN",
                "customer_info" => array(
                  "customer_id" => $customerPay->id,
                ),
                "metadata" => array("reference" => $this->reference($job->id), "more_info" => "trabajoexpress.com.mx"),
                'charges' => array(
                    array(
                        'payment_method' => array(
                            "type" => "card",
                            "payment_source_id" => $source->id
                        )
                    )  
                 )
              )
            );
        } catch (\Conekta\ProcessingError $error){
        return redirect('/payment/job/'.$job->id)->with('errorConekta', $error->getMessage());
        } catch (\Conekta\ParameterValidationError $error){
        return redirect('/payment/job/'.$job->id)->with('errorConekta', $error->getMessage());
        } catch (\Conekta\Handler $error){
        return redirect('/payment/job/'.$job->id)->with('errorConekta', $error->getMessage());
        }

        $job->status = 'Pagado';
        $job->save();

       
        return  redirect()->route($backUrl)->with('status', 'Pago realizado, exitosamente!');
        
    }
}
