<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\Job;
use TrabajoExpress\Service;

use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class TestController extends Controller
{
    public function test(){

        $message = new ExpoMessage(
            'Un nuevo usuario se ha postulado',
            'El usuario test se ha postulado en: trabajo'
        );

        $message->setJsonData(array('someData' => 'goes here'));

        dd($message);
    }
}
