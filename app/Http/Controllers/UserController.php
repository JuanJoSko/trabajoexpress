<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\Curriculum;
use TrabajoExpress\Company;
use TrabajoExpress\User;
use Auth;
use Image;

class UserController extends Controller
{
    public function MyProfile(){
        return view('my_profile');
    }

    public function UpdateAbout(Request $request){

        Auth::user()->type = $request->type;
        Auth::user()->save();
        if($request->type != 'Empresa/Negocio'){
            if(Auth::user()->curriculum){
                Auth::user()->curriculum->fill($request->all());
                Auth::user()->curriculum->save();
            }else{
                $newCurri = new Curriculum($request->all());
                Auth::user()->curriculum()->save($newCurri);
            }
        }else{
            if(Auth::user()->company){
                Auth::user()->company->name = $request->company;
                Auth::user()->company->turn = $request->turn;
                Auth::user()->company->description = $request->description;
                Auth::user()->company->save();
            }else{
                $newCompany = new Company();
                $newCompany->name = $request->company;
                $newCompany->turn = $request->turn;
                $newCompany->description = $request->description;
                Auth::user()->curriculum()->save($newCompany);
            }
        }

        return  redirect('/profile/'.Auth::user()->id)->with('status', '¡Información Guardada Exitosamente!');
    }

    public function UpdatePhoto(Request $request){

        if($request->hasFile('photo')){
            if(Auth::user()->photo != 'perfil.png' ){
                $file_path = public_path().'/img/users/'.Auth::user()->photo;
                \File::delete($file_path);
            }
            $nameFile = "";
            $file = $request->file('photo');
            $nameFile = time().$file->getClientOriginalName();
            $thumbnail = Image::make($file)->resize(256, 256)->
                    save(public_path().'/img/users/'.$nameFile);
            Auth::user()->photo = $nameFile;
            Auth::user()->save();
            return response()->json([
                'status' => true,
            ]);
        }
    }

    public function UpdateAccount(Request $request){
        Auth::user()->username = $request->username;
        Auth::user()->name = $request->name;
        Auth::user()->lastName = $request->lastName;
        Auth::user()->email = $request->email;
        Auth::user()->save();
        if(isset($request->password)){
            if($request->password == $request->password_confirmation){
                Auth::user()->password = bcrypt($request->password);
                Auth::user()->save();
            }else{
                return  redirect('/profile/'.Auth::user()->id)->with('error', '¡Las contraseñas no coinciden!');
            }
        }
        return  redirect('/profile/'.Auth::user()->id)->with('status', '¡Información Guardada Exitosamente!');
    }

    public function UpdateContact(Request $request){

        Auth::user()->phone = $request->phone;
        Auth::user()->address = $request->address;
        Auth::user()->latitude = $request->latitude;
        Auth::user()->longitude = $request->longitude;
        Auth::user()->city = $request->city;
        Auth::user()->state = $request->state;
        Auth::user()->save();
        return  redirect('/profile/'.Auth::user()->id)->with('status', '¡Información Guardada Exitosamente!');
    }

    public function UpdateSocials(Request $request){
        Auth::user()->whats = $request->whats;
        Auth::user()->fb = $request->fb;
        Auth::user()->insta = $request->insta;
        Auth::user()->state = $request->state;
        Auth::user()->save();
        return  redirect('/profile/'.Auth::user()->id)->with('status', '¡Información Guardada Exitosamente!');
    }

    public function profile($id){
        
        $user = User::find($id);

        return view('profile', compact('user'));
    }

}
