<?php

namespace TrabajoExpress\Http\Controllers;

use TrabajoExpress\Service;
use TrabajoExpress\CategoryService;
use TrabajoExpress\Http\Requests\ServiceRequest;

use Illuminate\Http\Request;
use Auth;
use Image;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jobs = Service::paginate(15);
        $titleTab = 'Negocios';

        if($request->ajax()){
            return response()->json(
                view('job.jobs_catalog',compact('jobs','titleTab'))->render()
            );
        }
      
        $categories = CategoryService::all();

        return view('home', compact('jobs','categories','titleTab'));
    }

    public function MyServices(Request $request){
        $services = Service::where('user_id', Auth::user()->id)->paginate(6);

        if($request->ajax()){
            return response()->json(
                view('job.jobs_catalog',compact('jobs','titleTab'))->render()
            );
        }
      

        return view('service.index', compact('services'));
    }

    public function delete(Request $request)
    {
        $service = Service::find($request->service_id);
        $serviceName = $service->title;
        $file_path = public_path().'/img/services/'.$service->photo;
        \File::delete($file_path);
        $service->delete();

        return response('El Negocio '.$serviceName.' se ha eliminado');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategoryService::all();
      

        return view('service.create', compact('categories'));
    }

    public function getCategories(Request $request){

        $categories = CategoryService::all();
        return response($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->withErrors([$request->validator->errors()->all()]);
        }else{
            $newService = new Service($request->except('photo'));

            Auth::user()->services()->save($newService);
            $newSlug =  str_replace(" ", "-", "$request->title");
            $newService->slug =  $newSlug.'-'.$newService->id;
            $newService->views = 0;

            if($request->hasFile('photo')){
                $nameFile = '';
                $file = $request->file('photo');
                $nameFile = time().$file->getClientOriginalName();
                $thumbnail = Image::make($file)->resize(120, 120)->
                        save(public_path().'/img/services/thumbnails/'.$nameFile);
                $image = Image::make($file)->resize(1024, 720)->
                    save(public_path().'/img/services/'.$nameFile);
                $newService->photo = $nameFile;
            }
           
            $newService->status = 'Pendiente';
            $newService->save();

            return  redirect('/payment/service/'.$newService->id)->with('status', '¡Información Guardada Exitosamente!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \TrabajoExpress\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        $service->views = $service->views + 1;
        $service->save(); 
        return view('service.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \TrabajoExpress\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \TrabajoExpress\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \TrabajoExpress\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
