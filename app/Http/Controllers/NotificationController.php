<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;

use TrabajoExpress\Notification;
use Auth;

class NotificationController extends Controller
{
    //
    public function setViews(Request $request){
        $notifications = Notification::where('user_id',Auth::user()->id)
                                    ->where('view',false)->get();
        foreach ($notifications as $key => $notifi) {
            $notifi->view = 1;
            $notifi->save();
        }               
        return response('update!');
    }
}
