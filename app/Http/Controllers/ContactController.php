<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use TrabajoExpress\Contact;
use Mail;
class ContactController extends Controller
{
    public function index(){

        return view('contact');
    }

    public function store(Request $request){
        // try{

            Contact::create($request->all());

            $email = 'juanjosko@gmail.com';
            Mail::send('mail.contact',[
                        'name' =>  $request->name,
                        'msj' => $request->message,
                        'phone' => $request->phone,
                        'email' => $request->email
                        ], function($msj) use($email){
            $msj->subject('Nuevo Mensaje de Contacto');
            $msj->to($email);
            });
            return response([
                'status' => 'success',
                'title' => 'Mensaje enviado',
                'message' => 'Gracias por ponerse en contacto con nosotros'
            ]);  
        // }catch(\Exception  $e){
        //     return response([
        //         'status' => 'error',
        //         'title' => 'Mensaje no enviado',
        //         'message' => 'Uppss.. Lo sentimos ha ocurrido  un error inesperdado, por favor intenta de nuevo o más tarde'
        //     ]);  
        // }

       
        
    }
}
