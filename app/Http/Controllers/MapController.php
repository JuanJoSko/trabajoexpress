<?php

namespace TrabajoExpress\Http\Controllers;

use Illuminate\Http\Request;
use FarhanWazir\GoogleMaps\GMaps;
use TrabajoExpress\Job;
use TrabajoExpress\Service;


class MapController extends Controller
{

    protected $gmap;

    public function __construct(GMaps $gmap){
        $this->gmap = $gmap;
    }

    public function indexJob(Request $request){
        $type = 'jobs';
        $config = array();
        $config['center'] = '20.5661379,-100.3781325';
        $config['zoom'] = '14';
        $config['map_height'] = '100%';

        $this->gmap->initialize($config); // Initialize Map with custom configuration
        if(isset($request->search))
            $jobs = Job::search($request->search)
            ->where('isDelete',0)
            ->where('isApproved',1)
            ->get();
        else{
            $jobs = Job::all();
        }

        $image = url('/').'/img/marker_fill.png';

        foreach ($jobs as $job) {
            $marker['position'] = $job->latitude.','.$job->longitude;
            $marker['icon'] = $image;
            $marker['icon_scaledSize'] = '52, 62';
            $marker['label_origin'] = '26, 22';
            $description = preg_replace( "/\r|\n/", "",$job->description);
            $marker['label'] = '{fontFamily: "categories",text: "\u'.$job->category->cheatsheet.'",fontSize: "22px",color: "white" }';
            $contentString = '<div class="grid-view brows-job-list  scale-up-center">'.
           ' <div class="brows-job-company-img">'.
           ($job->photo == '' ?  ($job->category->name != 'Express' ? ('<i class="cat-'.$job->category->icon.' cat-photo"></i>'): '<i class="icon-express" style="    font-size: 3em;position: relative; top: 18px; right: 0; color: white; cursor: pointer;" ></i>') : ('<img src="'.url('/').'/img/jobs/'.$job->photo.'" class="img-responsive" alt="" />')).
            '</div>'.
            '<div class="brows-job-position">'.
                '<h3><a href="'.url('/').'/job/'.$job->slug.'">'.str_limit( $job->title, 18).'</a></h3>'.
                '<p class="fix-description"><span>'.str_limit( $description, 32).'</span></p>'.
            '</div>'.
           ' <div class="job-position">'.
                '<span class="job-num">0 Postulados</span>'.
            '</div>'.
            '<div class="brows-job-type">'.
                '<span class="full-cash"><i class="fa fa-money"></i> $'.$job->payment.'</span>'.
            '</div>'.
            '<ul class="grid-view-caption">'.
               ' <li>'.
                    '<div class="brows-job-location">'.
                        '<p><i class="fa fa-map-marker"></i>'.str_limit( $job->address, 32).'</p>'.
                    '</div>'.
                '</li>'.
           '</ul>'.
        '</div>';
            $marker['infowindow_content'] =  $contentString;
          
            $this->gmap->add_marker($marker);
        }

        $map = $this->gmap->create_map(); // This object will render javascript files and map view; you can call JS by $map['js'] and map view by $map['html']


        return view('maps.index',  compact('map','type'));
    }

    public function indexService(Request $request){
        
        $type = 'services';
        $config = array();
        $config['center'] = '20.5661379,-100.3781325';
        $config['zoom'] = '14';
        $config['map_height'] = '100%';

        $this->gmap->initialize($config); // Initialize Map with custom configuration
        if(isset($request->search))
            $jobs = Service::search($request->search)
            ->where('isDelete',0)
            ->where('isApproved',1)
            ->get();
            // $jobs = Service::where('title', 'like', "%{$request->search}%")->get();
        else
            $jobs = Service::all();
        $image = url('/').'/img/marker_fill.png';

        foreach ($jobs as $job) {
            $marker['position'] = $job->latitude.','.$job->longitude;
            $marker['icon'] = $image;
            $marker['icon_scaledSize'] = '52, 62';
            $marker['label_origin'] = '26, 22';
            $marker['label'] = '{fontFamily: "icomoon",text: "\ue91a",fontSize: "22px",color: "white" }';
            $description = preg_replace( "/\r|\n/", "",$job->description);
            $contentString = '<div class="grid-view brows-job-list  scale-up-center">'.
           ' <div class="brows-job-company-img">'.
           ($job->photo == '' ?  '<i class="icon-express" style="font-size: 3em;position: relative; top: 18px; right: 0; color: white; cursor: pointer;" ></i>' : ('<img src="'.url('/').'/img/services/'.$job->photo.'" class="img-responsive" alt="" />')).
            '</div>'.
            '<div class="brows-job-position">'.
                '<h3><a href="'.url('/').'/service/'.$job->slug.'">'.str_limit( $job->title, 18).'</a></h3>'.
                '<p class="fix-description"><span>'.str_limit( $description, 32).'</span></p>'.
            '</div>'.
            '<ul class="grid-view-caption">'.
               ' <li>'.
                    '<div class="brows-job-location">'.
                        '<p><i class="fa fa-map-marker"></i>'.str_limit( $job->address, 32).'</p>'.
                    '</div>'.
                '</li>'.
           '</ul>'.
        '</div>';
            $marker['infowindow_content'] =  $contentString;
          
            $this->gmap->add_marker($marker);
        }

        $map = $this->gmap->create_map(); // This object will render javascript files and map view; you can call JS by $map['js'] and map view by $map['html']


        return view('maps.index',  compact('map','type'));
    }
}
