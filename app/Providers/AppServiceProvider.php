<?php

namespace TrabajoExpress\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Auth;
use View;
use TrabajoExpress\Notification;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('*', function($view) {
            if(Auth::check()){
                $notifys =  Notification::where('user_id',Auth::user()->id)->
                        where('view',false)->get();
                View::share('notifys', $notifys);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (env('APP_ENV') === 'production') {
            $this->app->bind('path.public', function() {
                return realpath(base_path().'/../trabajoexpress.com.mx');
            });
        }
    }
}
