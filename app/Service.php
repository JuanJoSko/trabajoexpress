<?php

namespace TrabajoExpress;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Searchable;

    public function toSearchableArray()
    {
        /**
         * Load the categories relation so that it's
         * available in the Laravel toArray() method
         */

        $array = $this->toArray();
        $array = $this->transform($array);

        $array['category'] = $this->category->name;

        return $array;
    }

    protected $fillable = [
        'photo', 
        'category_id',	
        'title',
        'description',
        'slug',
        'status',
        'address',
        'phone',
        'latitude',
        'longitude',
        'email',

    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function category()
    {
        return $this->belongsTo(CategoryService::class);
    }
}
