<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $fillable = [
        'user_id',
        'author_id',
        'postulation_id',
        'points',
        'type',
        'comment'
    ];

    protected $hidden = ['slug'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function postulation()
    {
        return $this->belongsTo(JobUser::class);
    }
}
