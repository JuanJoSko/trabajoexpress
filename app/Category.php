<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
