<?php
//app/Helpers/Envato/User.php
namespace TrabajoExpress\Helpers;
 
 
class TemplateOne {
    /**
     * @param string $name
     * 
     * @return string
     */
    public static function GetName($name) {
        $nameTemplate;
        switch($name){
            case "newPostulate":
                return "75ad5838-1d90-42f7-bc68-7c6e9726fbc4";
            break;
            case "acceptPostulate":
                return "8cd578d6-6741-4c1d-823e-c667ea9bf04d";
            break;
            case "cancelPostulate":
                return "b96c203d-59b2-4b45-acb9-47c6b9e187ff";
            break;
        }
         
        return "Nombre invalido";
    }
}
  