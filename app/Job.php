<?php

namespace TrabajoExpress;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;


class Job extends Model
{
    use Searchable;

    public function toSearchableArray()
    {
        /**
         * Load the categories relation so that it's
         * available in the Laravel toArray() method
         */

        $array = $this->toArray();
        $array = $this->transform($array);

        $array['category'] = $this->category->name;

        return $array;
    }
    
    protected $fillable = [
        'photo',
        'category_id', 
        'title', 
        'description',
        'requirements',
        'isSenior',
        'payment',
        'time',
        'age',
        'status',
        'address',
        'latitude',
        'longitude',
    ];

    protected $hidden = ['password'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }

    public function allPostulates()
    {
        return $this->belongsToMany(User::class, 'postulations', 'job_id', 'user_id')
                    ->using(JobUser::class)
                    ->withPivot('status');
    }
    public function acceptingPostulates()
    {
        return $this->belongsToMany(User::class, 'postulations', 'job_id', 'user_id')
                    ->using(JobUser::class)
                    ->withPivot('status','id')
                    ->wherePivot('status', 'Aceptado');;
    }
    

    public function havePostulation($user_id)
    {
        return  $this->belongsToMany(User::class, 'postulations', 'job_id', 'user_id')
                    ->where('user_id', $user_id)->count();
    }

    public function pendingPostulates()
    {
        return $this->belongsToMany(User::class, 'postulations', 'job_id', 'user_id')
                    ->withPivot('id')
                    ->wherePivot('status', 'Pendiente');
    }

    public function acceptPostulate()
    {
        return $this->belongsToMany(User::class, 'postulations', 'job_id', 'user_id')
                    ->using(JobUser::class)
                    ->withPivot('status','id')
                    ->wherePivot('status', 'Aceptado')->first();
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}
