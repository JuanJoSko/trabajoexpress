<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'turn', 
        'description', 
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
