<?php

namespace TrabajoExpress;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'lastName', 
        'email', 
        'password',
        'address',
        'state',
        'city', 
        'phone',
        'birthday',
        'latitude',
        'longitude',
        'token'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = array('stars');


    public function curriculum()
    {
        return $this->hasOne(Curriculum::class);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class);
    }


    public function getStarsAttribute()
    {
        return $this->stars();
    }

    public function points(){
        // return $this->hasMany(Qualification::class)->pluck('points');
        return $this->hasMany(Qualification::class)->pluck('points');

    }
    

    public function stars()
    {
        $points = $this->hasMany(Qualification::class)->pluck('points');
        $total = 0;
        foreach ($points as $key => $value) {
           $total += $value;
        }
        $stars = 0;
        if(count($points) > 0)
            $stars = $total/ count($points);
        return ($stars);

    }

    public function company()
    {
        return $this->hasOne(Company::class);
    }

    public function postulations()
    {
        return $this->belongsToMany(Job::class, 'postulations','user_id','job_id')
                    ->using(JobUser::class)
                    ->withPivot('status','id')
                    ->orderBy('created_at', 'DES');
    }

    public function pendingPostulations()
    {
        return $this->belongsToMany(Job::class, 'postulations', 'user_id', 'job_id')
                    ->wherePivot('status', 'Pendiente')->count();
    }

    public function acceptingPostulations()
    {
        return $this->belongsToMany(Job::class, 'postulations', 'user_id', 'job_id')
                    ->wherePivot('status', 'Aceptado')->count();
    }

    public function cancelPostulations()
    {
        return $this->belongsToMany(Job::class, 'postulations','user_id','job_id')
                    ->wherePivot('status', 'Cancelado')->count();
    }

    public function notifys()
    {
        return $this->hasMany(Notification::class)->orderBy('view','ASC');
    }
    public function newNotifys()
    {
        return $this->hasMany(Notification::class)->where('view',false);
    }

    
}
