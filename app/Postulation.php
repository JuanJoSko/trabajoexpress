<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Model;

class Postulation extends Model
{
    protected $fillable = [
        'user_id', 
        'job_id', 
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
    public function stars(){
        return $this->hasMany(Qualification::class);
    }
    public function starsEmployee(){
        return  $this->hasMany(Qualification::class)
                    ->where('type', 'Employee')->count();
    }
    public function starsEmployer(){
        return  $this->hasMany(Qualification::class)
                    ->where('type', 'Employer')->count();
    }
    
}
