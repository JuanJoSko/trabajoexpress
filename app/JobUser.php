<?php

namespace TrabajoExpress;

use Illuminate\Database\Eloquent\Relations\Pivot;

class JobUser extends Pivot
{
    protected $table = 'postulations';

    public function stars(){
        return $this->hasMany(Qualification::class,'postulation_id');
    }

    public function starsEmployee(){
        return  $this->hasMany(Qualification::class, 'postulation_id')
                    ->where('type', 'Employee')->count();
    }

    public function starsEmployer(){
        return  $this->hasMany(Qualification::class, 'postulation_id')
                    ->where('type', 'Employer')->count();
    }
}