<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/jobs', 'App\AppJobController@index');
Route::post('/getJob', 'App\AppJobController@getJob');
Route::post('/getJobsById', 'App\AppJobController@getJobsById');
Route::post('/getUser', 'App\AppUserController@getUser');
Route::post('/login','App\AppLoginController@login');
Route::post('/register','App\Auth\AppRegisterController@register');
Route::post('/makePostulation','App\AppPostulationController@store');
Route::get('/categories', 'App\AppCategoryController@getAll');
Route::get('/categoriesService', 'App\AppCategoryController@getAllService');
Route::post('/getCategoryJob', 'App\AppJobController@getCategoryJob');
Route::post('/getCategoryService', 'App\AppServiceController@getCategoryService');
Route::post('/myPostulations', 'App\AppPostulationController@index');
Route::get('/getMyJobs', 'App\AppJobController@getMyJobs');
Route::post('/deleteJob', 'App\AppJobController@deleteJob');
Route::post('/createJob', 'App\AppJobController@store');
Route::post('/updateJob', 'App\AppJobController@update');
Route::post('/createService', 'App\AppServiceController@store');
Route::post('/updateService', 'App\AppServiceController@update');
Route::post('/getServicesById', 'App\AppServiceController@getServiceById');
Route::post('/services', 'App\AppServiceController@index');
Route::post('/getService', 'App\AppServiceController@getService');
Route::post('/deleteService', 'App\AppServiceController@deleteService');
Route::post('/completePerfil', 'App\AppUserController@completePerfil');
Route::post('/updatePerfil', 'App\AppUserController@update');
Route::post('/getPendingPostulate', 'App\AppPostulationController@getPendingPostulate');
Route::post('/confirmationPostulate', 'App\AppPostulationController@confirmationPostulate');
Route::post('/getJobDetails', 'App\AppJobController@getJobDetails');
Route::post('/getAcceptPostulate', 'App\AppPostulationController@getAcceptPostulate');
Route::post('/getQualifications', 'App\AppQualificationController@index');
Route::post('/setToken', 'App\AppUserController@setToken');
Route::post('/notifications', 'App\AppNotificationController@index');
Route::post('/notifications/count', 'App\AppNotificationController@countNoti');
Route::post('/notifications/setViews', 'App\AppNotificationController@setViews');
Route::post('/notifications/setViewID', 'App\AppNotificationController@setViewID');
Route::post('/reset', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/storeQualifications', 'App\AppQualificationController@store');
Route::post('/check/user', 'App\AppUserController@checkUserID');























