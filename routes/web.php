<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/map', 'MapController@indexJob')->name('map');
Route::get('/map/services', 'MapController@indexService')->name('map.services');
Route::post('/map/search-job', 'MapController@indexJob')->name('map.searchMapJob');
Route::post('/map/search-service', 'MapController@indexService')->name('map.searchMapService');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/my-profile', 'UserController@MyProfile')->name('myProfile');
Route::get('/my-jobs', 'JobController@MyJobs')->name('myJobs');
Route::get('/my-services', 'ServiceController@MyServices')->name('myServices');
Route::get('/profile/{id}', 'UserController@profile')->name('profile');
Route::post('/profile-update/about', 'UserController@UpdateAbout')->name('profile.updateAbout');
Route::post('/profile-update/contact', 'UserController@UpdateContact')->name('profile.updateContact');
Route::post('/profile-update/account', 'UserController@UpdateAccount')->name('profile.updateAccount');
Route::post('/profile-update/socials', 'UserController@UpdateSocials')->name('profile.updateSocials');
Route::post('/profile-update/photo', 'UserController@UpdatePhoto')->name('profile.updatePhoto');
Route::resource('/job', 'JobController');
Route::get('/job-search', 'JobController@search');
Route::resource('/service', 'ServiceController');
Route::post('/service/delete', 'ServiceController@delete');
Route::get('/cancel-services', 'JobController@cancelServices');
Route::get('/pay-services', 'JobController@payServices');
Route::get('/pending-services', 'JobController@pendingServices');
Route::get('/old', 'JobController@old');
Route::get('/cancel-jobs', 'JobController@cancelJobs');
Route::get('/pay-jobs', 'JobController@payJobs');
Route::get('/pending-jobs', 'JobController@pendingJobs');
Route::get('/job/my-jobs', 'JobController@myJobs');
Route::get('/job/filters', 'JobController@filters');
Route::post('/job/delete', 'JobController@delete');
Route::get('/employee/profile', 'EmployeeController@index');
Route::get('/contact', 'ContactController@index')->name('contact.index');
Route::post('/contact/store', 'ContactController@store')->name('contact.store');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/politics', 'HomeController@politics')->name('politics');
Route::get('/payment/{type}/{id}/{source?}', 'PaymentController@index')->name('payment.index');
Route::post('/payment/process', 'PaymentController@process')->name('payment.process');
Route::post('/payment/new', 'PaymentController@newPayment')->name('payment.new');
Route::get('/postulation/{job}', 'PostulationController@store');
Route::get('/postulation', 'PostulationController@index')->name('postulations.index');
Route::post('/confirmation', 'PostulationController@confirmation');
Route::get('/notification/view', 'NotificationController@setViews');
Route::post('/qualification/store', 'QualificationController@store')->name('qualification.store');
Route::post('/payment/paypal', 'PaypalController@payWithpaypal')->name('paypal.complete');
Route::get('/payment/status', 'PaypalController@getPaymentStatus')->name('paypal.status');
Route::get('/test','TestController@test');
Route::get('/movil/payment/status/{status}','PaymentController@statusPaymentMovil');
Route::get('/getCategories','ServiceController@getCategories')->name('getCategories');


Route::get('/',function(){
    return  redirect()->route('job.index');
});

// Route::group(['prefix' => 'admin'], function () {
Route::namespace('Admin')->prefix('4dmil7')->name('admin.')->group(function () {

    Route::get('/dashboard', 'AdminController@index')->name('dashboard');
    Route::resource('/users','AdminUserController');
    Route::resource('/jobs','AdminJobController');
    Route::resource('/services','AdminServiceController');

});

